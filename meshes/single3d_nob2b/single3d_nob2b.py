import numpy as np
import struct

# mesh file
mesh = open("mesh.x", "w")

# number of blocks
#mesh.write(struct.pack('i',1))
mesh.write('1\n')

# uniform grid space step
LX =  2    # x length
LY =  2    # y length
LZ =  2    # z length
N  =  32   # grid cells per unit length
h  =  1/N  # space step

# block 1 range: 0-LX, 0-LY
# plot3D format header: block index range
#mesh.write(struct.pack('iii', LX*N+1, LY*N+1, LZ*N+1))
mesh.write( repr(LX*N+1) + ' ' + repr(LY*N+1) + ' ' + repr(LZ*N+1) + '\n')

# write coordinates
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write((repr(i*h) + ' '))
      #mesh.write(struct.pack('d', i*h))
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write((repr(j*h) + ' '))
      #mesh.write(struct.pack('d', j*h))
for k in range(LZ*N+1):
  for j in range(LY*N+1):
    for i in range(LX*N+1):
      mesh.write((repr(k*h) + ' '))
      #mesh.write(struct.pack('d', k*h))
mesh.write('\n')

mesh.close()

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

# block 1 face 1,2,3,4,5,6
bcMap.write("FarField 1 1\n")
bcMap.write("FarField 1 2\n")
bcMap.write("FarField 1 3\n")
bcMap.write("FarField 1 4\n")
bcMap.write("FarField 1 5\n")
bcMap.write("FarField 1 6\n")


