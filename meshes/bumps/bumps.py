import numpy as np

# mesh file
mesh = open("mesh.x", "w")

# number of blocks
mesh.write("5\n") 

# uniform grid space step
N = 4
h = 1/N

# block 1 range: 0-10, 0-2
# block 4 range: 2-3,  2-2.5
# block 2 range: 4-5,  2-3
# block 3 range: 6-7,  2-3.5
# block 5 range: 8-9,  2-4
# plot3D format header: block index range
mesh.write( repr(10*N+1) + ' ' + repr(2*N+1)   + ' ' + repr(1) + ' ' 
          + repr(N+1)    + ' ' + repr(N+1)     + ' ' + repr(1) + '\n')
mesh.write( repr(N+1)    + ' ' + repr(np.int(3*N/2+1)) + ' ' + repr(1) + ' '
          + repr(N+1)    + ' ' + repr(np.int(N/2+1))   + ' ' + repr(1) + '\n')
mesh.write( repr(N+1)    + ' ' + repr(2*N+1)   + ' ' + repr(1) + '\n')

# count every six coordinate then start new line
count = 0

# block 1 x
for j in range(2*N+1):
    for i in range(10*N+1):
        mesh.write(repr(i*h) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 1 y
for j in range(2*N+1):
    for i in range(10*N+1):
        mesh.write(repr(j*h) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 1 z
for j in range(2*N+1):
    for i in range(10*N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

# block 2 x
for j in range(N+1):
    for i in range(N+1):
        mesh.write(repr(i*h+4.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 2 y
for j in range(N+1):
    for i in range(N+1):
        mesh.write(repr(j*h+2.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 2 z
for j in range(N+1):
    for i in range(N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

# block 3 x
for j in range(np.int(3*N/2+1)):
    for i in range(N+1):
        mesh.write(repr(i*h+6.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 3 y
for j in range(np.int(3*N/2+1)):
    for i in range(N+1):
        mesh.write(repr(j*h+2.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 3 z
for j in range(np.int(3*N/2+1)):
    for i in range(N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

# block 4 x
for j in range(np.int(N/2+1)):
    for i in range(N+1):
        mesh.write(repr(i*h+2.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 4 y
for j in range(np.int(N/2+1)):
    for i in range(N+1):
        mesh.write(repr(j*h+2.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 4 z
for j in range(np.int(N/2+1)):
    for i in range(N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

# block 5 x
for j in range(2*N+1):
    for i in range(N+1):
        mesh.write(repr(i*h+8.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 5 y
for j in range(2*N+1):
    for i in range(N+1):
        mesh.write(repr(j*h+2.0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0
# block 5 z
for j in range(2*N+1):
    for i in range(N+1):
        mesh.write(repr(0) + ' ')
        count += 1
        if(count == 6):
            mesh.write("\n")
            count = 0

mesh.close()

# mapfile
bcMap = open("mapfile", "w")

# title line
bcMap.write("Type Block# Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 - Block# ")
bcMap.write("Face# Edg1.Indx1 Edg1.Indx2 Edg2.Indx1 Edg2.Indx2 Flip\n")

# block 1 face 1,2,4
bcMap.write("FarField 1 1\n")
bcMap.write("EulerWall 1 2\n")
bcMap.write("FarField 1 4\n")
# block 1 face 5
bcMap.write("EulerWall 1 5 1 1 1 " + repr(2*N+1) + "\n")
bcMap.write("EulerWall 1 5 1 1 "   + repr(3*N+1) + " " + repr(4*N+1) + "\n")
bcMap.write("EulerWall 1 5 1 1 "   + repr(5*N+1) + " " + repr(6*N+1) + "\n")
bcMap.write("EulerWall 1 5 1 1 "   + repr(7*N+1) + " " + repr(8*N+1) + "\n")
bcMap.write("EulerWall 1 5 1 1 "   + repr(9*N+1) + " " + repr(10*N+1) + "\n")
bcMap.write("Block2Block 1 5 1 1 " + repr(2*N+1) + " " + repr(3*N+1) + " " + repr(4) + " 2 1 1 1 "  + repr(N+1) + "\n")
bcMap.write("Block2Block 1 5 1 1 " + repr(4*N+1) + " " + repr(5*N+1) + " " + repr(2) + " 2 1 1 1 "  + repr(N+1) + "\n")
bcMap.write("Block2Block 1 5 1 1 " + repr(6*N+1) + " " + repr(7*N+1) + " " + repr(3) + " 2 1 1 1 "  + repr(N+1) + "\n")
bcMap.write("Block2Block 1 5 1 1 " + repr(8*N+1) + " " + repr(9*N+1) + " " + repr(5) + " 2 1 1 1 "  + repr(N+1) + "\n")

# block block 2,3,4,5, face 1,4,5
for i in range(2,6):
    for j in [1,4,5]:
        bcMap.write("EulerWall " + repr(i) + " " + repr(j) + "\n")
# close file
bcMap.close()
