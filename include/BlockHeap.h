#ifndef BLOCKHEAP_H
#define BLOCKHEAP_H

#include "common.h"
#include "Block.h"
#include "BlockGraph.h"


class BlockNode
{
  public:  //---------- public member functions ----------//
  // basics
  BlockNode(int id, double msgload)
    { blkID = id; load = msgload; }
  BlockNode(const Block& blk)
    { blkID = blk.id(); load = blk.msg_load(); }

  int id()
    { return(blkID); }

  double msg_load()
    { return(load); }

  void set_load(double val)
    { load = val; }

  bool operator<(const BlockNode& rhs) const
    { return(load < rhs.load); }
    
  bool operator>(const BlockNode& rhs) const
    { return(load > rhs.load); }
    
  private: //---------- private attributes      ----------//
  int    blkID;
  double load;
};


class BlockHeap
{
  public:  //---------- public member functions ----------//
  BlockHeap()
  /* default constructor */
  {}

  int build(const BlockGraph& bg);
  /* Build up the head for input block graph */

  int build(const BlockGraph& bg, const vector<int>& bids);
  /* Build up the head for input block graph, input block ids */

  int hpfy_min(int i);
  /*----------------------------------------------------------
    heapify a min heap

    IN: i - the position in heap
  ----------------------------------------------------------*/

  int swap_node(int lhsID, int rhsID);
  /*----------------------------------------------------------
    swap two block node in heap

    IN: lhsID, rhsID - two pos in heap
  ----------------------------------------------------------*/

  int chng_load(int blkID, double val);
  /*----------------------------------------------------------
    Change the load of a block and update its pos in heap

    IN: blkID - id of block 
        val   - new load value
  ----------------------------------------------------------*/

  int append_blk(const Block& blk);
  /* Add one block to heap */

  int append_blk(const int blkNull);
  /* Add a null block to heap to sync the heap index */

  int pop_min();
  /* remove the top and update heap */

  int pop_blk(int id);
  /* remove the block of the input id and update heap */

  int id_min()
  /* Return the block id of the top */
  { return(_nodes[0].id()); }

  unsigned int size()
  /* Return the # elements in heap */
  { return(_nodes.size()); }

  bool is_in(int blkID);
  /* Return whether an element is in heap */

  int clear();
  /* clear all data */

  int view();
  /* show all elements in heap */

  private: //---------- private attributes      ----------//
  vector<BlockNode> _nodes;
  vector<int>       _hpIDs; // block's pos in heap
};


#endif
