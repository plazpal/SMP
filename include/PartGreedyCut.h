#ifndef PartGreedyCut_H
#define PartGreedyCut_H

#include "common.h"
#include "Block.h"
#include "BlockGraph.h"
#include "PartMethod.h"

class PartGreedyCut : public PartMethod
{
  public:  //---------- public member functions ----------//
  PartGreedyCut()
  /* constructor with input # partitiions */
  {}

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);
  /* decompose the grid */

  int divide_zone(BlockGraph& bg, vector<int>& bids);
  /* Divide the domain into main and residue zone */

  int assign_r_zone(BlockGraph& bg, vector<int>& bids);

  int cut_m_zone_3d(BlockGraph& bg, vector<int>& bids, int bid);
  /*--------------------------------------------------------------
  Cut the main zone to partitions
  
  In:  bg   - block graph
       bid  - block's id

  Pre: The block has only main zone and is marked as undone. 
       It has at least 1 partition's work load

  Res: The main zone is cut into partitions.
  --------------------------------------------------------------*/

  int cut_mzone_reb(BlockGraph& bg, vector<int>& bids, int bid);

  private: //--------  private member functions   --------//
  int _find_rzone_cmpny(BlockGraph& bg, vector<int>& bids, \
                        vector<int>& localBids, int& cmpny);
  /*--------------------------------------------------------------
  Find company block for residual blocks to fit in 1 partition.
  
  In:  bg    - block graph
       bids  - residual blocks' id
  Out: cmpny - company block id.

  Result: A residual zone may be cut as a separate block to serve
          as the company. Or a small block may be chosen.
  --------------------------------------------------------------*/
  
  int _cut_blk_to_parts(BlockGraph& bg, vector<int>& bids, int bid, \
                        vector< vector<int> >& cutLocs);

  private: //---------- private attributes      ----------//
  vector<double> _rCls;       // the index of part to assign
  vector<double> _rFlrs;      // the index of part to assign
  vector<int>    _nLoads;     // # load of the main zone
  int            _nAvailPart; // available partitions
  int            _iPart;
  vector<bool>   _isDones;
};


#endif
