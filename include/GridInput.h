#ifndef GRIDINPUT_H
#define GRIDINPUT_H

#include <mpi.h>
#include <sstream>
#include <fstream>
#include <string>
#include "common.h"

using namespace std;

class GridInput
{
  public:  //---------- public member functions ----------//
  GridInput()
  { _nBlk = 0; _blkBuf = 0; _nMap = 0; _mapBuf = 0; }

  GridInput(const GridInput& rhs);

  const GridInput& operator=(const GridInput& rhs);

  ~GridInput();

  int bcast_block_info(const string& fname);
  /* include partition id */

  int bcast_map(const string& fname);
  /* read maps from mapfile */

  private: //---------- private attributes      ----------//
  int  _nBlk;
  int *_blkBuf;
  int  _nMap;
  int *_mapBuf;

  friend class BlockGraph;
};


#endif
