#ifndef PARTITION_H
#define PARTITION_H

#include <iostream>
#include <fstream>
#include <vector>
#include <algorithm>
#include "common.h"
#include "Block.h"
#include "BlockGraph.h"

using namespace std;

const int TIME     = 0;
const int NUM_CELL = 1;

class Partition
{
  public:
  Partition():
  /* default constructor */
    id(PARTITION_NULL), blkIDs(), nbrs(), _work(0.0), _wkIdeal(0.0), 
    _t(0.0), _tComp(0.0), _tComm(0.0), _cmpr(NUM_CELL),
    _nComm(0), _nShr(0), _volComm(0.0), _volShr(0.0)
  {}

  Partition(const Partition& rhs);
  /* copy constructor */

  const Partition& operator=(const Partition& rhs); 
  /* assignment */

  ~Partition()
  /* default destructor */
  { id = PARTITION_NULL; _work = 0; }

  void set_comparison(const int cmpr)
  /* set to compare work or time */
  { _cmpr = cmpr; }

  bool operator<(const Partition& rhs) const;
  /* overload strict less than */

  bool operator>(const Partition& rhs) const;
  /* overload strict greater than */

  double work() const
  /* return work load */
  { return(_work); }

  double ideal_work() const
  /* ideal work */
  { return(_wkIdeal); }

  double time() const
  /* return time */
  { return(_t); }

  double time_comm() const
  /* return communication time */
  { return(_tComm); }

  double time_comp() const
  /* return computation time */
  { return(_tComp); }

  int num_comm() const
  { return(_nComm); }

  int num_shmem_copy() const
  { return(_nShr); }

  double vol_comm() const
  { return(_volComm); }

  double vol_shmem_copy() const
  { return(_volShr); }

  double vol_direct_copy() const
  { return(_volDirectCpy); }

  double vol_indirect_copy() const
  { return(_volIndirectCpy); }

  double imbalance() const
  /* return the difference from ideal work */
  { return((_work - _wkIdeal) / _wkIdeal); }

  bool is_overload(double toler = 0.0) const
  /* return whether part is overload */
  { return(_work > _wkIdeal*(1.0+toler)); }

  int add_load(double load)
  /* Add input value to work load */
  { _work += load; return(0); }

  int decr_load(double load)
  /* decrease work load by input value */
  { _work -= load; return(0); }

  void set_ideal_work(double val)
  /* set the ideal work load */
  { _wkIdeal = val; }

  void set_work(double val)
  /* set the work load */
  { _work = val; }

  int setup_nbr(BlockGraph& bg);
  /*----------------------------------------------------------
    Set the connected partitions.
   
    In:  bg - given block graph
    Pre: The blocks in partition has already been set.
  ----------------------------------------------------------*/

  int del_blk(int blkID);
  /*----------------------------------------------------------
    delete a block from part
   
    In:   blkID - block id to be deleted
    Post: the nbrs are not updated.
  ----------------------------------------------------------*/

  int chng_blk(int bid, int bidNew);
  /* remove the input block id, not update nbr */

  int view();
  /* print the blocks, work load and nbrs to terminal */

  int del_shmem_map(BlockGraph& bg, int bid);
  /*----------------------------------------------------------
    Mark the map between given block and blocks in given part
    as not shared. This is typically used when a block is moved
    out of the partition.
    
    In: bg  - input block graph
        bid - block id
  ----------------------------------------------------------*/

  int add_shmem_map(BlockGraph& bg, int bid);
  /*----------------------------------------------------------
    Mark the map between given block and blocks in given part
    as shared. This is typically used when a block is added to
    the partition.
    
    In: bg  - input block graph
        bid - block id
  ----------------------------------------------------------*/

  int cmpt_msg_load(BlockGraph& bg);
  /* compute the communication time this partition */

  int cmpt_time(BlockGraph& bg);
  /* compute the time cost of blocks in this partition */

  public:
  int         id;     // part id
  vector<int> blkIDs; // blocks resided
  vector<int> nbrs;   // nbr part id

  private:
  double _work;              // sum of work load of all blocks
  double _wkIdeal;           // ideal work load
  double _t, _tComp, _tComm; // total, comp, comm time
  int    _cmpr;              // compare time or work
  int    _nComm, _nShr;      // #comm, #copy
  double _volComm, _volShr;  // vol of comm and shmem copy
  double _volDirectCpy, _volIndirectCpy;
};


class PartArray
{
  public:
  PartArray()
  /* default constructor */
    { _nPart = 0; _vals = 0; _keys = 0; _order = NUM_CELL; }

  PartArray(const int& nPart);
  /* constructor with input # partitions */

  PartArray(const PartArray& rhs);                  
  /* copy constructor */

  const PartArray& operator=(const PartArray& rhs); 
  /* assignment */

  ~PartArray();
  /* default destructor */

  void set_order(const int order);
  /* set the order of sorting */

  int size()
  /* return # paritions */
    { return(_nPart); }

  int build();
  /* Sort the vals and set up the keys */

  Partition& rank(int i)
  /* Return the ith partition in sorted order */
  { return(_vals[i]); }

  Partition& operator[](int i)
  /* Return the ith partition */
  { return(_vals[_keys[i]]); }

  int add_load(int i, double load);
  /*---------------------------------------------------------- 
   Add work load to given parition and maintain order

   In: i    - index of partition
       load - value add to work load
  ----------------------------------------------------------*/

  int find_part_shift(BlockGraph& bg, int pid0, int pid1, double toler, \
                      BlockCut& cut, bool isBlnc = true);
  /*----------------------------------------------------------
    Find block/cut-off that can be shifted from overload part
    to underload part. 
    
    In:   bg     - BlockGraph
          pid0   - overload  partition's id
          pid1   - underload partition's id
          toler  - tolerance of work load
          isBlnc - balance the target part or not
    Out:  Cut    - block cut, represents the shift
  ----------------------------------------------------------*/

  int find_time_shift(BlockGraph& bg, int pid0, int pid1, BlockCut& cut);
  /*----------------------------------------------------------
    Find block/cut-off that can be shifted from overload part
    to underload part. 
    
    In:   bg     - BlockGraph
          pid0   - overload  partition's id
          pid1   - underload partition's id
          toler  - tolerance of work load
          isBlnc - balance the target part or not
    Out:  Cut    - block cut, represents the shift
  ----------------------------------------------------------*/

  int shift_part(BlockGraph& bg, int toPart, BlockCut& cut);
  /*----------------------------------------------------------
    Shift block/cut-off to the target parition.
    The source partition can be known via cut's block id.
    
    In:   bg     - BlockGraph
          toPart - target partition
          cut    - specify the shift
    Out:  bg     - may have new block or change a block's part

    Res:  Both part's workload are updated and so is their 
          location in array.
  ----------------------------------------------------------*/

  int merge_part_blk(BlockGraph& bg, int pid);
  /* Try to merge the blocks in given partition.*/

  int merge_part_blk(BlockGraph& bg);
  /* Try to merge the blocks in each partition.*/

  int clear();
  /* clear all data */

  private:
  int _rm_null_blk(BlockGraph& bg);

  private:
  int        _nPart;
  Partition *_vals;
  int       *_keys;
  int        _order;
};


#endif
