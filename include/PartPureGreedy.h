#ifndef PUREGREEDY_H
#define PUREGREEDY_H

#include "Block.h"
#include "BlockGraph.h"
#include "Partition.h"
#include "PartMethod.h"

class PartPureGreedy : public PartMethod
{
  public:  //---------- public member functions ----------//
  PartPureGreedy():
    _wkParts(0),
    _tolerMax(0.05),
    _n2Waycut(0),
    _n1WayCut(0)
  {}

  int decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid);

  private: //--------- private member functions ----------//
  int _cut_2way(BlockGraph& bg, vector<int>& bids, int bid, int axis0, int pos0,\
                int axis1, int pos1);

  int _cut_1way(BlockGraph& bg, vector<int>& bids, int bid, int axis, int pos);

  /*----------------------------------------------------------
    cut block in two axes, resulting in 4 blocks

    In:   bg           - blockgraph to be cut
          bid          - the block to cut
          axis0, axis1 - cut axes
          pos0,  pos1  - cut positions
    Out:  bg           - after being cut
          bids         - block IDs involved in partition

    Res:  After cutting, the block bid includes the cornor
          of axis0 and axis1
  ----------------------------------------------------------*/
  

  private: //---------- private attributes      ----------//
  vector<bool> _isDones;
  double      *_wkParts;
  double       _tolerMax;
  int          _n2Waycut, _n1WayCut;
};

#endif
