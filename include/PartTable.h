#ifndef PARTTABLE_H
#define PARTTABLE_H

#include "common.h"
#include "Block.h"
#include "Partition.h"


class TableRow
{
  public:  //---------- public member functions ----------//
  TableRow(int pid)
    { _pid = pid; }
  
  int part_id()
  { return(_pid); }

  bool is_empty()
  { return(_cuts.size() == 0); }

  int add(int toPart, BlockCut& cut);
  /* add new shift and maintain the order */

  int top(int& toPart, BlockCut& cut)
  /* return the top of the row, which has min msgIncr */
  { toPart = _targets[0]; cut = _cuts[0]; return(0); }

  int del_top();
  /* delete the top */

  int del_target(int toPart);
  /* delete all the elements sent to toPart */

  int pos(int toPart, const BlockCut& cut);
  /* return the index if given cut is found, else return null */

  int pos(int toPart, int bid);
  /* return the index if given block id is found, else return null */

  int del_element(int idx);
  /* delete the elment at given index */

  int del_element(const BlockCut& cut);
  /* shifts involved the input cut */

  int chng_element(int idx, int toPart, const BlockCut& cut);
  /* change the elment at given index, and adjust position */

  int clear();
  /* remove all elments in the row */

  int view();
  /* print all elements in the row */

  private: //---------- public attributes       ----------//
  int              _pid;
  vector<int>      _targets;
  vector<BlockCut> _cuts;
};


class PartTable
{
  public:  //---------- public member functions ----------//
  PartTable()
    {}
  PartTable(int nProc);

  bool is_empty();

  TableRow& operator[](int pid);
  /* return the access to a given row */

  int append_row(int pid)
  /* add new row to table */
  { _rows.emplace_back(pid); _rowIDs[pid] = _rows.size()-1; return(0);}

  int del_row(int pid);
  /* delete a row, rows size decrement */

  int clear_row(int pid);
  /* clear the data in a row, rows size no change */

  int add(int  pid, int  toPart, BlockCut& cut);
  /* Add new shift to table. */

  int pop(int& pid, int& toPart, BlockCut& cut);
  /* return the cut with min msg increase */

  int del_target(int toPart);
  /* Delete the element connected with toPart (underload) */

  int clear()
  /* clear all rows in the table */
  { _rows.clear(); return(0); }

  int view();

  private: //---------- public attributes       ----------//
  vector<int>      _rowIDs;
  vector<TableRow> _rows;
};


#endif
