#include <stdlib.h>
#include <iostream>
#include "BlockGraph.h"
#include "BlockPart.h"

using namespace std;

const int    NX  = 17;
const int    NY  = 9;
const double LX  = 4.0;
const double LY  = 2.0;

int main()
{
  int         rngs[6];
  BlockGraph  geo(1);
  Block       *ptb;

  geo.get_block_pt(0, &ptb);

  // set block data range
  rngs[0] =  0;  rngs[3] = NX-1;
  rngs[1] =  0;  rngs[4] = NY-1;
  rngs[2] =  0;  rngs[5] =  0;
  ptb->set_range(rngs);

  // set block bc
  // x- bc
  rngs[0] =  0;  rngs[3] =  0;
  rngs[1] =  0;  rngs[4] =  NY-1;
  ptb->add_bc(0, "FarField", rngs);
  // x+ bc
  rngs[0] = NX-1;  rngs[3] = NX-1;
  rngs[1] =    0;  rngs[4] = NY-1;
  ptb->add_bc(3, "FarField", rngs);
  // y- bc 1
  rngs[0]   = 0;     rngs[3]   = NX-1;
  rngs[1]   = 0;     rngs[4]   = 0;
  ptb->add_bc(1, "FarField", rngs);
  // y+ bc
  rngs[0] = 0;     rngs[3] = NX-1;
  rngs[1] = NY-1;  rngs[4] = NY-1;
  ptb->add_bc(4, "FarField", rngs);

  // save a copy
  //BlockGraph grid(geo);
  geo.setup_msg_load(0.1, 10000.0, 8);

  // set coordinates for geo
  double dx = LX / (double)(NX-1);
  double dy = LY / (double)(NY-1);
  ptb->get_range(rngs);
  ptb->alloc_coord();
  for(int i=rngs[0]-1; i<=rngs[3]+1; i++){
    for(int j=rngs[1]-1; j<=rngs[4]+1; j++){
      for(int k=rngs[2]-1; k<=rngs[5]+1; k++){
        double coords[3];
        coords[0] = i * dx;
        coords[1] = j * dy;
        coords[2] = 0.0;
        ptb->set_coord(i, j, k, coords);
      }
    }
  }

  // cut the block unevenly
  BlockCut cut;
  cut.blkID  =  0;
  cut.axis   =  1;
  cut.pos    = NY/2;
  cut.isKept = true;
  geo.cut_block(cut);
  cut.blkID  =  0;
  cut.axis   =  0;
  cut.pos    = NX/4;
  geo.cut_block(cut);
  cut.blkID  =  1;
  cut.axis   =  0;
  cut.pos    = 3*NX/4;
  geo.cut_block(cut);

  // set partition
  for(int i=0; i<4; i++){
    geo.get_block_pt(i, &ptb);
    ptb->set_partition(i);
  }

  // find partition shift
  BlockPart bp(4);
  bp.adjust(geo);
  bp.merge_blk_part(geo);
  geo.view();

  bp.init_part_prop();
  bp.eval_part_prop(geo);
  bp.view_part_prop();


  // write the decomp file
  //grid.fwrite_mesh_tecplot("decomp.plt");

  return(0);
}
