#include "PartMethod.h"

using namespace std;

int PartMethod::shift_partition_id(BlockGraph& grid, vector<int> bids, int iShift)
{
  for(unsigned int i=0; i<bids.size(); i++){
    grid[bids[i]].set_partition(grid[bids[i]].partition_id() + iShift);
  }

  return(0);
}
