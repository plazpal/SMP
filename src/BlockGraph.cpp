#include "BlockGraph.h"

using namespace std;

/************************      BlockGraph Class        ************************/
/************************          public              ************************/

BlockGraph::BlockGraph(const int nBlock)
{
  alpha       = 0.001;
  beta        = 10000.0;
  sizeCellMsg = 1;
  nHalo       = 2;
  _tPerCell   = 1.0e-6;

  Block blk;
  for(int iBlk=0; iBlk<nBlock; iBlk++){
    blk.set_blockID(iBlk);
    blk.set_parentID(iBlk);
    blks.push_back(blk);
  }
}


BlockGraph::BlockGraph(const BlockGraph& rhs)
{
  blks        = rhs.blks;
  alpha       = rhs.alpha;
  beta        = rhs.beta;
  nHalo       = rhs.nHalo;
  sizeCellMsg = rhs.sizeCellMsg;
  _tPerCell   = rhs._tPerCell;
}


const BlockGraph& BlockGraph::operator=(const BlockGraph& rhs)
{
  if(this != &rhs)
  {
    blks        = rhs.blks;
    alpha       = rhs.alpha;
    beta        = rhs.beta;
    nHalo       = rhs.nHalo;
    sizeCellMsg = rhs.sizeCellMsg;
    _tPerCell   = rhs._tPerCell;
  }

  return(*this);
}


Block& BlockGraph::operator[](int i)
{
  assert((unsigned int)i < blks.size() && i >= 0);
  return(blks[i]);
}


void BlockGraph::get_block_pt(int blkID, Block** ptBlk)
{
  *ptBlk = &(blks[blkID]);
}


int BlockGraph::work_load()
{
  int work = 0;
  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++)
    work += blks[iBlk].work_load();

  return(work);
}


double BlockGraph::work()
{
  double work = 0;
  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++)
    work += blks[iBlk].work();

  return(work);
}


int BlockGraph::resize(const int nBlock)
{
  // remove current blocks
  unsigned int nBlk = blks.size();
  for(unsigned int i=nBlk; i>0; i--)
    blks.pop_back();

  // push new blocks
  Block blk;
  for(int i=0; i<nBlock; i++){
    blk.set_blockID(i);
    blk.set_parentID(i);
    blks.push_back(blk);
  }

  return(0);
}


int BlockGraph::view()
{
  cout << "\n Block Graph Info" << endl;
  cout << "# Blocks " << blks.size() << endl;

  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++){
    cout << "----------------" << endl;
    blks[iBlk].view();
  }

  return(0);
}


int BlockGraph::copy_coord(const BlockGraph& bg)
{
  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++) {
    int iPrnt = blks[iBlk].parent();
    blks[iBlk].alloc_coord();
    blks[iBlk].copy_coord(bg.blks[iPrnt]);
  }

  return(0);
}


int BlockGraph::fread_mesh_plot3d(const string& fname, bool isReadCoord)
{
  ifstream input(fname);

  // get # blocks
  int nBlk;
  input >> nBlk;

  // remove old blocks if any
  for(unsigned int i = 0; i < blks.size(); i++)
    blks.pop_back();

  // add empty blocks
  Block blk;
  for(int iBlk=0; iBlk<nBlk; iBlk++){
    blk.set_blockID(iBlk);
    blk.set_parentID(iBlk);
    blks.push_back(blk);
  }

  // read block index ranges
  for(int iBlk=0; iBlk<nBlk; iBlk++){
    int rngs[6] = {0, 0, 0, 0, 0, 0};
    input >> rngs[3] >> rngs[4] >> rngs[5];
    for(int i=3; i<6; i++)
      rngs[i]--;
    blks[iBlk].set_range(rngs);
  }

  // alloc and read coordinates
  if(isReadCoord){
    for(int iBlk=0; iBlk<nBlk; iBlk++){
      int rngs[6], nHalos[6];
      blks[iBlk].get_range(rngs);
      blks[iBlk].get_num_halo(nHalos);
      blks[iBlk].alloc_coord();
      //...
      int     ySize = rngs[4] - rngs[1] + 1 + nHalos[4] + nHalos[1];
      int     zSize = rngs[5] - rngs[2] + 1 + nHalos[5] + nHalos[2];
      double *coords;
      //...
      for(int iDir=0; iDir<3; iDir++){
        blks[iBlk].get_coord_pt(iDir, &coords);
        //...
        for(int k=rngs[2]; k<=rngs[5]; k++){
          for(int j=rngs[1]; j<=rngs[4]; j++){
            for(int i=rngs[0]; i<=rngs[3]; i++){
              int idx = (k - (rngs[2] - nHalos[2])) \
                      + (j - (rngs[1] - nHalos[1])) * zSize \
                      + (i - (rngs[0] - nHalos[0])) * ySize * zSize;
              input >> coords[idx];
            }
          }
        }
      }
    }
  }

  return(0);
}


int BlockGraph::fread_map(const string& fname)
{
  ifstream input(fname);
  string line;

  // skip the title line.
  getline(input, line);

  // set bcs
  while(getline(input, line)){
    // read bc type
    string bcStr;
    istringstream strIn(line);
    getline(strIn, bcStr, ' ');

    // read block, face ID
    int blkID, fcID;
    strIn >> blkID >> fcID;
    blkID--;
    fcID--;

    // set the default face ranges, which is whole face
    int fcDir, dirs[2], rngs[6];
    fcDir   = fcID % 3;
    dirs[0] = EdgeMap3d[fcDir][0];
    dirs[1] = EdgeMap3d[fcDir][1];
    blks[blkID].get_range(rngs);
    rngs[fcDir]   = rngs[fcID];
    rngs[fcDir+3] = rngs[fcID];

    // check the bc type
    // note that all indices in file starts from 1, in block class from 0
    if(bcStr.compare("Block2Block") == 0){
      // set face range in this map
      strIn >> rngs[dirs[0]];   rngs[dirs[0]]--;
      strIn >> rngs[dirs[0]+3]; rngs[dirs[0]+3]--;
      strIn >> rngs[dirs[1]];   rngs[dirs[1]]--;
      strIn >> rngs[dirs[1]+3]; rngs[dirs[1]+3]--;
      // set targe block face
      int toBlkID, toFcID, toFcDir, toDirs[2], toRngs[6];
      strIn >> toBlkID >> toFcID;
      toBlkID--;
      toFcID--;
      // set target face range
      toFcDir   = toFcID % 3;
      toDirs[0] = EdgeMap3d[toFcDir][0];
      toDirs[1] = EdgeMap3d[toFcDir][1];
      //...
      blks[toBlkID].get_range(toRngs);
      toRngs[toFcDir]   = toRngs[toFcID];
      toRngs[toFcDir+3] = toRngs[toFcID];
      strIn >> toRngs[toDirs[0]];   toRngs[toDirs[0]]--;
      strIn >> toRngs[toDirs[0]+3]; toRngs[toDirs[0]+3]--;
      strIn >> toRngs[toDirs[1]];   toRngs[toDirs[1]]--;
      strIn >> toRngs[toDirs[1]+3]; toRngs[toDirs[1]+3]--;
      // flip directions or not
      string flipStr;
      strIn >> ws >> flipStr;
      bool   isFlip = (flipStr.compare("yes") == 0);
      // add bc
      blks[blkID].add_bc(fcID, bcStr, rngs, toBlkID, toFcID, toRngs, isFlip);
      // add reverse mapping
      for(int i=0; i<2; i++){
        if(toRngs[toDirs[i]] > toRngs[toDirs[i]+3]){
          swap(toRngs[toDirs[i]], toRngs[toDirs[i]+3]);
          if(isFlip)
            swap(rngs[dirs[1-i]], rngs[dirs[1-i]+3]);
          else
            swap(rngs[dirs[i]], rngs[dirs[i]+3]);
        }
      }
      blks[toBlkID].add_bc(toFcID, bcStr, toRngs, blkID, fcID, rngs, isFlip);
    }
    // physical bcs
    else{
      strIn >> ws;
      if(!strIn.eof()){
        strIn >> rngs[dirs[0]];   rngs[dirs[0]]--;
        strIn >> rngs[dirs[0]+3]; rngs[dirs[0]+3]--;
        strIn >> rngs[dirs[1]];   rngs[dirs[1]]--;
        strIn >> rngs[dirs[1]+3]; rngs[dirs[1]+3]--;
      }
      blks[blkID].add_bc(fcID, bcStr, rngs);
    }
  }

  // update msg load of each block
  for(unsigned int i=0; i<blks.size(); i++)
    blks[i].cmpt_time(alpha, beta, sizeCellMsg, _tPerCell);

  return(0);
}


int BlockGraph::read_input(const GridInput& input)
{
  // remove old blocks if any
  for(uint i = 0; i < blks.size(); i++)
    blks.pop_back();

  // add blocks
  Block blk;
  for(int iBlk=0; iBlk<input._nBlk; iBlk++){
    // id and parent id
    blk.set_blockID(iBlk);
    blk.set_parentID(iBlk);
    // set block's ranges
    int rngs[6] = {0};
    memcpy(rngs+3, input._blkBuf+3*iBlk, 3*sizeof(int));
    for (int i=3; i<6; ++i)  --rngs[i];
    blk.set_range(rngs);
    // add block
    blks.push_back(blk);
  }

  // add maps
  for (int iMap=0; iMap<input._nMap; iMap++) {
    // bc Type
    string bcStr;
    if(input._mapBuf[14*iMap] == (int)BC_FARFIELD)     bcStr = "FarField";
    if(input._mapBuf[14*iMap] == (int)BC_VISCOUSWALL)  bcStr = "ViscousWall";
    if(input._mapBuf[14*iMap] == (int)BC_EULERWALL)    bcStr = "EulerWall";
    if(input._mapBuf[14*iMap] == (int)BC_SYMMETRY)     bcStr = "Symmetry";
    if(input._mapBuf[14*iMap] == (int)BC_BLOCK2BLOCK)  bcStr = "Block2Block";

    // set block, face dir
    int blkID, fcID, fcDir, dirs[2];
    blkID   = input._mapBuf[14*iMap+1]; 
    fcID    = input._mapBuf[14*iMap+2];
    fcDir   = fcID % 3;
    dirs[0] = EdgeMap3d[fcDir][0];
    dirs[1] = EdgeMap3d[fcDir][1];

    // map range
    int rngs[6];
    blks[blkID].get_range(rngs);
    rngs[fcDir]   = rngs[fcID];
    rngs[fcDir+3] = rngs[fcID];
    if(input._mapBuf[14*iMap+3] != INDEX_NULL){
      rngs[dirs[0]]   = input._mapBuf[14*iMap+3];
      rngs[dirs[0]+3] = input._mapBuf[14*iMap+4];
      rngs[dirs[1]]   = input._mapBuf[14*iMap+5];
      rngs[dirs[1]+3] = input._mapBuf[14*iMap+6];
    }

    // b2b map
    if(input._mapBuf[14*iMap] == (int)BC_BLOCK2BLOCK){
      // set target block, face dir
      int toBlkID, toFcID, toFcDir, toDirs[2], toRngs[6];
      toBlkID   = input._mapBuf[14*iMap+7];
      toFcID    = input._mapBuf[14*iMap+8];
      toFcDir   = toFcID % 3;
      toDirs[0] = EdgeMap3d[toFcDir][0];
      toDirs[1] = EdgeMap3d[toFcDir][1];
      // target range
      blks[toBlkID].get_range(toRngs);
      toRngs[toFcDir]     = toRngs[toFcID];
      toRngs[toFcDir+3]   = toRngs[toFcID];
      toRngs[toDirs[0]]   = input._mapBuf[14*iMap+ 9];
      toRngs[toDirs[0]+3] = input._mapBuf[14*iMap+10];
      toRngs[toDirs[1]]   = input._mapBuf[14*iMap+11];
      toRngs[toDirs[1]+3] = input._mapBuf[14*iMap+12];
      // flip
      bool isFlip = (input._mapBuf[14*iMap+13] == 1);
      // add bc
      blks[blkID].add_bc(fcID, bcStr, rngs, toBlkID, toFcID, toRngs, isFlip);
      // add reverse mapping
      for(int i=0; i<2; i++){
        if(toRngs[toDirs[i]] > toRngs[toDirs[i]+3]){
          swap(toRngs[toDirs[i]], toRngs[toDirs[i]+3]);
          if(isFlip)
            swap(rngs[dirs[1-i]], rngs[dirs[1-i]+3]);
          else
            swap(rngs[dirs[i]], rngs[dirs[i]+3]);
        }
      }
      blks[toBlkID].add_bc(toFcID, bcStr, toRngs, blkID, fcID, rngs, isFlip);
    }
    // non b2b map
    else{
      blks[blkID].add_bc(fcID, bcStr, rngs);
    }
  }

  // update msg load of each block
  for(uint i=0; i<blks.size(); i++)
    cmpt_time(i);

  return(0);
}


int BlockGraph::fwrite_mesh_tecplot(const string& fname)
{
  ofstream output(fname);

  // tecplot header
  output << "TITLE = \"Mesh Per Proc\"" << endl;
  output << "VARIABLES = \"X\", \"Y\", \"Z\", \"Proc\", \"BlockID\"" << endl;

  // output each block's coordinates
  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++){
    int rngs[6];
    blks[iBlk].get_range(rngs);
    //rngs[0] = max(0, rngs[0]-1);
    //rngs[1] = max(0, rngs[1]-1);
    //rngs[2] = max(0, rngs[2]-1);

    // zone title
    int sizes[3];
    for(int i=0; i<3; i++)
      sizes[i] = rngs[i+3] - rngs[i] + 1;
    int partID;
    blks[iBlk].get_partition(partID);
    output << "ZONE T=\"Proc " << partID
           << "\", I=" << sizes[0]
           << ", J="  << sizes[1]
           << ", K="  << sizes[2]
           << ", DATAPACKING=POINT" << endl;
    output << "ZONETYPE=Ordered" << endl;

    //output coordinates
    for( int k=rngs[2]; k<=rngs[5]; k++){
      for( int j=rngs[1]; j<=rngs[4]; j++){
        for( int i=rngs[0]; i<=rngs[3]; i++){
          double coords[3];
          blks[iBlk].get_coord(i, j, k, coords);
          output << coords[0] << " " << coords[1] << " " << coords[2] << " " 
                 << partID <<  " " << iBlk << endl;
        }
      }
    }
  }

  return(0);
}


int BlockGraph::setup_msg_load(double a, double b, int scm)
{
  alpha       = a;
  beta        = b;
  sizeCellMsg = scm;

  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++)
    blks[iBlk].cmpt_msg_load(alpha, beta, sizeCellMsg);

  return(0);
}


int BlockGraph::cmpt_msg_load(int bid)
{
  blks[bid].ltncy   = 0.0;
  blks[bid].msgLoad = 0.0;

  for(int i=0; i<6; i++){
    BlockFace *ptFc = blks[bid].face_pt(i);
    MapIter    iter = ptFc->mapList.begin();
    ptFc->msgLoad   = 0.0;
    while(iter != ptFc->mapList.end()){
      // set shmem map
      if(iter->bcType.compare("Block2Block") == 0){
        int pidFrom   = blks[bid].partition_id();
        int pidTo     = blks[iter->toBlk].partition_id();
        iter->isShMem = (pidFrom == pidTo && pidFrom != INDEX_NULL);
        // skip shmem map 
        if(!iter->isShMem){
          ptFc->msgLoad  += alpha;
          ptFc->msgLoad  += max(iter->rngs[3] - iter->rngs[0], 1) \
                          * max(iter->rngs[4] - iter->rngs[1], 1) \
                          * max(iter->rngs[5] - iter->rngs[2], 1) \
                          * ptFc->nHalo * sizeCellMsg / beta;
          blks[bid].ltncy += alpha;
        }
      }
      iter++;
    }
    blks[bid].msgLoad += ptFc->msgLoad;
  }

  return(0);
}


double BlockGraph::time_comm(int bid, int fid, int pos, const vector<int>& cmpnys)
{
  double tComm = blks[bid].time_comm(fid, pos, alpha, beta, sizeCellMsg);
  
  // remove the communiation to cmpnys
  BlockCut cut;
  if(cmpnys.size() > 0){
    for(uint i=0; i<cmpnys.size(); i++){
      cut.isKept = (fid >= 3);
      cut.axis   = fid % 3;
      cut.pos    = pos;
      cut.blkID  = bid;
      tComm -= msg_load(cut, cmpnys[i]);
    }
  }

  return(tComm);
}


int BlockGraph::cmpt_time(int bid)
{
  cmpt_msg_load(bid);
  
  int rngs[6];
  blks[bid].get_range(rngs);
  blks[bid]._tComp = (rngs[3] - rngs[0]) * (rngs[4] - rngs[1]) \
                   * max(rngs[5] - rngs[2], 1) * _tPerCell;
  blks[bid]._t     = blks[bid]._tComp + blks[bid].msgLoad;

  return(0);
}


int BlockGraph::cmpt_nbr_time(int bid)
{
  for(int i=0; i<6; i++){
    BlockFace *ptFc = blks[bid].face_pt(i);
    MapIter    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0)  cmpt_time(iter->toBlk);
      iter++;
    }
  }
  return(0);
}


int BlockGraph::reset_parent()
{
  for(unsigned int iBlk=0; iBlk<blks.size(); iBlk++)
    blks[iBlk].set_parentID(iBlk);

  return(0);
}


int BlockGraph::find_min_blkcut(int id, double load, double err, BlockCut& cut, \
                                const vector<int>& cmpnys)
{
  int      rngs[6];
  BlockCut cutTmp(DLARGE), cutDir(DLARGE); // cut used to find min cut per dir

  blks[id].get_range(rngs);
  cut.reset(); cut.msgIncr = DLARGE;

  for(int i=0; i<3; i++){
    // parallel faces is the 4 planes parallel with current i
    // e.g. i~x, prllFcs are y-, y+, z-, z+ faces
    int prllFcs[4];
    prllFcs[0] = EdgeMap3d[i][0];  prllFcs[2] = prllFcs[0] + 3;
    prllFcs[1] = EdgeMap3d[i][1];  prllFcs[3] = prllFcs[1] + 3;

    // find the allowed cut position
    int area = max(rngs[prllFcs[2]] - rngs[prllFcs[0]], 1) \
             * max(rngs[prllFcs[3]] - rngs[prllFcs[1]], 1);
    // if the area > load, no way to cut in this axis
    // if all faces are huge, cut will be not found
    if((double)area > load*(1.0+err))  continue;

    for(int ii=0; ii<2; ii++){
      // before calling this function, caller make sure that block size is 
      // larger than load+err, posFlr, posCl not exceed block range
      int posFlr, posCl, posMin;
      if(ii == 0){ // cut from start
        //posFlr = (int)floor(load * (1.0-err) / area) + rngs[i];
        //posCl  = (int)ceil( load * (1.0+err) / area) + rngs[i];
        posFlr = (int)ceil(load * (1.0-err) / area) + rngs[i];
        posCl  = (int)floor( load * (1.0+err) / area) + rngs[i];
        posMin = (int)round(load / area) + rngs[i];
      }
      else{ // cut from end
        //posFlr = rngs[i+3] - (int)ceil( load * (1.0+err) / area);
        //posCl  = rngs[i+3] - (int)floor(load * (1.0-err) / area);
        posFlr = rngs[i+3] - (int)floor( load * (1.0+err) / area);
        posCl  = rngs[i+3] - (int)ceil(load * (1.0-err) / area);
        posMin = rngs[i+3] - (int)round(load / area);
      }
      // avoid cutting face collapse on block's boundary
      posFlr = max(posFlr, rngs[i]  +nHalo);
      posCl  = min(posCl,  rngs[i+3]-nHalo);
      // skip the rest if no position can be found
      if(posFlr > posCl) continue;

      // default min cut in this dir
      if(posMin < posFlr || posMin > posCl) posMin = (posFlr + posCl) / 2;
      cutDir.set(id, i, posMin, DLARGE, (ii==1));
      cmpt_msg_incr(cutDir, cmpnys);
      // find the min cut in this direction.
      for(int pos=posFlr; pos<=posCl; pos++){
        cutTmp.set(id, i, pos, DLARGE, (ii==1));
        cmpt_msg_incr(cutTmp, cmpnys);
        if(cutTmp < cutDir) cutDir = cutTmp;
      }
      // find the min cut
      if(cutDir < cut)  cut = cutDir;
    }// end ii
  }// end i

  // if cut not found, set the msgIncr to huge value
  if(cut.blkID == BLOCK_NULL)  cut.msgIncr = DLARGE;

  return(0);
}


int BlockGraph::find_time_cut(int id, double tLoad, double err, BlockCut& cut, \
                              const vector<int>& cmpnys)
{
  int      rngs[6];
  double   load  = tLoad / _tPerCell;
  BlockCut cutTmp(DLARGE);

  blks[id].get_range(rngs);
  cut.reset();
  cut.msgIncr = DLARGE;

  for(int i=0; i<3; i++){
    // parallel faces is the 4 planes parallel with current i
    // e.g. i~x, prllFcs are y-, y+, z-, z+ faces
    int prllFcs[4];
    prllFcs[0] = EdgeMap3d[i][0];  prllFcs[2] = prllFcs[0] + 3;
    prllFcs[1] = EdgeMap3d[i][1];  prllFcs[3] = prllFcs[1] + 3;

    // find the allowed cut position
    int area = max(rngs[prllFcs[2]] - rngs[prllFcs[0]], 1) \
             * max(rngs[prllFcs[3]] - rngs[prllFcs[1]], 1);
    // if the area > load, no way to cut in this axis
    // if all faces are huge, cut will be not found
    if((double)area > load*(1.0+err))  continue;

    for(int ii=0; ii<2; ii++){
      // cut basics
      cutTmp.blkID   = id;
      cutTmp.axis    = i;
      cutTmp.isKept  = (ii != 0);

      // before calling this function, caller make sure that block size is 
      // larger than load+err, posFlr, posCl not exceed block range
      int    posFlr, posCl;
      double tComm, tComp;
      if(ii == 0){ // cut from start
        posCl  = min((int)ceil(load*(1.0+err)/area) + rngs[i], rngs[i+3]-1);
        tComm  = blks[id].time_comm(i, posCl, alpha, beta, sizeCellMsg);
        posFlr = (int)floor(max((load*(1.0-err)-tComm/_tPerCell)/area, 1.0)) + rngs[i];
      }
      else{ // cut from end
        posFlr = max(rngs[i+3] - (int)ceil(load*(1.0+err)/area), rngs[i]+1);
        tComm  = blks[id].time_comm(i+3, posFlr, alpha, beta, sizeCellMsg);
        posCl  = rngs[i+3] - (int)floor(max((load*(1.0-err)-tComm/_tPerCell)/area, 1.0));
      }
      // search between posFlr, posClr
      for(int pos=posFlr; pos<=posCl; pos++){
        // communication time
        int fid = (ii == 0) ? i : i+3;
        tComm = time_comm(id, fid, pos);
        // remove the communication to cmpnys
        if(cmpnys.size() > 0){
          for(uint j=0; j<cmpnys.size(); j++){
            cutTmp.pos = pos;
            tComm -= 2 * msg_load(cutTmp, cmpnys[j]);
          }
        }
        // computation time
        int len = (ii == 0) ? pos - rngs[i] : rngs[i+3] - pos;
        tComp = len * area * _tPerCell;
        // check if time stays in range
        if(tComp + tComm <= tLoad*(1.0+err) && tComp + tComm >= tLoad*(1.0-err)){
          // compare and save cut
          cutTmp.pos = pos;
          cmpt_msg_incr(cutTmp, cmpnys);
          if(cutTmp < cut)  cut = cutTmp;
        }
      }
    }// end ii
  }// end i

  return(0);
}


int BlockGraph::find_min_blkdiv(int bid, int nPart, int nBlks[3], double& tCommMax)
{
  if(nPart == 1){
    fill(nBlks, nBlks+3, 1);
    tCommMax = blks[bid].msg_load();
    return(0);
  }

  int rngs[6], n[3];
  uint size0;
  double tComm;
  vector<Block> blks0 = blks;
  vector< vector<int> > cutLocs(3);

  blks[bid].get_range(rngs);

  tCommMax = DLARGE;

  for(n[0]=1; n[0]<=nPart; n[0]++){
    if(nPart%n[0] != 0 || n[0] >= blks[bid].length(0)) continue;
    //..
    for(n[1]=1; n[1]<=(nPart/n[0]); n[1]++){
      if(nPart/n[0] % n[1] != 0 || n[1] >= blks[bid].length(1)) continue;
      //..
      n[2] = nPart/n[0]/n[1];
      if(n[2] >= blks[bid].length(2)) continue;
      // skip if the divison is too small for halo
      if(blks[bid].length(0)/n[0] < nHalo || blks[bid].length(0)/n[0] < nHalo || \
         blks[bid].length(2)/n[2] < nHalo)
        continue;
      // set cut position
      cutLocs[0].clear();
      cutLocs[1].clear();
      cutLocs[2].clear();
      for(int i=0; i<3; i++){
        int iCut = rngs[i];
        for(int j=1; j<n[i]; j++){
          if(j <= blks[bid].length(i)%n[i])
            iCut += blks[bid].length(i)/n[i] + 1;
          else
            iCut += blks[bid].length(i)/n[i];
          cutLocs[i].push_back(iCut);
        }
      }
      // cut the block
      size0 = blks.size();
      cut_block(bid, cutLocs);
      tComm = blks[bid].msg_load();
      for(uint i=size0; i<blks.size(); i++)
        if(blks[i].msg_load() > tComm)  tComm = blks[i].msg_load();
      // find min for tCommMax
      if(tComm < tCommMax){
        tCommMax = tComm;
        nBlks[0] = n[0];
        nBlks[1] = n[1];
        nBlks[2] = n[2];
      }
      // restore graph
      blks = blks0;
    }
  }
  
  return(0);
}


int BlockGraph::find_min_blkdiv(BlockCut& cut, int nPart, int nBlks[3], double& tCommMax)
{
  vector<Block> blks00 = blks; // back up of graph
  
  cut_block(cut);
  int bidRmn = cut.isKept ? cut.blkID : (int)blks.size()-1;
  int bidCut = cut.isKept ? (int)blks.size()-1 : cut.blkID;
  vector<Block> blks0 = blks;  // back up of cut graph

  if(nPart == 1){
    fill(nBlks, nBlks+3, 1);
    tCommMax = max(blks[cut.blkID].msg_load(), blks.back().msg_load());
    blks = blks00;
    return(0);
  }

  int rngs[6], n[3];
  uint size0;
  double tComm;
  vector< vector<int> > cutLocs(3);

  blks[bidRmn].get_range(rngs);

  tCommMax = DLARGE;

  for(n[0]=1; n[0]<=nPart; n[0]++){
    if(nPart%n[0] != 0 || n[0] >= blks[bidRmn].length(0)) continue;
    //..
    for(n[1]=1; n[1]<=(nPart/n[0]); n[1]++){
      if(nPart/n[0] % n[1] != 0 || n[1] >= blks[bidRmn].length(1)) continue;
      //..
      n[2]  = nPart/n[0]/n[1];
      if(n[2] >= blks[bidRmn].length(2)) continue;
      // skip if the divison is too small for halo
      if(blks[bidRmn].length(0)/n[0] < nHalo || blks[bidRmn].length(0)/n[0] < nHalo || \
         blks[bidRmn].length(2)/n[2] < nHalo)
        continue;
      // set cut position
      cutLocs[0].clear();
      cutLocs[1].clear();
      cutLocs[2].clear();
      for(int i=0; i<3; i++){
        int iCut = rngs[i];
        for(int j=1; j<n[i]; j++){
          if(j <= blks[bidRmn].length(i)%n[i])
            iCut += blks[bidRmn].length(i)/n[i] + 1;
          else
            iCut += blks[bidRmn].length(i)/n[i];
          cutLocs[i].push_back(iCut);
        }
      }
      // cut the block
      size0 = blks.size();
      cut_block(bidRmn, cutLocs);
      tComm = max(blks[bidRmn].msg_load(), blks[bidCut].msg_load());
      for(uint i=size0; i<blks.size(); i++)
        if(blks[i].msg_load() > tComm)  tComm = blks[i].msg_load();
      // find min for tCommMax
      if(tComm < tCommMax){
        tCommMax = tComm;
        nBlks[0] = n[0];
        nBlks[1] = n[1];
        nBlks[2] = n[2];
      }
      // restore graph
      blks = blks0;
    }
  }

  // fully restore graph
  blks = blks00;
  
  return(0);
}


int BlockGraph::cut_block(BlockCut& cut)
{
  blks.push_back(blks[cut.blkID]);
  Block& blk0 = blks[cut.blkID];
  Block& blk1 = blks.back(); // new blk

  // set current block's range
  int rngs[6];
  blk0.get_range(rngs);
  rngs[cut.axis+3] = cut.pos; 
  blk0.set_range(rngs);

  // set new block's id, range
  blk1.get_range(rngs);
  rngs[cut.axis] = cut.pos; 
  blk1.set_range(rngs);
  blk1.set_blockID(blks.size()-1);

  // bc map between current and new block
  int    toRngs[6];
  blk0.clear_bc(cut.axis+3);
  blk0.get_range(rngs);
  rngs[cut.axis] = cut.pos;
  memcpy(toRngs, rngs, 6*sizeof(int));
  blk0.add_bc(cut.axis+3, "Block2Block", rngs, blk1.id(), cut.axis, toRngs, false );
  //..
  blk1.clear_bc(cut.axis);
  blk1.get_range(rngs);
  rngs[cut.axis+3] = cut.pos;
  memcpy(toRngs, rngs, 6*sizeof(int));
  blk1.add_bc(cut.axis, "Block2Block", rngs, cut.blkID, cut.axis+3, toRngs, false );

  // remove bc cut off
  blk0.cut_off_bc(0, cut);
  blk1.cut_off_bc(1, cut);

  // adjust maps on parallel faces
  int prllFcs[4];
  prllFcs[0] = EdgeMap3d[cut.axis][0];  prllFcs[2] = prllFcs[0] + 3;
  prllFcs[1] = EdgeMap3d[cut.axis][1];  prllFcs[3] = prllFcs[1] + 3;

  for(int j=0; j<4; j++){// parallel face
    // get face
    BlockFace *ptFc0, *ptFc1;
    blk0.get_face_pt(prllFcs[j], &ptFc0);
    blk1.get_face_pt(prllFcs[j], &ptFc1);
    // loop over maps to update map info
    list<BcMap>::iterator iter0 = ptFc0->mapList.begin(), iter1;
    while(iter0 != ptFc0->mapList.end()){ // blk0's face
      // map divide into 2
      if(cut.pos > iter0->rngs[cut.axis] && 
         cut.pos < iter0->rngs[cut.axis+3]){ // if: map cut
        // find the same map on blk1
        iter1 = ptFc1->map_iter(*iter0);
        // B2B bc
        if(iter0->bcType.compare("Block2Block") == 0){ // B2B
          // map to self
          if(iter0->toBlk == blk0.id()){
            // target range on cut axis
            int toStart = iter0->toRngs[cut.axis];
            int toEnd   = iter0->toRngs[cut.axis+3];
            if(toEnd < toStart)  swap(toEnd, toStart);
            // target range within blk1
            if(cut.pos <= toStart){
              // delete the inverse map on blk1
              iter0->toBlk = blk1.id();
              del_inv_map(blk0.id(), prllFcs[j], *iter0);
              // adjust blk0's map and add inverse to blk1
              ptFc0->adjust_map_range(cut.axis+3, cut.pos, *iter0);
              add_inv_map(blk0.id(), prllFcs[j], *iter0);
              // adjust blk1's map and add inverse to itself
              iter1->toBlk = blk1.id();
              ptFc1->adjust_map_range(cut.axis, cut.pos, *iter1);
              add_inv_map(blk1.id(), prllFcs[j], *iter1);
            }
            // target range cut
            else if(cut.pos < toEnd){
              ptFc0->adjust_map_range(cut.axis+3, cut.pos, *iter0);
              ptFc1->adjust_map_range(cut.axis,   cut.pos, *iter1);
              if(cut.pos <= iter0->toRngs[cut.axis] &&
                 cut.pos <= iter0->toRngs[cut.axis+3]){
                iter0->toBlk = blk1.id();
              }
              else if( (cut.pos - iter0->toRngs[cut.axis+3]) \
                      *(cut.pos - iter0->toRngs[cut.axis]) < 0){
                BcMap map = *iter0;
                ptFc0->adjust_map_target_range(cut.axis,   cut.pos, *iter0);
                ptFc0->adjust_map_target_range(cut.axis+3, cut.pos, map);
                ptFc0->mapList.push_back(map);
              }
              if(cut.pos >= iter1->toRngs[cut.axis] &&
                 cut.pos >= iter1->toRngs[cut.axis+3]){
                iter1->toBlk = blk0.id();
              }
              else if( (cut.pos - iter1->toRngs[cut.axis+3]) \
                      *(cut.pos - iter1->toRngs[cut.axis]) < 0){
                BcMap map = *iter1;
                ptFc1->adjust_map_target_range(cut.axis,   cut.pos, *iter1);
                ptFc1->adjust_map_target_range(cut.axis+3, cut.pos, map);
                ptFc1->mapList.push_back(map);
              }
            }
            // target range within blk0
            else{
              // delete the inverse map on blk0
              del_inv_map(blk0.id(), prllFcs[j], *iter0);
              // adjust blk0's map and add inverse to blk0
              ptFc0->adjust_map_range(cut.axis+3, cut.pos, *iter0);
              add_inv_map(blk0.id(), prllFcs[j], *iter0);
              // adjust blk1's map and add inverse to blk0
              ptFc1->adjust_map_range(cut.axis, cut.pos, *iter1);
              add_inv_map(blk1.id(), prllFcs[j], *iter1);
            }
          }
          // map to other block
          else{
            // delete the inverse map on target
            del_inv_map(blk0.id(), prllFcs[j], *iter0);
            // adjust blk0, blk1 map
            ptFc0->adjust_map_range(cut.axis+3, cut.pos, *iter0);
            ptFc1->adjust_map_range(cut.axis,   cut.pos, *iter1);
            // Add inverse map to target
            add_inv_map(blk0.id(), prllFcs[j], *iter0);
            add_inv_map(blk1.id(), prllFcs[j], *iter1);
          }
        }// end if: B2B
        // other bc
        else{
          iter0->rngs[cut.axis+3] = cut.pos;
          iter1->rngs[cut.axis]   = cut.pos;
        }
      }// end if: map cut
      iter0++;
    }// end while: blk0's face
  }// end for: parallel face

  // reset blk1's inverse map's target, some may still be blk0
  for(int i=0; i<6; i++){
    BlockFace *ptFc;
    blk1.get_face_pt(i, &ptFc);
    list<BcMap>::iterator iter1 = ptFc->mapList.begin();
    while(iter1 != ptFc->mapList.end()){
      if(iter1->bcType.compare("Block2Block") == 0){
        list<BcMap>::iterator iterInv = inv_map_iter(*iter1);
        if(iterInv->toBlk == blk0.id()) iterInv->toBlk = blk1.id();
      }
    iter1++;
    }
  }

  cmpt_time(blk0.id());
  cmpt_time(blk1.id());
  cmpt_nbr_time(blk0.id());
  cmpt_nbr_time(blk1.id());

#if verbose>=2
  cout << "BlockGraph: Cut block " << cut.blkID << " at " << cut.pos
       << " in " << cut.axis << endl;
  cout << "BlockGraph: "  << blks.size() << " blocks now" << endl;
#endif

  return(0);
}


int BlockGraph::cut_block(int bid, vector< vector<int> >& cutLocs)
{
  // variables in loop
  BlockCut cut;
  unsigned int nAppend = 0;           // # appended blocks
  unsigned int size0   = blks.size(); // save current graph size

  //cut blocks in x, y, z order in any cut is found
  for(int iDir=0; iDir<3; iDir++){
    if(cutLocs[iDir].size() > 0){
      sort(cutLocs[iDir].begin(), cutLocs[iDir].end(), greater<int>());
      for(unsigned int iCut=0; iCut<cutLocs[iDir].size(); iCut++){
        cut.blkID  = bid;
        cut.axis   = iDir;
        cut.pos    = cutLocs[iDir][iCut];
        cut.isKept = true;
        cut_block(cut);
        for(unsigned int iApnd=0; iApnd<nAppend; iApnd++){
          cut.blkID = size0 + iApnd;
          cut_block(cut);
        }
      }
    }
    // save # appended blocks
    nAppend = blks.size() - size0;
  }
  
  return(0);
}


int BlockGraph::cut_block(int bid, int nSubBlk)
{
  assert(nSubBlk > 1);

  // find # single cut and division of the rest
  // nSubBlk = d + a*b*c
  int d, abc[3];
  _find_best_cuts(bid, nSubBlk, d, abc);
#if verbose>=1
  cout << "BlockGraph: " << nSubBlk << " divided into " << d << " + " << abc[0]
       << "*" << abc[1] << "*" << abc[2] << endl;
#endif

  // average work, this may change for sub block so fix it here
  double wkAvg = (double)blks[bid].work_load() / nSubBlk;

  // variables in loop
  int rngs[6], lens[3];
  BlockCut cut;
  cut.blkID = bid;
  
  // perform single cut, do nothing if d = 0
  for(int i=0; i<d; i++){
    // range and length of block
    blks[cut.blkID].get_range(rngs);
    for(int j=0; j<3; j++)
      lens[j] = rngs[j+3] - rngs[j];
    // sort direction by length
    int dirMax, dirMid, dirMin;
    blks[cut.blkID].get_axes_sorted(dirMax, dirMid, dirMin);
    // set cut
    int lenCut = (int)round(wkAvg / lens[dirMid] / lens[dirMin]);
    cut.axis   = dirMax;
    cut.pos    = rngs[dirMax] + lenCut;
    cut.isKept = false;
    cut_block(cut);
    // set blkID to the remaining block
    cut.blkID  = (int)blks.size()-1;
  }

  // cut the rest block, cut.blkID is set in previous loop or init
  // remaining block to cut
  blks[cut.blkID].get_range(rngs);
  for(int j=0; j<3; j++)
    lens[j] = rngs[j+3] - rngs[j];
  // find cut locations
  vector< vector<int> > cutLocs(3);
  for(int i=0; i<3; i++){
    int qtnt  = lens[i] / abc[i];
    int rmndr = lens[i] % abc[i];
    int loc   = rngs[i];
    for(int j=0; j<abc[i]-1; j++){
      if(j < rmndr){
        loc += qtnt + 1;
        cutLocs[i].push_back(loc);
      }
      else{
        loc += qtnt;
        cutLocs[i].push_back(loc);
      }
    }
  }
  // cut the block
  cut_block(cut.blkID, cutLocs);

  return(0);
}


int BlockGraph::reb(int bid, int npREB, double toler)
{
  uint        size0   = blks.size();
  uint        nNewBlk = 0;
  vector<int> npNewBlks;

#if verbose>=2
  cout << "BlockGraph: REB block " << bid << " begin." << endl;
#endif

  // bisect id until it fits in one proc
  _bisect_to_1_block(bid, npREB, toler, npNewBlks);

  while (blks.size() > size0 + nNewBlk) {
    _bisect_to_1_block(size0+nNewBlk, npNewBlks[nNewBlk], toler, npNewBlks);
    nNewBlk++;
  }
#if verbose>=2
  cout << "BlockGraph: REB block " << bid << " end." << endl;
#endif

  return(0);
}


int BlockGraph::factorize_block(int bid, vector<int>& preBids, int np, double toler)
{
  assert(np >= 1);

  if (np == 1) return 0;

  // save the current graph before m + n[3] division

  // check different division m + n[0]*n[1]*n[2] of the main block.
  // choose the division of the min communication cost among new blocks and residue block
  BlockCut cut;
  double wkAvg = blks[bid].work() / np;
  double tCommMax = DLARGE, tCommSumMax = DLARGE, tComm = DLARGE, tCommSum;
  int    m = INDEX_NULL, n[3], nBest[3]; // division of blocks
  int    bidDiv, bidSngl;     // block id of single and divisable part
  int    rngs[6];             // block ranges
  vector< vector<int> > cutLocs(3);
  vector<Block> blks00 = blks;
  
  //..
  for (int iMthd=0; iMthd<2; ++iMthd) {
    bidSngl = INDEX_NULL; // block id for single part bigSngl
    bidDiv  = bid;        // block id for divisible part n[3]
    vector<Block> blks0 = blks;
    int npDiv = np;

    // try to cut as 1 + n0*n1*n2 first
    if (iMthd == 0) {
      find_min_blkcut(bid, wkAvg, toler, cut);
      if (cut.blkID != BLOCK_NULL) {
        cut_block(cut);
        bidSngl = (cut.isKept) ? (int)blks.size()-1 : bid;
        bidDiv  = (cut.isKept) ? bid : (int)blks.size()-1;
        npDiv   = np - 1;
        blks0   = blks;
      }
    }
    // check which division gives the min communication
    for(n[0]=1; n[0]<=npDiv; ++n[0]){
      if(npDiv%n[0] != 0 || n[0] >= blks[bidDiv].length(0)) continue;
      //..
      for(n[1]=1; n[1]<=(npDiv/n[0]); ++n[1]){
        if(npDiv/n[0] % n[1] != 0 || n[1] >= blks[bidDiv].length(1)) continue;
        //..
        n[2] = npDiv/n[0]/n[1];
        if(n[2] >= blks[bidDiv].length(2)) continue;
        // skip if the divison is too small for halo
        if(blks[bidDiv].length(0)/n[0] < nHalo || blks[bidDiv].length(0)/n[0] < nHalo || \
           blks[bidDiv].length(2)/n[2] < nHalo)
          continue;
        // set cut position
        blks[bidDiv].get_range(rngs);
        cutLocs[0].clear();
        cutLocs[1].clear();
        cutLocs[2].clear();
        for(int i=0; i<3; i++){
          int iCut = rngs[i];
          for(int j=1; j<n[i]; j++){
            if(j <= blks[bidDiv].length(i)%n[i])
              iCut += blks[bidDiv].length(i)/n[i] + 1;
            else
              iCut += blks[bidDiv].length(i)/n[i];
            cutLocs[i].push_back(iCut);
          }
        }
        // cut the block
        uint size0 = blks.size();
        cut_block(bidDiv, cutLocs);
        // find the max time and time sum amount divied blocks
        tCommSum = blks[bidDiv].msg_load();
        tComm    = blks[bidDiv].msg_load();
        for (uint i=size0; i<blks.size(); i++) {
          if (blks[i].msg_load() > tComm)  tComm = blks[i].msg_load();
          tCommSum += blks[i].msg_load();
        }
        // compare time with single block
        if (bidSngl != INDEX_NULL) {
          tCommSum += blks[bidSngl].msg_load();
          if (blks[bidSngl].msg_load() > tComm) tComm = blks[bidSngl].msg_load();
        }
        // compare time with previous generated blocks
        for (uint i=0; i<preBids.size(); ++i) {
          tCommSum += blks[preBids[i]].msg_load();
          if (blks[preBids[i]].msg_load() > tComm) tComm = blks[preBids[i]].msg_load();
        }
        // find min for tCommMax
        if (tComm < tCommMax - DSMALL || (tComm < tCommMax+DSMALL && tCommSum < tCommSumMax)) {
          tCommMax    = tComm;
          tCommSumMax = tCommSum;
          nBest[0]    = n[0];
          nBest[1]    = n[1];
          nBest[2]    = n[2];
          m           = (bidSngl != INDEX_NULL ? 1 : 0);
        }
        // restore graph before dividing
        blks = blks0;
      }
    }
    // restore graph before any cut
    if (iMthd == 0) blks = blks00;
  }

  // cut the block
  if (m == 1) {
    find_min_blkcut(bid, wkAvg, toler, cut);
    cut_block(cut);
    bidDiv  = (cut.isKept) ? bid : (int)blks.size()-1;
    bidSngl = (cut.isKept) ? (int)blks.size()-1 : bid;
    preBids.push_back(bidSngl);
    --np;
    factorize_block(bidDiv, preBids, np, toler);
#if verbose>=1
    cout << "BlockGraph: recursively cut block " << bid << " into divisable block "
         << bidDiv << " single block " << bidSngl << endl;
#endif
  }
  else if (m == 0) {
    blks[bid].get_range(rngs);
    cutLocs[0].clear();
    cutLocs[1].clear();
    cutLocs[2].clear();
    for(int i=0; i<3; i++){
      int iCut = rngs[i];
      for(int j=1; j<nBest[i]; j++){
        if(j <= blks[bid].length(i)%nBest[i])
          iCut += blks[bid].length(i)/nBest[i] + 1;
        else
          iCut += blks[bid].length(i)/nBest[i];
        cutLocs[i].push_back(iCut);
      }
    }
    cut_block(bid, cutLocs);
#if verbose>=1
    cout << "BlockGraph: cut block " << bid << " into " << nBest[0] << " "
         << nBest[1] << " " << nBest[2] << " sub-blocks."<< endl;
#endif
  }
  else {
    cout << "PureGreedyCut: Fail to find factorization of " << np << " for block " << bid << endl;
    exit(-1);
  }

  return 0;
}


int BlockGraph::find_time_bisect(int bid, double r, double e, BlockCut& cut)
{
  int      posFlr, posCl, rngs[6], pFcs[4], area;
  double   tCut, tRmn, dt, rErr = DLARGE;
  BlockCut cutTmp(DLARGE);

  cut = cutTmp;

  blks[bid].get_range(rngs);

  for(int i=0; i<6; i++){
    rErr = DLARGE;

    // set cut basics
    cutTmp.axis   = i%3;
    cutTmp.blkID  = bid;
    cutTmp.isKept = (i >= 3);

    // parallel faces, normal face area
    pFcs[0] = EdgeMap3d[i%3][0];  pFcs[2] = pFcs[0] + 3;
    pFcs[1] = EdgeMap3d[i%3][1];  pFcs[3] = pFcs[1] + 3;
    area    = (rngs[pFcs[3]] - rngs[pFcs[1]]) * (rngs[pFcs[2]] - rngs[pFcs[0]]);

    // starts from x-, y-, z-
    if(i < 3){
      // set up search range posFlr - posCl
      //.. floor
      posFlr   = rngs[i] + (int)floor(blks[bid].length(i) * r*(1.0-e));
      tCut     = blks[bid].time_comm(i,   posFlr, alpha, beta, sizeCellMsg);
      tCut    += area * (posFlr - rngs[i]) * _tPerCell;
      tRmn     = blks[bid].time_comm(i+3, posFlr, alpha, beta, sizeCellMsg);
      tRmn    += area * (rngs[i+3] - posFlr) * _tPerCell;
      if(tCut/(tRmn+tCut) >= r*(1.0-e)){
        dt     = tCut - (tRmn+tCut)*r*(1.0-e);
        posFlr = max(rngs[i]+1, posFlr - (int)ceil(dt/_tPerCell/area));
      }
      //.. ceil
      posCl    = rngs[i] + (int)ceil(blks[bid].length(i) * r*(1.0+e));
      tCut     = blks[bid].time_comm(i,   posCl, alpha, beta, sizeCellMsg);
      tCut    += area * (posCl - rngs[i]) * _tPerCell;
      tRmn     = blks[bid].time_comm(i+3, posCl, alpha, beta, sizeCellMsg);
      tRmn    += area * (rngs[i+3] - posCl) * _tPerCell;
      if(tCut/(tRmn+tCut) <= r*(1.0+e)){
        dt     = (tRmn+tCut)*r*(1.0+e) - tCut;
        posCl = min(rngs[i+3]-1, posCl + (int)ceil(dt/_tPerCell/area));
      }
      // search between posFlr, posCl
      if(posFlr <= posCl){
        for(int pos=posFlr; pos<=posCl; pos++){
          tCut  = blks[bid].time_comm(i,   pos, alpha, beta, sizeCellMsg);
          tCut += area * (pos - rngs[i]) * _tPerCell;
          tRmn  = blks[bid].time_comm(i+3, pos, alpha, beta, sizeCellMsg);
          tRmn += area * (rngs[i+3] - pos) * _tPerCell;
          // compare and ratio
          if(abs(tCut/(tRmn+tCut) - r) < rErr){
            rErr = abs(tCut/(tRmn+tCut) - r);
            cutTmp.pos = pos;
            cmpt_msg_incr(cutTmp);
          }
          // break out if a cut is on b2b margin and within tolerance
          if(blks[bid].is_b2b_margin(i, pos)){
            if(tCut/(tRmn+tCut) <= r*(1.0+e) && tCut/(tRmn+tCut) >= r*(1.0-e)){
              cutTmp.pos = pos;
              cmpt_msg_incr(cutTmp);
              break;
            }
          }
        }
      }
    }
    // starts from x+, y+, z+
    else{
      // set up search range posFlr - posCl
      //.. floor
      posFlr   = rngs[i] - (int)ceil(blks[bid].length(i-3) * r*(1.0+e));
      tCut     = blks[bid].time_comm(i,   posFlr, alpha, beta, sizeCellMsg);
      tCut    += area * (rngs[i] - posFlr) * _tPerCell;
      tRmn     = blks[bid].time_comm(i-3, posFlr, alpha, beta, sizeCellMsg);
      tRmn    += area * (posFlr - rngs[i]) * _tPerCell;
      if(tCut/(tRmn+tCut) <= r*(1.0+e)){
        dt     = (tRmn+tCut)*r*(1.0+e) - tCut;
        posFlr = max(rngs[i-3]+1, posFlr - (int)ceil(dt/_tPerCell/area));
      }
      //.. ceil
      posCl    = rngs[i] - (int)floor(blks[bid].length(i-3) * r*(1.0-e));
      tCut     = blks[bid].time_comm(i,   posCl, alpha, beta, sizeCellMsg);
      tCut    += area * (rngs[i] - posCl) * _tPerCell;
      tRmn     = blks[bid].time_comm(i-3, posCl, alpha, beta, sizeCellMsg);
      tRmn    += area * (posCl - rngs[i-3]) * _tPerCell;
      if(tCut/(tRmn+tCut) >= r*(1.0-e)){
        dt     = tCut - (tRmn+tCut)*r*(1.0-e);
        posCl = min(rngs[i]-1, posCl + (int)ceil(dt/_tPerCell/area));
      }
      // search between posFlr, posCl
      if(posFlr <= posCl){
        for(int pos=posFlr; pos<=posCl; pos++){
          tCut  = blks[bid].time_comm(i,   pos, alpha, beta, sizeCellMsg);
          tCut += area * (rngs[i] - pos) * _tPerCell;
          tRmn  = blks[bid].time_comm(i-3, pos, alpha, beta, sizeCellMsg);
          tRmn += area * (pos - rngs[i-3]) * _tPerCell;
          // compare and time ratio
          if(abs(tCut/(tRmn+tCut) - r) < rErr){
            rErr = abs(tCut/(tRmn+tCut) - r);
            cutTmp.pos = pos;
            cmpt_msg_incr(cutTmp);
          }
          // break out if a cut is on b2b margin and within tolerance
          if(blks[bid].is_b2b_margin(i-3, pos)){
            if(tCut/(tRmn+tCut) <= r*(1.0+e) && tCut/(tRmn+tCut) >= r*(1.0-e)){
              cutTmp.pos = pos;
              cmpt_msg_incr(cutTmp);
              break;
            }
          }
        }
      }
    }

    if(cutTmp < cut)  cut = cutTmp;
  }

  return(0);
}


int BlockGraph::cut_to_elements()
{
  int size0    = 0;        // save graph size
  int rngs[6];                   // block ranges
  BlockCut cut;
  vector< vector<int> > cuts(3); // cut positions

  while(size0 != size()){
    // save graph size
    size0 = size();

    // trvs blocks, cut it if find any map inside block's rngs
    for(int iBlk=0; iBlk<size0; iBlk++){

      // trvs face to find possible on all faces
      blks[iBlk].get_range(rngs);
      for(int iFc=0; iFc<6; iFc++){
        BlockFace *ptFc = blks[iBlk].face_pt(iFc);
        MapIter    iter = ptFc->mapList.begin();
        // trvs map to find possible cut on current face
        while(iter != ptFc->mapList.end()){
          for(int iDir=0; iDir<3; iDir++){
            // add cut to its direction (0, 1, 2) if 1) it is strictly inside
            // block's range and 2) has not been added.
            if(iter->rngs[iDir] > rngs[iDir] && iter->rngs[iDir] < rngs[iDir+3]){
              if(find(cuts[iDir].begin(), cuts[iDir].end(), iter->rngs[iDir])
                  == cuts[iDir].end()){
                cuts[iDir].push_back(iter->rngs[iDir]);
              }
            }
          }
          iter++;
        }// end map
      }// end face

      //cut blocks in x, y, z order in any cut is found
      cut_block(iBlk, cuts);

      // clear cuts
      for(int iDir=0; iDir<3; iDir++)
        cuts[iDir].clear();
    }
  }

  return(0);
}


list<BcMap>::iterator BlockGraph::inv_map_iter(const BcMap& map)
{
  BlockFace* ptFc;
  blks[map.toBlk].get_face_pt(map.toFace, &ptFc);

  list<BcMap>::iterator iter = ptFc->mapList.begin(); 
  while(iter != ptFc->mapList.end()){
    int rngs[6];
    memcpy(rngs, map.toRngs, 6*sizeof(int));
    for(int k=0; k<3; k++){
      if(rngs[k+3] < rngs[k])  swap(rngs[k], rngs[k+3]);
    }
    if(memcmp(rngs, iter->rngs, 6*sizeof(int)) == 0)  break;
    iter++;
  }

  if(iter == ptFc->mapList.end()){
    cerr << "ERROR BlockGraph: inverse map not found" << endl;
    exit(-1);
  }

  return(iter);
}


int BlockGraph::add_inv_map(int blkID, int fcID, const BcMap& map)
{
  BcMap mapInv;
  mapInv.bcType  = "Block2Block";
  mapInv.toBlk   = blkID;
  mapInv.toFace  = fcID;
  mapInv.isFlip  = map.isFlip;
  mapInv.isShMem = map.isShMem;

  BlockFace *ptFc, *ptToFc;
  blks[blkID].get_face_pt(fcID, &ptFc);
  blks[map.toBlk].get_face_pt(map.toFace, &ptToFc);

  memcpy(mapInv.rngs,   map.toRngs, 6*sizeof(int));
  memcpy(mapInv.toRngs, map.rngs,   6*sizeof(int));
  for(int k=0; k<3; k++){
    if(map.toRngs[k+3] < map.toRngs[k]){
      swap(mapInv.rngs[k], mapInv.rngs[k+3]);
      // toAxis is the axis in mapInv's toRngs, which is map's range
      int toAxis = ptFc->map_axis_inv(map, k);
      swap(mapInv.toRngs[toAxis], mapInv.toRngs[toAxis+3]);
    }
  }

  ptToFc->mapList.push_back(mapInv);

  return(0);
}


int BlockGraph::del_inv_map(int blkID, int fcID, const BcMap& map)
{
  // find the inverse map
  list<BcMap>::iterator iter = inv_map_iter(map);
  if(iter->toBlk != blkID || iter->toFace != fcID){
    cerr << "ERROR BlockGraph: Inconsistent bc map in deletion" << endl;
    exit(-1);
  }

  // erase the inverse map
  BlockFace *ptToFc;
  blks[map.toBlk].get_face_pt(map.toFace, &ptToFc);
  ptToFc->mapList.erase(iter);

  return(0);
}


double BlockGraph::msg_load(int id0, int id1)
{
  double msgLoad = 0.0;

  for(int i=0; i<6; i++){
    BlockFace *ptFc =  blks[id0].face_pt(i);
    CMapIter   iter =  ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        if(iter->toBlk == id1){
          msgLoad += max(iter->rngs[3] - iter->rngs[0], 1) \
                   * max(iter->rngs[4] - iter->rngs[1], 1) \
                   * max(iter->rngs[5] - iter->rngs[2], 1) \
                   * nHalo * sizeCellMsg / beta  + alpha;
        }
      }
      iter++;
    }
  }

  return(msgLoad);
}


double BlockGraph::msg_load(const BlockCut& cut, int bid)
{
  double msgLoad = 0.0;
 
  // set cut range
  int rngs[6];
  blks[cut.blkID].get_range(rngs);
  int posS = (cut.isKept) ? cut.pos          : rngs[cut.axis];
  int posE = (cut.isKept) ? rngs[cut.axis+3] : cut.pos;
  
  // parallel faces
  int pFcs[4];
  pFcs[0] = EdgeMap3d[cut.axis][0];  pFcs[2] = pFcs[0] + 3;
  pFcs[1] = EdgeMap3d[cut.axis][1];  pFcs[3] = pFcs[1] + 3;

  // count the message to bid on the parallel faces
  for(int i=0; i<4; i++){
    BlockFace *ptFc = blks[cut.blkID].face_pt(pFcs[i]);
    MapIter    iter = ptFc->mapList.begin();
    int cutMapArea = 0;
    //..
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk == bid){
        if(iter->rngs[cut.axis] < posE && iter->rngs[cut.axis+3] > posS){
          cutMapArea  = min(iter->rngs[cut.axis+3], posE) - max(iter->rngs[cut.axis], posS);
          cutMapArea *= (iter->rngs[pFcs[3-i%2]] - iter->rngs[pFcs[1-i%2]]);
          assert(cutMapArea > 0);
          msgLoad += alpha + cutMapArea * nHalo * sizeCellMsg / beta;
        }
      }
      iter++;
    }      
  }

  // count the massage on the normal uncut face
  int fid = (cut.isKept) ? cut.axis+3 : cut.axis;
  BlockFace *ptFc = blks[cut.blkID].face_pt(fid);
  MapIter    iter = ptFc->mapList.begin();
  while(iter != ptFc->mapList.end()){
    if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk == bid){
      int area = (iter->rngs[pFcs[2]] - iter->rngs[pFcs[0]]) \
               * (iter->rngs[pFcs[3]] - iter->rngs[pFcs[1]]);
      msgLoad += alpha + area * nHalo * sizeCellMsg / beta;
    }
    iter++;
  }

  return(msgLoad);
}


int BlockGraph::set_shmem_map(int nBlk, int *blkIDs)
{
  for(int i=0; i<nBlk; i++){
    for(int j=0; j<6; j++){
      BlockFace *ptFc;
      blks[blkIDs[i]].get_face_pt(j, &ptFc);
      list<BcMap>::iterator iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(find(blkIDs, blkIDs+nBlk, iter->toBlk) != (blkIDs+nBlk))
          iter->isShMem = true;
        iter++;
      }
    }
  }

  for(int i=0; i<nBlk; i++)
    cmpt_msg_load(i);

  return(0);
}


int BlockGraph::cmpt_msg_incr(BlockCut& cut, int toPart)
{
  int pid = blks[cut.blkID].partition_id();

  int rngs[6];
  blks[cut.blkID].get_range(rngs);

  // cut axis index range, and the face connected to toPart
  int posCl, posFlr, fcOff, fcKept;
  if(cut.isKept){
    posCl  = rngs[cut.axis+3];
    posFlr = cut.pos;
    fcOff  = cut.axis+3;
    fcKept = cut.axis;
  }
  else{
    posCl  = cut.pos;
    posFlr = rngs[cut.axis];
    fcOff  = cut.axis;
    fcKept = cut.axis+3;
  }

  cut.msgIncr = 0.0;

  // normal face on cut-off, deduct msg to toPart, add local msg
  cut.msgIncr -= msg_to_part(cut.blkID, fcOff, toPart);
  cut.msgIncr += msg_to_part(cut.blkID, fcOff, pid);

  // if whole block will be moved to toPart, treat the normal face as above
  if(rngs[fcKept] == cut.pos){
    cut.msgIncr -= msg_to_part(cut.blkID, fcKept, toPart);
    cut.msgIncr += msg_to_part(cut.blkID, fcKept, pid);
  }
  // cut block in middle, introduce a new b2b face
  else{
    cut.msgIncr += alpha + blks[cut.blkID].area(fcOff) * nHalo * sizeCellMsg / beta;
  }

  // parallel faces
  int prllFcs[4], area, dir;
  prllFcs[0] = EdgeMap3d[cut.axis][0];  prllFcs[2] = prllFcs[0] + 3;
  prllFcs[1] = EdgeMap3d[cut.axis][1];  prllFcs[3] = prllFcs[1] + 3;
  //..
  for(int j=0; j<4; j++){// prll
    BlockFace *ptPrllFc;
    blks[cut.blkID].get_face_pt(prllFcs[j], &ptPrllFc);
    // axes of parallel face, one is cut axis, find the other
    dir = EdgeMap3d[prllFcs[j]%3][0];
    if(dir == cut.axis)  dir = EdgeMap3d[prllFcs[j]%3][1];
    // traverse maps
    list<BcMap>::iterator iter = ptPrllFc->mapList.begin();
    while(iter != ptPrllFc->mapList.end()){// map
      if(iter->bcType.compare("Block2Block") == 0){// b2b
        int mapStart = iter->rngs[cut.axis];
        int mapEnd   = iter->rngs[cut.axis + 3];
        // if map is in cut-off
        if(mapStart >= posFlr && mapEnd <= posCl){
          area = max(mapEnd - mapStart, 1) \
               * max(iter->rngs[dir+3] - iter->rngs[dir], 1);
          // map to local part
          if(blks[iter->toBlk].partition_id() == pid)
            cut.msgIncr += alpha + area*nHalo*sizeCellMsg/beta;
          // map to target part
          if(blks[iter->toBlk].partition_id() == toPart)
            cut.msgIncr -= alpha + area*nHalo*sizeCellMsg/beta;
        }
        // map not in cut-off, since either posCl or posFlr is the end
        // map range may intersect cut range, but not fully cover it
        else{
          // mapStart < posFlr, mapEnd in cut range .or.
          // mapStart in cut range, mapEnd > posCl
          if((mapEnd   >  posFlr && mapEnd   <= posCl) ||
             (mapStart >= posFlr && mapStart <  posCl)){
            // area in cut-off
            area = max(iter->rngs[dir+3] - iter->rngs[dir], 1) \
                 * max(min(mapEnd,posCl) - max(mapStart,posFlr), 1);
            // cut-off part is mapped to local
            if(blks[iter->toBlk].partition_id() == pid)
              cut.msgIncr += alpha + area*nHalo*sizeCellMsg/beta;
            // cut-off part is mapped to target
            else if(blks[iter->toBlk].partition_id() == toPart)
              cut.msgIncr -= area*nHalo*sizeCellMsg/beta;
            // cut-off part is mapped to else partition
            else
              cut.msgIncr += alpha;
          }
        }
      }// end b2b if

      iter++;
    }// end map loop
  }// end prll loop

  return 0;
}


int BlockGraph::cmpt_msg_incr(BlockCut& cut, const vector<int>& cmpnys)
{
  int pFcs[4]; // parallel faces
  pFcs[0] = EdgeMap3d[cut.axis][0];  pFcs[2] = pFcs[0] + 3;
  pFcs[1] = EdgeMap3d[cut.axis][1];  pFcs[3] = pFcs[1] + 3;

  cut.msgIncr = 0.0;

  // add up latency from parallel faces
  for(int i=0; i<4; i++){
    BlockFace *ptFc = blks[cut.blkID].face_pt(pFcs[i]);
    MapIter    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0 && !iter->isShMem){
        if(iter->rngs[cut.axis] < cut.pos && iter->rngs[cut.axis+3] > cut.pos)
          cut.msgIncr += 2.0 * alpha;
      }
      iter++;
    }
  }

  // add commucation from cut face
  int area     = blks[cut.blkID].area(cut.axis);
  cut.msgIncr += 2.0 * (alpha + area * nHalo * sizeCellMsg / beta);

  // remove communication time to companies
  if(cmpnys.size() > 0){
    for(uint i=0; i<cmpnys.size(); i++)
      cut.msgIncr -= 2*msg_load(cut, cmpnys[i]);
  }

  return(0);
}


double BlockGraph::msg_to_part(int blkID, int fcID, int toPart)
{
  double     msg = 0.0;
  int        plFc0, plFc1, area;
  BlockFace *ptFc;

  blks[blkID].get_face_pt(fcID, &ptFc);
  list<BcMap>::iterator iter = ptFc->mapList.begin();
  while(iter != ptFc->mapList.end()){
    if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != blkID){
      if(blks[iter->toBlk].partition_id() == toPart){
        plFc0 = EdgeMap3d[fcID%3][0];
        plFc1 = EdgeMap3d[fcID%3][1];
        area  = max(iter->rngs[plFc0+3] - iter->rngs[plFc0], 1) \
              * max(iter->rngs[plFc1+3] - iter->rngs[plFc1], 1);
        msg  += (alpha + area * nHalo * sizeCellMsg / beta);
      }
    }
    iter++;
  }

  return(msg);
}


double BlockGraph::msg_to_part(int blkID, int toPart)
{
  double     msg = 0.0;
  int        plFc0, plFc1, area;

  for(int iFc=0; iFc<6; iFc++){
    BlockFace* ptFc = blks[blkID].face_pt(iFc);
    list<BcMap>::iterator iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0 && iter->toBlk != blkID){
        if(blks[iter->toBlk].partition_id() == toPart){
          plFc0 = EdgeMap3d[iFc%3][0];
          plFc1 = EdgeMap3d[iFc%3][1];
          area  = max(iter->rngs[plFc0+3] - iter->rngs[plFc0], 1) \
                * max(iter->rngs[plFc1+3] - iter->rngs[plFc1], 1);
          msg  += (alpha + area * nHalo * sizeCellMsg / beta);
        }
      }
      iter++;
    }
  }

  return(msg);
}


int BlockGraph::merge(int bid, int nCmpny, int *cmpnys)
{
  BlockFace            *ptFc, *ptBcFc;
  list<BcMap>::iterator iter, iterBc;
  vector<int>           mrgIDs;
  int                   mrgFc   = FACE_NULL;
  bool                  isMrgbl = true;
  int                   mrgLen;
  int                   plFcs[4], blkRngs[6], toBlkRngs[6];

  blks[bid].get_range(blkRngs);

  // find a mergable face
  for(int iFc=0; iFc<6; iFc++){
    blks[bid].get_face_pt(iFc, &ptFc);
    // check if this face is mergable with other blocks
    iter    =  ptFc->mapList.begin();
    isMrgbl = !ptFc->mapList.empty();
    mrgIDs.clear();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        // set merge length, if one block have different length,
        // it cannot be merged
        if(iter == ptFc->mapList.begin())
          mrgLen = blks[iter->toBlk].length(iter->toFace%3);
        else if(mrgLen != blks[iter->toBlk].length(iter->toFace%3)){
          isMrgbl = false; break;
        }
        // check if toBlk is null or self
        if(blks[iter->toBlk].id() == BLOCK_NULL){
          isMrgbl = false; break;
        }  
        if(blks[iter->toBlk].id() == bid){
          isMrgbl = false; break;
        }
        // check if toBlk has same parent id, this is a temperary limitation
        if(blks[iter->toBlk].parent() != blks[bid].parent()){
          isMrgbl = false; break;
        }
        // check if toBlk is in compy
        if(nCmpny != 0 && find(cmpnys, cmpnys+nCmpny, iter->toBlk) == cmpnys+nCmpny){
          isMrgbl = false; break;
        }
        // check if toBlk is selected already 
        // each block can only selected once
        if(nCmpny != 0 && find(mrgIDs.begin(), mrgIDs.end(), iter->toBlk) != mrgIDs.end()){
          isMrgbl = false; break;
        }
        // for now exclude inverse, rotate, flip map
        if(iter->toRngs[3] - iter->toRngs[0] < 0 || iter->toRngs[4] - iter->toRngs[1] < 0 ||
           iter->toRngs[5] - iter->toRngs[2] < 0 || iter->toFace != (2*(iFc%3)+3-iFc) ||
           iter->isFlip == true){
          isMrgbl = false; break;
        }
        // mrgbl blk' whole face should be covered by iter map
        blks[iter->toBlk].get_range(toBlkRngs);
        toBlkRngs[iter->toFace%3]   = iter->toRngs[iter->toFace];
        toBlkRngs[iter->toFace%3+3] = iter->toRngs[iter->toFace];
        if(memcmp(toBlkRngs, iter->toRngs, 6*sizeof(int)) != 0){
          isMrgbl = false; break;
        }
        // add toBlk to mergable vector
        mrgIDs.push_back(iter->toBlk);
      }
      // if there is a non-b2b map, face cannot be merged.
      else {
        isMrgbl = false; break;
      }
      iter++;
    }
    // not mergable, move on to next face; mergable, record face
    if(isMrgbl){
      mrgFc = iFc; break;
    }
    else
      continue;
  }

  if(mrgFc != FACE_NULL){// mrgFc
    // change block range
    if(mrgFc > 2)
      blkRngs[mrgFc] += mrgLen;
    else
      blkRngs[mrgFc] -= mrgLen;
    blks[bid].set_range(blkRngs);

    // trvs map of mrgbl blk to setup map on parallel faces of new merged block
    blks[bid].get_face_pt(mrgFc, &ptFc);
    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      // set the map on parallel boundary
      plFcs[0] = EdgeMap3d[mrgFc%3][0];  plFcs[2] = plFcs[0] + 3;
      plFcs[1] = EdgeMap3d[mrgFc%3][1];  plFcs[3] = plFcs[1] + 3;
      for(int j=0; j<4; j++){// prll
        // if the mrgbl block is on boundary of merged blk
        if(iter->rngs[plFcs[j]] == blks[bid].rngs[plFcs[j]]){// on b
          // the face of iter->toBlk on bc is also plFcs[j]
          BlockFace *ptPlFc;
          blks[iter->toBlk].get_face_pt(plFcs[j], &ptBcFc);
          iterBc = ptBcFc->mapList.begin();
          while(iterBc != ptBcFc->mapList.end()){// iterbc
            BcMap mapNew = *iterBc;
            // set the ranges of the new map
            if(mrgFc > 2){
              for(int k=0; k<6; k++)
                mapNew.rngs[k] = iterBc->rngs[k] - blks[iter->toBlk].rngs[k%3] \
                               + iter->rngs[k%3];
            }
            else{
              for(int k=0; k<6; k++)
                mapNew.rngs[k] = iter->rngs[k%3+3] - blks[iter->toBlk].rngs[k%3+3] \
                               + iterBc->rngs[k];
            }
            // if b2b map to blocks going to be merged with bid, reset target range
            if(mapNew.bcType.compare("Block2Block") == 0){// b2b
              if(find(mrgIDs.begin(), mrgIDs.end(), mapNew.toBlk) != mrgIDs.end()){
                mapNew.toBlk = bid;
                // find bid's map to iterBc->toBlk;
                // it is also target of a map on *ptFc
                list<BcMap>::iterator toIter = ptFc->mapList.begin();
                while(toIter != ptFc->mapList.end()){
                  if(toIter->toBlk == iterBc->toBlk) break;
                  toIter++;
                }
                // set toRngs
                if(mrgFc > 2){
                  for(int k=0; k<6; k++)
                    mapNew.toRngs[k] = iterBc->toRngs[k] - blks[toIter->toBlk].rngs[k%3] \
                                     + toIter->rngs[k%3];
                }
                else{
                  for(int k=0; k<6; k++)
                    mapNew.toRngs[k] = toIter->rngs[k%3+3] - blks[toIter->toBlk].rngs[k%3+3] \
                                     + iterBc->toRngs[k];
                }
              }
            }// b2b if
            // add map and its inverse to block
            blks[bid].get_face_pt(plFcs[j], &ptPlFc);
            ptPlFc->mapList.push_back(mapNew);
            if(mapNew.bcType.compare("Block2Block") == 0)  add_inv_map(bid, plFcs[j], mapNew);

            iterBc++;
          }//iterbc loop
        }// on b if
      }//prll loop
      iter++;
    }

    // trvs map of mrgbl blk to setup map on normal face of new merged block
    // ignore the possibility where mrgbl map to mrgbl
    list<BcMap> normMapList;
    blks[bid].get_face_pt(mrgFc, &ptFc);
    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      blks[iter->toBlk].get_face_pt(mrgFc, &ptBcFc);
      iterBc = ptBcFc->mapList.begin();
      while(iterBc != ptBcFc->mapList.end()){
        BcMap mapNew = *iterBc;
        if(mrgFc > 2){
          for(int k=0; k<6; k++)
            mapNew.rngs[k] = iterBc->rngs[k] - blks[iter->toBlk].rngs[k%3] \
                           + iter->rngs[k%3];
        }
        else{
          for(int k=0; k<6; k++)
            mapNew.rngs[k] = iter->rngs[k%3+3] - blks[iter->toBlk].rngs[k%3+3] \
                           + iterBc->rngs[k];
        }
        normMapList.push_back(mapNew);
        if(mapNew.bcType.compare("Block2Block") == 0)  add_inv_map(bid, mrgFc, mapNew);
        iterBc++;
      }
      // remove map of old norm face
      del_inv_map(bid, mrgFc, *iter);
      iter = ptFc->mapList.erase(iter);
    }
    ptFc->mapList = normMapList;

    // remove maps of mrgbl blocks and mark as null
    for(unsigned int i=0; i<mrgIDs.size(); i++){
      for(int iFc=0; iFc<6; iFc++){
        blks[mrgIDs[i]].get_face_pt(iFc, &ptFc);
        iter = ptFc->mapList.begin();
        while(iter != ptFc->mapList.end()){
          if(iter->bcType.compare("Block2Block") == 0)
            del_inv_map(mrgIDs[i], iFc, *iter);
          iter = ptFc->mapList.erase(iter);
        }
      }
      blks[mrgIDs[i]].set_blockID(BLOCK_NULL);
    }

    // merge the map of new merged block also update its inv map
    merge_map(bid);

#if verbose>=2
    cout << "BlockGraph: Merge block " << bid << " with ";
    for(unsigned int i=0; i<mrgIDs.size(); i++)
      cout << mrgIDs[i] << " ";
    cout << endl;
#endif

    return(1);
  }// mrgFc if
  else{
    return(0);
  }
}


int BlockGraph::merge_map(int bid)
{
  BlockFace *ptFc;
  int        isMrgbl = 0;
  list<BcMap>::iterator iter0, iter1;

  for(int iFc=0; iFc<6; iFc++){// face
    blks[bid].get_face_pt(iFc, &ptFc);
    // search for mergable map
    while(1){
      isMrgbl = 0;
      for(iter0 = ptFc->mapList.begin(); iter0 != ptFc->mapList.end(); iter0++){
        for(iter1 = next(iter0,1); iter1 != ptFc->mapList.end(); iter1++){
          isMrgbl = ptFc->is_mergable(*iter0, *iter1);
          if(isMrgbl) break;
        }
        if(isMrgbl) break;
      }
      if(isMrgbl){// mrgbl
        int   dir0   = EdgeMap3d[iFc%3][0];
        int   dir1   = EdgeMap3d[iFc%3][1];
        BcMap mapMrg = *iter0;
        switch(isMrgbl){
        case 1:
          mapMrg.rngs[dir0]   = iter1->rngs[dir0];
          if(iter0->bcType.compare("Block2Block") == 0){
            int toDir = ptFc->map_axis(*iter0, dir0);
            mapMrg.toRngs[toDir] = iter1->toRngs[toDir];
          }
          break;
        case 2:
          mapMrg.rngs[dir0+3] = iter1->rngs[dir0+3];
          if(iter0->bcType.compare("Block2Block") == 0){
            int toDir = ptFc->map_axis(*iter0, dir0);
            mapMrg.toRngs[toDir+3] = iter1->toRngs[toDir+3];
          }
          break;
        case 3:
          mapMrg.rngs[dir1]   = iter1->rngs[dir1];
          if(iter0->bcType.compare("Block2Block") == 0){
            int toDir = ptFc->map_axis(*iter0, dir1);
            mapMrg.toRngs[toDir] = iter1->toRngs[toDir];
          }
          break;
        case 4:
          mapMrg.rngs[dir1+3] = iter1->rngs[dir1+3];
          if(iter0->bcType.compare("Block2Block") == 0){
            int toDir = ptFc->map_axis(*iter0, dir1);
            mapMrg.toRngs[toDir+3] = iter1->toRngs[toDir+3];
          }
          break;
        }
        // remove inverse b2b maps
        if(iter0->bcType.compare("Block2Block") == 0){
        del_inv_map(bid, iFc, *iter0);
        del_inv_map(bid, iFc, *iter1);
        }
        // remove unmerged map
        ptFc->mapList.erase(iter0);
        ptFc->mapList.erase(iter1);
        // add new merged map and inverse if it is b2b
        ptFc->mapList.push_back(mapMrg);
        if(mapMrg.bcType.compare("Block2Block") == 0)  add_inv_map(bid, iFc, mapMrg);
      }// end mrgbl if
      else{
        break;
      }
    }// while loop
  }// face loop

  return(0);
}


int BlockGraph::fwrite_block_info(const string& fname)
{
  ofstream output(fname);
  int      rngs[6];

  output << blks.size() << endl;
  for(uint i=0; i<blks.size(); i++){
    blks[i].get_range(rngs);
    output << rngs[0] << " " << rngs[1] << " " << rngs[2] << " "
           << rngs[3] << " " << rngs[4] << " " << rngs[5] << " "
           << blks[i].partition_id() << endl;
  }
  output.close();

  return(0);
}


int BlockGraph::fwrite_map(const string& fname)
{
  ofstream output(fname);

  // write to header
  output << "Type BC\t" << "Block #\t" << "Face #\t" 
         << "Edg1.Indx1\t" << "Edg1.Indx2\t" << "Edg2.Indx1\t" << "Edg2.Indx2\t"
         << " - Block #\t" << "Face #\t"
         << "Edg1.Indx1\t" << "Edg1.indx2\t" << "Edg2.Indx1\t" << "Edg2.indx2\t"
         << "Flip" << endl;

  // write maps
  for(uint i=0; i<blks.size(); i++){
    int rngs[6];
    blks[i].get_range(rngs);
    for(int j=0; j<6; j++){
      int        dir0 = EdgeMap3d[j%3][0];
      int        dir1 = EdgeMap3d[j%3][1];
      BlockFace *ptFc = blks[i].face_pt(j);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        // write common part
        // write b2b specific part
        if(iter->bcType.compare("Block2Block") == 0){
          if(iter->toBlk > (int)i || (iter->toBlk == (int)i && iter->toFace > j)){
            // note that in the mapfile all indcies start from 1 so the indices
            // are incremented.
            output << iter->bcType << " " << i+1 << " " << j+1 << " "
                   << iter->rngs[dir0]+1 << " " << iter->rngs[dir0+3]+1 << " "
                   << iter->rngs[dir1]+1 << " " << iter->rngs[dir1+3]+1 << " ";
            output << iter->toBlk+1 << " " << iter->toFace+1 << " ";
            int toDir0 = EdgeMap3d[iter->toFace%3][0];
            int toDir1 = EdgeMap3d[iter->toFace%3][1];
            output << iter->toRngs[toDir0]+1 << " " << iter->toRngs[toDir0+3]+1 << " "
                   << iter->toRngs[toDir1]+1 << " " << iter->toRngs[toDir1+3]+1 << " ";
            if(iter->isFlip)
              output << "yes" << endl;
            else
              output << "no"  << endl;
          }
        }
        else{
          output << iter->bcType << " " << i+1 << " " << j+1 << " "
                 << iter->rngs[dir0]+1 << " " << iter->rngs[dir0+3]+1 << " "
                 << iter->rngs[dir1]+1 << " " << iter->rngs[dir1+3]+1 << endl;;
        }
        iter++;
      }
    }
  }

  return(0);
}


int BlockGraph::fwrite_block_profile(const string& fname)
{
  ofstream output(fname);
  output << "#Blocks " << blks.size() << endl;
  output << "total work " << work() << endl;
  output << "#Block id\tWorkLoad\tMax#FaceB2B\t#EdgeCut\tVolComm\t#Copy\tVolShr\tTimeComm\n";

  for(uint i=0; i<blks.size(); i++){
    int    nComm   = 0,   nCpy  = 0;
    double volComm = 0.0, volShr = 0.0;
    int    nB2bFcMax = 0;
    // adds up info on each face
    for(int j=0; j<6; j++){
      BlockFace *ptFc = blks[i].face_pt(j);
      CMapIter   iter = ptFc->mapList.begin();
      int        nB2bFc = 0;
      // traveser all maps 
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0){
          double vol = max(iter->rngs[5] - iter->rngs[2], 1) \
                     * max(iter->rngs[4] - iter->rngs[1], 1) \
                     * max(iter->rngs[3] - iter->rngs[0], 1) \
                     * (double)nHalo * sizeCellMsg;
          if(iter->isShMem){
            nCpy++;
            volShr += vol;
          }
          else{
            nComm++;
            volComm += vol;
          }
          nB2bFc++;
        }
        iter++;
      }
      // count b2b on a face
      if (nB2bFc > nB2bFcMax) nB2bFcMax = nB2bFc;
    }
    // output
    output << i << "\t" << blks[i].work() << "\t" << nB2bFcMax << "\t" 
           << nComm << "\t" << volComm << "\t" << nCpy << "\t" << volShr << "\t" 
           << blks[i].msg_load() << endl;
  }

  output.close();

  return(0);
}


int BlockGraph::debug_check()
{
  for(uint i=0; i<blks.size(); i++){
    // check length
    for(int j=0; j<3; j++){
      if(blks[i].length(j) < nHalo)  
        cout << "ERROR: block " << i << " length " << j << " " 
             << blks[i].length(j) << " < nHalo." << endl;
    }
    // check maps, implement later
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc = blks[i].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin(), iterInv;
      double     fcArea = 0.0;
      int        dir0   = EdgeMap3d[iFc%3][0];
      int        dir1   = EdgeMap3d[iFc%3][1];
      // traverse map in face
      while(iter != ptFc->mapList.end()){
        fcArea += max(iter->rngs[3] - iter->rngs[0], 1)\
                * max(iter->rngs[4] - iter->rngs[1], 1)\
                * max(iter->rngs[5] - iter->rngs[2], 1);
        // check block2block bc
        if(iter->bcType.compare("Block2Block") == 0){
          iterInv = inv_map_iter(*iter);
          // check flip
          if(iter->isFlip != iterInv->isFlip)
            cerr << "ERROR: block flip boundary does not match." << endl;
          // check range
          int toDir0, toDir1, toLen0, toLen1, len0, len1;
          len0   = iter->rngs[dir0+3] - iter->rngs[dir0];
          len1   = iter->rngs[dir1+3] - iter->rngs[dir1];
          toDir0 = (iter->isFlip) ? EdgeMap3d[iter->toFace%3][1] : EdgeMap3d[iter->toFace%3][0];
          toDir1 = (iter->isFlip) ? EdgeMap3d[iter->toFace%3][0] : EdgeMap3d[iter->toFace%3][1];
          toLen0 = iter->toRngs[toDir0+3] - iter->toRngs[toDir0];
          toLen1 = iter->toRngs[toDir1+3] - iter->toRngs[toDir1];
          if(len0 != abs(toLen0) || len1 != abs(toLen1))
            cerr << "ERROR: map's range does not match." << endl;
          // check inverse
          len0 = iterInv->toRngs[dir0+3] - iterInv->toRngs[dir0];
          len1 = iterInv->toRngs[dir1+3] - iterInv->toRngs[dir1];
          if(len0 != toLen0 || len1 != toLen1)
            cerr << "ERROR: map's range does not match inverse map." << endl;
          // check if shared map and partitions match
          bool isSamePart = blks[i].partition_id() == blks[iter->toBlk].partition_id();
          if(iter->isShMem^isSamePart)
            cerr << "ERROR: shared memory map and partitions do not match." << endl;
          // if the maps' range intersect with each other
          MapIter iterPost = next(iter, 1);
          while(iterPost != ptFc->mapList.end()){
            bool isInterSect = false;
            isInterSect = (iterPost->rngs[dir0]   >= iter->rngs[dir0]   &&
                           iterPost->rngs[dir0]   <  iter->rngs[dir0+3] &&
                           iterPost->rngs[dir1]   >= iter->rngs[dir1]   &&
                           iterPost->rngs[dir1]   <  iter->rngs[dir1+3]) || isInterSect;
            isInterSect = (iterPost->rngs[dir0]   >= iter->rngs[dir0]   &&
                           iterPost->rngs[dir0]   <  iter->rngs[dir0+3] &&
                           iterPost->rngs[dir1+3] >  iter->rngs[dir1]   &&
                           iterPost->rngs[dir1+3] <= iter->rngs[dir1+3]) || isInterSect;
            isInterSect = (iterPost->rngs[dir0+3] >  iter->rngs[dir0]   &&
                           iterPost->rngs[dir0+3] <= iter->rngs[dir0+3] &&
                           iterPost->rngs[dir1]   >= iter->rngs[dir1]   &&
                           iterPost->rngs[dir1]   <  iter->rngs[dir1+3]) || isInterSect;
            isInterSect = (iterPost->rngs[dir0+3] >  iter->rngs[dir0]   &&
                           iterPost->rngs[dir0+3] <= iter->rngs[dir0+3] &&
                           iterPost->rngs[dir1+3] >  iter->rngs[dir1]   &&
                           iterPost->rngs[dir1+3] <= iter->rngs[dir1+3]) || isInterSect;
            if(isInterSect)
              cerr << "ERROR: two map intects on block " << i << " face " << iFc << endl;
            iterPost++;
          }
        }
        // increment
        iter++;
      }
      // check face area
      if(fcArea != blks[i].area(iFc))
        cerr << "ERROR: block " << i << " face " << iFc << " area does not match" << endl;
    }
  }
  return(0);
}


int BlockGraph::set_mesh(int nBlock, const int* il, const int* jl, const int* kl)
{
  // resize the blk array
  this->resize(nBlock);

  // read block index ranges
  for(int iBlk=0; iBlk<(int)blks.size(); iBlk++){
    int rngs[6] = {0};
    rngs[3] = il[iBlk] - 1;
    rngs[4] = jl[iBlk] - 1;
    rngs[5] = kl[iBlk] - 1;
    blks[iBlk].set_range(rngs);
  }

  return(0);
}


int BlockGraph::read_bc(int* NbcInt, int length, string* bcString, int* bcInt)
{
  int count, next=0;

  for (int i = 0; i < length; i++) {
    int blkID, fcID, toBlkID, toFcID, toFcDir, toDirs[2], toRngs[6], fcDir, dirs[2], rngs[6];
    string bcStr;
    bool isFlip;

    bcStr=bcString[i];
    count=NbcInt[i];

    blkID=bcInt[i+next];
    fcID=bcInt[i+1+next];
    blkID--;
    fcID--;
  // set the default face ranges, which is whole face
    fcDir   = fcID % 3;
    dirs[0] = EdgeMap3d[fcDir][0];
    dirs[1] = EdgeMap3d[fcDir][1];
    blks[blkID].get_range(rngs);
    rngs[fcDir]   = rngs[fcID];
    rngs[fcDir+3] = rngs[fcID];

    if(count==13){
      rngs[dirs[0]]=bcInt[i+2+next];rngs[dirs[0]]--;
      rngs[dirs[0]+3]=bcInt[i+3+next];rngs[dirs[0]+3]--;
      rngs[dirs[1]]=bcInt[i+4+next];rngs[dirs[1]]--;
      rngs[dirs[1]+3]=bcInt[i+5+next];rngs[dirs[1]+3]--;

      toBlkID=bcInt[i+6+next];
      toFcID=bcInt[i+7+next];
      toBlkID--;
      toFcID--;

      toFcDir   = toFcID % 3;
      toDirs[0] = EdgeMap3d[toFcDir][0];
      toDirs[1] = EdgeMap3d[toFcDir][1];
      //...
      blks[toBlkID].get_range(toRngs);
      toRngs[toFcDir]   = toRngs[toFcID];
      toRngs[toFcDir+3] = toRngs[toFcID];

      toRngs[toDirs[0]]=bcInt[i+8+next];toRngs[toDirs[0]]--;
      toRngs[toDirs[0]+3]=bcInt[i+9+next];toRngs[toDirs[0]+3]--;
      toRngs[toDirs[1]]=bcInt[i+10+next];toRngs[toDirs[1]]--;
      toRngs[toDirs[1]+3]=bcInt[i+11+next];toRngs[toDirs[1]+3]--;

      int flipStr;
      flipStr=bcInt[i+12+next];
      isFlip = (flipStr != 0);
      // add bc
      blks[blkID].add_bc(fcID, bcStr, rngs, toBlkID, toFcID, toRngs, isFlip);
      //cout << blkID << "|" << fcID << "|" << toBlkID << "|" << toFcID << endl;
      // add reverse mapping
      for(int i=0; i<2; i++){
        if(toRngs[toDirs[i]] > toRngs[toDirs[i]+3]){
          swap(toRngs[toDirs[i]], toRngs[toDirs[i]+3]);
          swap(rngs[dirs[i]], rngs[dirs[i]+3]);
        }
      }
      blks[toBlkID].add_bc(toFcID, bcStr, toRngs, blkID, fcID, rngs, isFlip);
      count--;
      next=next+count;
    }else if (count == 6) {

      rngs[dirs[0]]=bcInt[i+2+next];rngs[dirs[0]]--;
      rngs[dirs[0]+3]=bcInt[i+3+next];rngs[dirs[0]+3]--;
      rngs[dirs[1]]=bcInt[i+4+next];rngs[dirs[1]]--;
      rngs[dirs[1]+3]=bcInt[i+5+next];rngs[dirs[1]+3]--;

      blks[blkID].add_bc(fcID, bcStr, rngs);
      count--;
      next=next+count;
    } else {

      blks[blkID].add_bc(fcID, bcStr, rngs);
      count--;
      next=next+count;
    }


  }

  return(0);
}


void BlockGraph::get_ID(int np, int* partiID)
{
  for (int iBlk = 0; iBlk < (int)blks.size(); iBlk++) {
    partiID[iBlk] = blks[iBlk].parent();
  }
}


void BlockGraph::get_bc_name(string* bcstring)
{
  int pos = 0;
  for (int iBlk = 0; iBlk < (int)blks.size(); iBlk++) {
    for (int ifc = 0; ifc < 6; ifc++){
      BlockFace* ptFc = blks[iBlk].face_pt(ifc);
      CMapIter   iter = ptFc->mapList.begin();
      while (iter != ptFc->mapList.end()) {
        bcstring[pos] = iter->bcType;
        ++pos;
        ++iter;
      }
    }
  }
}


void BlockGraph::get_number_faces(int &numberoffaces, int* nsurface)
{
  int nf=0,nf2;
  for (int iBlk = 0; iBlk < (int)blks.size(); iBlk++) {
    nf2=0;
    for (int ifc = 0; ifc < 6; ifc++){
      BlockFace face;
      blks[iBlk].get_face(ifc,face);
      nf=nf + (int)face.mapList.size();
      nf2=nf2 + (int)face.mapList.size();
    }
  nsurface[iBlk]=nf2;
  }
  numberoffaces=nf;
}


void BlockGraph::get_bc(int* bclist)
{
  int pos = 0; // position for next current write
  for (int iBlk = 0; iBlk < (int)blks.size(); iBlk++) {
    for (int ifc = 0; ifc < 6; ifc++){
      BlockFace* ptFc = blks[iBlk].face_pt(ifc);
      CMapIter   iter = ptFc->mapList.begin();
      while (iter != ptFc->mapList.end()) {
        bclist[pos]    = iBlk+1;
        bclist[pos+1]  = ifc;
        memcpy(bclist+pos+2,  iter->rngs,   6*sizeof(int));
        bclist[pos+8]  = iter->toBlk;
        bclist[pos+9]  = iter->toFace;
        memcpy(bclist+pos+10, iter->toRngs, 6*sizeof(int));
        bclist[pos+16] = iter->isFlip;
        pos += 17;
        ++iter;
      }
    }
  }
}


void BlockGraph::get_range(int* range)
{
  for(int iBlk=0; iBlk<(int)blks.size(); iBlk++){
    int rng[6];
    blks[iBlk].get_range(rng);
    for (int i=0; i<6; i++) {
      range[i+iBlk*6]=rng[i];
    }
  }
}


/************************      BlockGraph Class        ************************/
/************************          private             ************************/


int BlockGraph::_find_best_cuts(int bid, int nSubBlk, int& d, int abc[3])
{
  assert(nSubBlk > 1);

  // length of block
  int rngs[6],   lens[3];
  blks[bid].get_range(rngs);
  for(int j=0; j<3; j++)
    lens[j] = rngs[j+3] - rngs[j];

  // variables in loop
  d = 0;
  int    abcD0[3], abcD1[3];
  double msgD0,    msgD1;
  double wkAvg  = (double)blks[bid].work_load() / nSubBlk;
  int    nSubBlk0 = nSubBlk;

  while(true){
    // find best division without cutting 1 chuck off
    _find_blk_division(lens, nSubBlk, abcD0, msgD0);

    // find best division after cutting 1 chunk off
    if(nSubBlk > 2){
      // sort direction by its length
      int dirMax, dirMid, dirMin;
      blks[bid].get_axes_sorted(dirMax, dirMid, dirMin);
      // cut off 1 chuck along longest direction and reduce length
      int    lenCut = (int)round(wkAvg / lens[dirMin] / lens[dirMid]);
      lens[dirMax] -= lenCut;
      nSubBlk      -= 1;
      // find the division and add communication between the cut-off chuck and divisions
      _find_blk_division(lens, nSubBlk, abcD1, msgD1);
      msgD1 += lens[dirMid]*lens[dirMin]/beta + abcD1[dirMin]*abcD1[dirMid]*alpha;
    }
    else{
      msgD1 = DLARGE;
    }

    // compare two divisions
    if(msgD1 <= msgD0){
      d++;
      memcpy(abc, abcD1, 3*sizeof(int));
    }
    else{
      memcpy(abc, abcD0, 3*sizeof(int));
      break;
    }
  }

  if(d + abc[0]*abc[1]*abc[2] != nSubBlk0){
    cerr << "ERROR: PartMethod: division n = d + abc does not hold" << endl;
    cerr << "ERROR: PartMethod: n = " << nSubBlk << " d = " << d << " a,b,c = "
         << abc[0] << " " << abc[1] << " " << abc[2] << endl;
    exit(-1);
  }

  return(0);
}


int BlockGraph::_find_blk_division(int lens[3], int nSubBlk, int nDivs[3], double &msgMin)
{
  double msg;

  // match dirMax, dirMid, dirMin to longest, longer, long direction
  int dirMin = 2, dirMid = 1, dirMax = 0;
  if(lens[dirMax] < lens[dirMid])  swap(dirMax, dirMid);
  if(lens[dirMax] < lens[dirMin])  swap(dirMax, dirMin);
  if(lens[dirMid] < lens[dirMin])  swap(dirMid, dirMin);

  // set area
  int areaMin = lens[dirMid] * lens[dirMin];
  int areaMid = lens[dirMax] * lens[dirMin];
  int areaMax = lens[dirMid] * lens[dirMax];

  // cubic root of nSubBlk
  int cubeRoot = (int)floor(pow(nSubBlk, 1.0/3.0));

  // init msg and nDivs
  nDivs[dirMax] = nSubBlk;
  nDivs[dirMid] = 1;
  nDivs[dirMin] = 1;
  msgMin        = (nSubBlk-1) * (areaMin/beta + alpha);

  // search among factors to find division
  // factor iMax, iMid, iMin matches dirMax, dirMid, dirMin
  // iMax in [nSubBlk^1/3, nSubBlk]
  for(int iMax = nSubBlk; iMax >= cubeRoot; iMax--){
    if(nSubBlk%iMax != 0) continue;
    int qtnt = nSubBlk / iMax;
    // iMid in [1, iMax]
    for(int iMid = 1; iMid <= iMax; iMid++){
      if(qtnt%iMid != 0) continue;
      // iMin in [1, iMid]
      for(int iMin = 1; iMin <= iMid; iMin++){
        if(qtnt != iMid*iMin) continue;
        // overall communication after cutting this block
        msg = (iMax-1) * (areaMin/beta + iMid*iMin*alpha) \
            + (iMid-1) * (areaMid/beta + iMax*iMin*alpha) \
            + (iMin-1) * (areaMax/beta + iMid*iMax*alpha);
        if(msg < msgMin){
          nDivs[dirMax] = iMax;
          nDivs[dirMid] = iMid;
          nDivs[dirMin] = iMin;
          msgMin        =  msg;
        }
      }
    }
  }
  
  return(0);
}


int BlockGraph::_bisect_to_1_block(int bid, int npREB, double toler, vector<int>& npNewBlks)
{
  // workload and #proc
  double wkBlk  = blks[bid].work();

  // bisect the block recursively
  while(npREB > 1){
    int    halfFlr = npREB/2, halfCl = npREB - npREB/2;
    BlockCut cut;
    find_min_blkcut(bid, halfFlr*wkBlk/npREB, toler, cut);
    cut_block(cut);
    // setup next iteration
    if(cut.isKept){
      npNewBlks.push_back(halfFlr);
      npREB = halfCl;
    }
    else{
      npNewBlks.push_back(halfCl);
      npREB = halfFlr;
    }
    wkBlk = blks[bid].work();
  }

#if verbose>=2
  cout << "BlockGraph: bisect " << bid << " to single block." << endl;
#endif

  return(0);
}
