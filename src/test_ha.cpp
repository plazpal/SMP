#include <stdlib.h>
#include <iostream>
#include "GridInput.h"
#include "BlockGraph.h"
#include "BlockPart.h"
#include "PartMethod.h"
#include "Partitioner.h"
#include "HaloArray.h"

using namespace std;

int main(int argc, char* argv[])
{

  int         nProc, rank, thrdMode;
  BlockGraph  geo, grid;
  vector<int> bids;
  int         nh = 2;
  int         methodType = 0, adjustType = INDEX_NULL;

  MPI_Init_thread(&argc, &argv, MPI_THREAD_SERIALIZED, &thrdMode);
  
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &nProc);

  if(thrdMode != MPI_THREAD_SERIALIZED){
    if(rank == 0) cerr << "Does not support MPI_THREAD_SERIALIZED" << endl;
    MPI_Finalize();
  }
  
  if(rank == 0){
    if(argc > 5){
      cerr << "Wrong number of command arguments" << endl;
      cerr << "The correct usage is ./[executable] (-method [type] -adjust [type]" << endl;
    }
    else {
      for (int i=0; i<argc; i++) {
        if (strcmp(argv[i], "-method") == 0)   methodType = atoi(argv[i+1]);
        if (strcmp(argv[i], "-adjust") == 0)   adjustType = atoi(argv[i+1]);
      }
    }
  }
  // bcast method type
  MPI_Bcast(&methodType, 1, MPI_INT, 0, MPI_COMM_WORLD);
  // bcast adjust type
  MPI_Bcast(&adjustType, 1, MPI_INT, 0, MPI_COMM_WORLD);

  geo.setup_msg_load(2.3081e-5, 1.7728e9, 8);
  geo.set_cell_time(6.0e-8);
  geo.fread_mesh_plot3d("mesh.x", false);
  geo.fread_map("mapfile");

  // create and setup partition
  Partitioner pttnr(nProc);
  pttnr.set_tolerance(0.05);
  pttnr.setup_partition_method((Method_t)methodType);

  // decompose the geometry blocks
  pttnr.decompose(geo, grid);

  // adjust the grid if set to
  if (adjustType != INDEX_NULL) {
    if (adjustType == 0) pttnr.adjust_all(grid);
    if (adjustType == 1) pttnr.adjust_greedy(grid);
  }

  // evaluate partition properties
  if(rank == 0){
    grid.debug_check();
    pttnr.eval_part_prop(grid);
    pttnr.view_part_prop();
    cout << " #blocks " << grid.size() << endl;
  }

  // create halo array
  pttnr.get_block_ids(rank, bids);
  HaloArray ha(&grid, bids, nh);

  // init ha
  for(uint iBlk=0; iBlk<bids.size(); iBlk++){
    int rngs[6];
    grid[bids[iBlk]].get_range(rngs);
    for(int i=nh; i<rngs[3]-rngs[0]+nh; i++){
      for(int j=nh; j<rngs[4]-rngs[1]+nh; j++){
        for(int k=nh; k<rngs[5]-rngs[2]+nh; k++){
          ha.at((int)iBlk,i,j,k) = (i - nh + rngs[0]) * bids[iBlk] \
                                 +  j - nh + rngs[1] \
                                 + (double)(k - nh + rngs[2]) / (bids[iBlk]+1);
        }
      }
    }
  }

  // exchange halo
#pragma omp parallel
{
  #pragma omp master
  ha.setup_timer(omp_get_num_threads());
  #pragma omp barrier  
  ha.pack_halo_to_buf();
  #pragma omp barrier  
  #pragma omp master
  {
    ha.update_halo_begin();
    ha.update_halo_end();
  }
  ha.copy_halo_shared_mem();
  #pragma omp barrier  
  ha.unpack_buf_to_halo();
  #pragma omp barrier  
}//end parallel

  // check halo value
  double errTot = 0.0;
  for(uint iBlk=0; iBlk<bids.size(); iBlk++){
    int rngs[6];
    grid[bids[iBlk]].get_range(rngs);
    for(int iFc=0; iFc<6; iFc++){
      BlockFace *ptFc = grid[bids[iBlk]].face_pt(iFc);
      MapIter    iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        double errMap = 0.0;
        if(iter->bcType.compare("Block2Block") == 0){
          // direction map
          int dir0, dir1, s2t[3];
          s2t[iFc%3] = iter->toFace % 3;
          dir0       = EdgeMap3d[iFc%3][0];
          dir1       = EdgeMap3d[iFc%3][1];
          s2t[dir0]  = iter->isFlip ? EdgeMap3d[iter->toFace%3][1] : EdgeMap3d[iter->toFace%3][0];
          s2t[dir1]  = iter->isFlip ? EdgeMap3d[iter->toFace%3][0] : EdgeMap3d[iter->toFace%3][1];
          // inverse
          bool isInvs[3];
          isInvs[dir0]  = iter->toRngs[s2t[dir0]+3] < iter->toRngs[s2t[dir0]];
          isInvs[dir1]  = iter->toRngs[s2t[dir1]+3] < iter->toRngs[s2t[dir1]];
          if(iter->isFlip)
            isInvs[iFc%3] = (isInvs[dir0] && isInvs[dir1]) || (!isInvs[dir0] && !isInvs[dir1]);
          else
            isInvs[iFc%3] = (isInvs[dir0] && !isInvs[dir1]) || (!isInvs[dir0] && isInvs[dir1]);
          // halo region range
          int hlRngs[6];
          memcpy(hlRngs, iter->rngs,   6*sizeof(int));
          hlRngs[iFc%3]   = iFc < 3 ? hlRngs[iFc%3] - nh : hlRngs[iFc%3];
          hlRngs[iFc%3+3] = iFc < 3 ? hlRngs[iFc%3+3]    : hlRngs[iFc%3+3] + nh;
          // corresponding body region range
          int bdRngs[6], fid = iter->toFace;
          memcpy(bdRngs, iter->toRngs, 6*sizeof(int));
          for(int i=0; i<3; i++)
            if(bdRngs[i+3] < bdRngs[i])  swap(bdRngs[i], bdRngs[i+3]);
          bdRngs[fid%3]   = fid < 3 ? bdRngs[fid%3]        : bdRngs[fid%3] - nh;
          bdRngs[fid%3+3] = fid < 3 ? bdRngs[fid%3+3] + nh : bdRngs[fid%3+3];
          // check result pointwise
          for(int i=hlRngs[0]; i<hlRngs[3]; i++){
            for(int j=hlRngs[1]; j<hlRngs[4]; j++){
              for(int k=hlRngs[2]; k<hlRngs[5]; k++){
                double val    = 0.0;
                int    idx[3] = {i, j, k}, toIdx[3];
                for(int l=0; l<3; l++){
                  toIdx[l] = (isInvs[l]) ? bdRngs[s2t[l]+3]-1 - idx[l] + hlRngs[l] \
                           : bdRngs[s2t[l]] + idx[l] - hlRngs[l];

                  if(s2t[l] == 0)  val += (double)toIdx[l] * iter->toBlk;
                  if(s2t[l] == 1)  val += (double)toIdx[l];
                  if(s2t[l] == 2)  val += (double)toIdx[l] / (iter->toBlk+1);
                }
                int iLcl = i - rngs[0] + nh;
                int jLcl = j - rngs[1] + nh;
                int kLcl = k - rngs[2] + nh;
                double err = abs(val - ha.at((int)iBlk, iLcl, jLcl, kLcl));
                errMap = max(err, errMap);
                //if(errMap > DSMALL && rank == 0){
                //cout << bids[iBlk] << " " << iter->toBlk << endl;
                //}
                //if(err > DSMALL && rank == 0)
                  //cout << iLcl << " " << jLcl << " " << kLcl << " " << val << " " << ha.at(iBlk, iLcl, jLcl, kLcl) << endl;
              }
            } 
          }
        }
        errTot = max(errMap, errTot);
        iter++;
      }
    }
  }

  MPI_Allreduce(MPI_IN_PLACE, &errTot, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

  if(rank == 0)  cout << "Error: " << errTot << endl;

  MPI_Finalize();

  return(0);
}
