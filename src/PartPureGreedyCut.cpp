#include "PartPureGreedyCut.h"

using namespace std;

inline bool operator<(const NbrBlock& lhs, const NbrBlock& rhs){ 
  return (lhs.tCommSave < rhs.tCommSave);
}

inline bool operator>(const NbrBlock& lhs, const NbrBlock& rhs){ 
  return (lhs.tCommSave > rhs.tCommSave);
}

/************************       PartPureGreedyCut Class        ************************/

int PartPureGreedyCut::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
{
  grid   = geo;
  _nPart = nPart;
  
  // save a copy of grid
  vector<int> bids0 = bids;

  // average workload
  double wkTot = 0.0;
  for(unsigned int i=0; i<bids.size(); i++)
    wkTot += (double)grid[bids[i]].work_load();
  _wkAvg = wkTot / _nPart;

  // mark large blocks and assign paritions
  setup_large_blocks(grid, bids);

  while (true) { // top while
      // init partitions
      _partBids.clear();
    for(int i=0; i<_nPart; ++i)
      _partBids.push_back(VECTOR_INT_NULL);

    // mark blocks as undone, note that _isDones[i] matches bids[i]
    _isDones.clear();
    for(unsigned int i=0; i<bids.size(); i++)
      _isDones.push_back(false);

#if verbose>=1
    cout << "PureGreedyCut: ------------ Cut Large Blocks ------------" << endl;
#endif
    cut_large_blocks(grid, bids);

#if verbose>=1
    cout << "PureGreedyCut: ----------- Grow Small Blocks ------------" << endl;
#endif
    grow_small_blocks(grid, bids);

#if verbose>=1
    cout << "PureGreedyCut: ----------- Sweep Assign Blocks ----------" << endl;
#endif
    sweep_assign_blocks(grid, bids);

#if verbose>=1
    cout << "PureGreedyCut: ----------- Cut assign Blocks ------------" << endl;
#endif
    cut_assign_blocks(grid, bids);

    // check if there is empty partition at the end
    int npEmpty = 0;
    for (int i=_nPart-1; i>=0; --i) {
      if (_partBids[i].empty())  ++npEmpty;
    }
    int npAssigned = 0;
    for (uint i=0; i<_npLargeBlks.size(); ++i)  npAssigned += _npLargeBlks[i];

    // if some parts are empty, increment the parts assigned to large blocks
    int npEmpty0 = npEmpty;
    if (npEmpty > 0) {
      vector<int> usedBids;
      while (npEmpty > 0) {
        // find out the most overload
        double  overloadMax = -DLARGE, overload;
        int     overloadBid = INDEX_NULL;
        for (uint i=0; i<_largeBids.size(); ++i) {
          overload = geo[_largeBids[i]].work() - _npLargeBlks[_largeBids[i]]*_wkAvg;
          overload = overload / geo[_largeBids[i]].work();
          if (overload > overloadMax) {
            overloadMax = overload;
            overloadBid = _largeBids[i];
          }
        }
        _npLargeBlks[overloadBid]++; 
        npEmpty--;
      }
      // partition the grid again 
#if verbose>=1
      cout << "PureGreedyCut: Re-assign " << npEmpty0 << " empty partitions." << endl;
      cout << "PureGreedyCut: Repeat the partition process." << endl;
#endif
      // reset grid and results
      grid = geo;
      bids = bids0;
    }
    else{
      break;
    }
  } // end top while

  // update msg load
  for(uint i=0; i<bids.size(); i++)
    grid.cmpt_time(bids[i]);

  return 0;
}


int PartPureGreedyCut::setup_large_blocks(BlockGraph& bg, vector<int>& bids)
{
  int    npLargeBlk = 0;
  for (uint i=0; i<bids.size(); ++i) {
    if (bg[bids[i]].work() > _wkAvg * (1.0-_toler)) {
      // save block id
      _largeBids.push_back(bids[i]);
      // reserve partitions for large block
      int npBlk = max(1, (int)floor(bg[bids[i]].work()/_wkAvg));
      _npLargeBlks.push_back(npBlk);
      npLargeBlk += npBlk;
    }
  }

  //int npAvail = _nPart - npLargeBlk;
  //while(npAvail > 0){
    //// find out the most overload
    //double  overloadMax = -DLARGE, overload;
    //int     overloadBid = INDEX_NULL;
    //for (uint i=0; i<_largeBids.size(); ++i) {
      //overload = bg[_largeBids[i]].work() - _npLargeBlks[_largeBids[i]]*_wkAvg;
      ////overload = overload / geo[_largeBids[i]].work();
      //if (overload > overloadMax) {
        //overloadMax = overload;
        //overloadBid = _largeBids[i];
      //}
    //}
    //if (overloadMax <= 0.0) break;
    //_npLargeBlks[overloadBid]++; 
    //npAvail--;
  //}
  
  return 0;
}


int PartPureGreedyCut::cut_large_blocks(BlockGraph& bg, vector<int>& bids)
{
  int  nLargeBlk = 0; // count large blocks assigned partitions
  int  iPart = 0; // start from partition 0
  uint nBlks0 = bids.size();
  for (uint i=0; i<nBlks0; ++i) {
    // skip the block is assigned
    if (_isDones[i]) continue;
    // if the block is larger than average
    if (bg[bids[i]].work() > _wkAvg * (1.0-_toler)) {
      int npMain  = _npLargeBlks[vector_index(_largeBids, bids[i])];  // # parts of main part
      int bidMain = bids[i]; // main block id
      int iMain   = (int)i;  // main block index in bids
      int bidRes  = INDEX_NULL; // residual block id
      // if residual part exists
      if (bg[bids[i]].work() > npMain*_wkAvg*(1.0+_toler)) {
        // find the cut of residual part
        double wkCut    = bg[bids[i]].work() - npMain * _wkAvg;
        double tolerCut = min(npMain*_wkAvg*_toler / wkCut, (_wkAvg-wkCut)/wkCut*0.95);
        BlockCut cut;
        bg.find_min_blkcut(bids[i], wkCut, tolerCut, cut);
        // if cut is valid, cut off the resiual part
        if (cut.blkID != BLOCK_NULL) {
          bg.cut_block(cut);
          bids.push_back(bg.size()-1);
          _isDones.push_back(false);
          bidMain = (cut.isKept) ? bids[i] : bg.size()-1;
          iMain   = (cut.isKept) ? (int)i  : (int)bids.size()-1;
          bidRes  = (cut.isKept) ? bg.size()-1 : bids[i];
#if verbose>=1
          cout << "PureGreedyCut: cut large block " << bids[i] << " into main block " 
               << bidMain << " and residual block " << bidRes << endl;
#endif
        }
      }

      // recursively bisect the main part
      int size0 = bg.size();
      //vector<int> preBids;
      //if (bidRes != INDEX_NULL) preBids.push_back(bidRes);
      //bg.factorize_block(bidMain, preBids, npMain, _toler);
      bg.reb(bidMain, npMain, _toler);

      // set main block's partition
      bg[bidMain].set_partition(iPart);
      _partBids[iPart].push_back(bidMain);
      _isDones[iMain] = true;
      ++iPart;
      // set blocks for newly appended blocks
      for (int j=size0; j<bg.size(); ++j) {
        bg[j].set_partition(iPart);
        bids.push_back(j);
        _partBids[iPart].push_back(j);
        _isDones.push_back(true);
        ++iPart;
      }
#if verbose>=1
      cout << "PureGreedyCut: cut large block " << bidMain << " into " << npMain << " partitiones." << endl;
#endif
      // count large block
      ++nLargeBlk;
    } // end if block larger than average
  } // end for block

#if verbose>=1
  cout << "PureGreedyCut: " << nLargeBlk << " Large blocks use " << iPart << " partitiones." << endl;
#endif

  return(0);
}


int PartPureGreedyCut::grow_small_blocks(BlockGraph& bg, vector<int>& bids)
{
  // find number of unassigned blocks
  int nSmallBlk = 0;
  for (uint i=0; i<bids.size(); ++i)
    if (!_isDones[i])  ++nSmallBlk; 
  
  // find the first empty partition
  int iPart = 0;
  for (iPart=0; iPart<_nPart; ++iPart)
    if (_partBids[iPart].empty()) break;

#if verbose>=1
  cout << "PureGreedyCut: assign " << nSmallBlk << " small blocks to " 
       << _nPart-iPart << " parts." << endl;
#endif

  while (iPart < _nPart) {
    // find the max unassigned block
    double wkMax = 0.0;
    int    bidMax = INDEX_NULL, iMax = INDEX_NULL;
    for (uint i=0; i<bids.size(); ++i) {
      if (bg[bids[i]].work() > wkMax && !_isDones[i]) {
        wkMax  = bg[bids[i]].work();
        bidMax = bids[i];
        iMax   = i;
      }
    }
    // if all blocks are done, break out, left partition empty
    if (bidMax == -1)  break;
    // assign the max block to an empty partition
    bg[bidMax].set_partition(iPart);
    _partBids[iPart].push_back(bidMax);
    _isDones[iMax] = true;

    ++iPart;
  }

  return 0;
}
 

int PartPureGreedyCut::sweep_assign_blocks(BlockGraph& bg, vector<int>& bids)
{
  while (true) {
    // mark if any block is moved
    bool isBlkMoved = false;

    // travese partition to move blocks
    for (int iPart=0; iPart<_nPart; ++iPart) {

      // if current partition is empty, assign the max unassigned block here
      if (_partBids.empty()) {
        double wkMax = 0.0;
        int    bidMax = INDEX_NULL, iMax = INDEX_NULL;
        for (uint i=0; i<bids.size(); ++i) {
          if (bg[bids[i]].work() > wkMax && !_isDones[i]) {
            wkMax  = bg[bids[i]].work();
            bidMax = bids[i];
            iMax   = i;
          }
        }
        bg[bidMax].set_partition(iPart);
        _partBids[iPart].push_back(bidMax);
        _isDones[iMax] = true;
      }

      // current work of the partition
      double wkPart = 0.0;
      for (uint i=0; i<_partBids[iPart].size(); ++i)
        wkPart += bg[_partBids[iPart][i]].work();

      // find the neighbors that fit in the room
      vector<NbrBlock> nbrs;
      for (uint i=0; i<_partBids[iPart].size(); ++i) {
        int bid = _partBids[iPart][i];
        for (int j=0; j<6; ++j) {
          BlockFace *ptFc = bg[bid].face_pt(j);
          CMapIter   iter = ptFc->mapList.begin();
          while (iter != ptFc->mapList.end()) {
            if (iter->bcType.compare("Block2Block") == 0 && 
                wkPart + bg[iter->toBlk].work() < _wkAvg*(1.0+_toler)) {
              // if the block is not in current partition
              int toPart = bg[iter->toBlk].partition_id();
              if (toPart != iPart) {
                // try to find target block in saved neighbors
                uint pos     = 0;
                bool isFound = false;
                for (pos=0; pos<nbrs.size(); ++pos) {
                  if (nbrs[pos].bid == iter->toBlk) {
                    isFound = true;
                    break;
                  }
                }
                // if target block found
                if (isFound) {
                  nbrs[pos].tCommSave += bg.msg_load(iter->toBlk, bid);
                }
                // else target block first added to neighbors
                else {
                  NbrBlock nbr = {iter->toBlk, bg.msg_load(bid, iter->toBlk)};
                  // if target block was assigned, exclude the shared mem copy
                  if (toPart != INDEX_NULL) {
                    for (uint k=0; k<_partBids[toPart].size(); ++k) {
                      if (iter->toBlk != _partBids[toPart][k])
                        nbr.tCommSave -= bg.msg_load(_partBids[toPart][k], iter->toBlk);
                    }
                  }
                  // add block to neighbors
                  nbrs.push_back(nbr);
                }
              }
            }
            ++iter;
          }// end while map
        }// end for face
      }// end blocks in partition

      // sort the neighbors by its communication saved
      if (nbrs.size() == 0) continue;
      sort(nbrs.begin(), nbrs.end());

      // add neighbors to partition until exceeding the average
      for (int k=(int)nbrs.size()-1; k>=0; --k) {
        // break if the neighbor does not save communication
        if (nbrs[k].tCommSave <= 0.0) break;
        // skip if the neighbor exceeds room left in current partition
        if (wkPart + bg[nbrs[k].bid].work() > _wkAvg*(1.0+_toler)) continue;
        wkPart += bg[nbrs[k].bid].work();
        // remove neighbor from its' partition
        int toPart = bg[nbrs[k].bid].partition_id();
        if (toPart != INDEX_NULL) {
          uint pos = 0;
          for (pos=0; pos<_partBids[toPart].size(); ++pos)
            if (_partBids[toPart][pos] == nbrs[k].bid) break;
          swap(_partBids[toPart][pos], _partBids[toPart].back());
          _partBids[toPart].pop_back();
        }
        // add block to current partition
        _partBids[iPart].push_back(nbrs[k].bid);
        _isDones[vector_index(bids, nbrs[k].bid)] = true;
        bg[nbrs[k].bid].set_partition(iPart);
        // mark a block as moved
        isBlkMoved = true;
#if verbose>=1
        cout << "PureGreedyCut: Assign block " << nbrs[k].bid << " " 
             << nbrs[k].tCommSave<< " to " << iPart << endl;
#endif
      }
    }// end for partition
    // is no block can be moved, break out
    if (!isBlkMoved) break;
  }

  return 0;
}


int PartPureGreedyCut::cut_assign_blocks(BlockGraph& bg, vector<int>& bids)
{
  // find number of unassigned blocks
  int nSmallBlk = 0;
  for (uint i=0; i<bids.size(); ++i)
    if (!_isDones[i])  ++nSmallBlk; 

  while (true) {
    // find largest unassigned block
    double wkMax  = 0.0;
    int    bidMax = INDEX_NULL, iMax = INDEX_NULL;
    for (uint i=0; i<bids.size(); ++i) {
      if (!_isDones[i] && bg[bids[i]].work() > wkMax) {
        bidMax = bids[i];
        iMax   = i;
        wkMax  = bg[bids[i]].work();
      }
    }
    // break if all blocks are done
    if (bidMax == INDEX_NULL) break;

    // find the most underload partition
    int    pidMin    = 0;
    double wkPartMin = DLARGE;
    for (int i=0; i<_nPart; ++i) {
      double wkPart = 0.0;
      for (uint j=0; j<_partBids[i].size(); ++j)
        wkPart += bg[_partBids[i][j]].work();
      if (wkPart < wkPartMin) {
        wkPartMin = wkPart;
        pidMin    = i;
      }
    }

    // assign max block or its cutoff to most underload partition
    // if block fit in the room
    if (wkMax <= _wkAvg*(1.0+_toler) - wkPartMin) {
      bg[bidMax].set_partition(pidMin);
      _isDones[iMax] = true;
      _partBids[pidMin].push_back(bidMax);
#if verbose>=1
      cout << "PureGreedyCut: Assign Block " << bidMax << " to " << pidMin << endl;
#endif
    }
    // if block exceeds the room, cut 0ff a chunk and assign it
    else {
      double   wkCut    = _wkAvg - wkPartMin;
      double   tolerCut = _wkAvg * _toler / wkCut;
      BlockCut cut(DLARGE);
      bg.find_min_blkcut(bidMax, wkCut, tolerCut, cut, _partBids[pidMin]);
      // if a cut is found
      if (cut.blkID != INDEX_NULL) {
        bg.cut_block(cut);
        _isDones.push_back(false);
        bids.push_back(bg.size()-1);
        // if cut-off is appended
        if (cut.isKept) {
          bg[bg.size()-1].set_partition(pidMin);
          _partBids[pidMin].push_back(bg.size()-1);
          _isDones.back() = true;
#if verbose>=1
          cout << "PureGreedyCut: Cut block " << bidMax << " to " << bidMax << " " 
               << bg.size()-1 << " and assign " << bg.size()-1 << " to " << pidMin << endl;
#endif
        }
        // cut-off is in place
        else {
          bg[bidMax].set_partition(pidMin);
          _partBids[pidMin].push_back(bidMax);
          _isDones[iMax] = true;
#if verbose>=1
          cout << "PureGreedyCut: Cut block " << bidMax << " to " << bidMax << " " 
               << bg.size()-1 << " and assign " << bidMax << " to " << pidMin << endl;;
#endif
        }
      }
      // else cut is not found, impose largest block to most underload partition
      else {
        bg[bidMax].set_partition(pidMin);
        _partBids[pidMin].push_back(bidMax);
        _isDones[iMax] = true;
#if verbose>=1
        cout << "PureGreedyCut: Impose block " << bidMax << " to " << pidMin << endl;
#endif
      }
    }
  }
  
#if verbose>=1
  cout << "PureGreedyCut: cut assign " << nSmallBlk << " small blocks." << endl; 
#endif

  return 0;
}
