#include "BlockHeap.h"


int BlockHeap::build(const BlockGraph& bg)
{
  // init heap, pointers
  for(unsigned int i = 0; i < bg.blks.size(); i++){
    _nodes.emplace_back(bg.blks[i]);
    _hpIDs.push_back(i);
  }

  // build heap
  if(_nodes.size() > 1){
    for(int i=(int)(_nodes.size()-2)/2; i>=0; i--)
      hpfy_min(i);
  }

  return(0);
}


int BlockHeap::build(const BlockGraph& bg, const vector<int>& bids)
{
  // init heap, pointers
  for(int i=0; i<bg.size(); i++)
    _hpIDs.push_back(INDEX_NULL);
  for(unsigned int i = 0; i < bids.size(); i++){
    _nodes.emplace_back(bg.blks[bids[i]]);
    _hpIDs[bids[i]] = i;
  }

  // build heap
  if(_nodes.size() > 1){
    for(int i=(int)(_nodes.size()-2)/2; i>=0; i--)
      hpfy_min(i);
  }

  return(0);
}


int BlockHeap::hpfy_min(int i)
/*----------------------------------------------------------
Mthd: compare with its children and swap the value if the
      a child has minimum value then heapify recursively 
      at that child.
----------------------------------------------------------*/
{
  unsigned int l = 2*i+1, r = 2*(i+1);
  int          iMin = i;
  if(l < _nodes.size()){
    if(_nodes[l] < _nodes[iMin])  iMin = l;
  }
  if(r < _nodes.size()){
    if(_nodes[r] < _nodes[iMin])  iMin = r;
  }
  if(i != iMin){
    swap_node(i, iMin);
    hpfy_min(iMin);
  }

  return(0);
}


int BlockHeap::swap_node(int lhsID, int rhsID)
/*----------------------------------------------------------
Note: Need to update their pos in heap (_hpIDs).
----------------------------------------------------------*/
{
  assert(lhsID >= 0 && (uint)lhsID < _nodes.size());
  assert(rhsID >= 0 && (uint)rhsID < _nodes.size());

  if(lhsID != rhsID){
    // swap heap index
    _hpIDs[_nodes[lhsID].id()] = rhsID;
    _hpIDs[_nodes[rhsID].id()] = lhsID;
    // swap heap node
    swap(_nodes[lhsID], _nodes[rhsID]);

#if verbose>=2
    cout << "BlockHeap: switch heap node " << lhsID << " " << rhsID << endl;
#endif
  }



  return(0);
}


int BlockHeap::chng_load(int blkID, double val)
/*----------------------------------------------------------
Mthd: if value is larger than load, heapify the location;
      else compare with its parent and swap if it becomes
      smaller.
----------------------------------------------------------*/
{
#if verbose>=2
  cout << "BlockHeap: Change block " << blkID << "'s load from " 
       << _nodes[_hpIDs[blkID]].msg_load() << " to " << val << endl;
#endif

  // decrease load, node moves up
  if(_nodes[_hpIDs[blkID]].msg_load() > val){
    _nodes[_hpIDs[blkID]].set_load(val);
    int i = _hpIDs[blkID];
    while(i > 0 && _nodes[i] < _nodes[(i-1)/2]){
      swap_node(i, (i-1)/2);
      i = (i-1)/2;
    }
  }
  // increase load, node moves down
  else{
    _nodes[_hpIDs[blkID]].set_load(val);
    hpfy_min(_hpIDs[blkID]);
  }

#if verbose>=2
  cout << "BlockHeap: Heap blocks: ";
  for(unsigned int i=0; i<_nodes.size(); i++)
    cout << "(" << _nodes[i].id() << "," << _nodes[i].msg_load() << ") ";
  cout << endl;
#endif

  return(0);
}


int BlockHeap::append_blk(const Block& blk)
/*----------------------------------------------------------
Mthd: Assign the block a huge init load so it stays at the 
      end of heap. Update the load of block and let it be 
      shifted to the corrent pos.
----------------------------------------------------------*/
{
#if verbose>=2
  cout << "BlockHeap: Append block " << blk.id() << " to heap." << endl;
#endif

  // append a node with large init load to heap.
  BlockNode node(blk);
  node.set_load(DLARGE);
  _nodes.push_back(node);
  _hpIDs.push_back(_nodes.size()-1);

  // assign corrent load and maintain heap property
  chng_load(blk.id(), blk.msg_load());

#if verbose>=2
  cout << "BlockHeap: Heap blocks: ";
  for(unsigned int i=0; i<_nodes.size(); i++)
    cout << "(" << _nodes[i].id() << "," << _nodes[i].msg_load() << ") ";
  cout << endl;
#endif

  return(0);
}


int BlockHeap::append_blk(const int blkNull)
{
  if(blkNull != BLOCK_NULL){
    cerr << "ERROR: blockHeap.append_blk only takes BLOCK_NULL as int input" << endl;
    exit(-1);
  }

  _hpIDs.push_back(BLOCK_NULL);

  return(0);
}


int BlockHeap::pop_min()
/*----------------------------------------------------------
Mthd: Swap the top with the last node and remove the last 
      element from vector. Heapify at the top.
----------------------------------------------------------*/
{
#if verbose>=2
  cout << "BlockHeap: Remove block " << _nodes[0].id() << " from heap." << endl;
#endif

  // get min id
  int blkID = _nodes[0].id();
  // swap the min with last leaf
  swap_node(0, _nodes.size()-1);
  // mark its heap pos as null
  _hpIDs[blkID] = INDEX_NULL;
  // deleta min
  _nodes.pop_back();
  // heapify from root, retrieve heap property
  if(_nodes.size() > 0)  hpfy_min(0);

#if verbose>=2
  cout << "BlockHeap: Heap blocks: ";
  for(unsigned int i=0; i<_nodes.size(); i++)
    cout << "(" << _nodes[i].id() << "," << _nodes[i].msg_load() << ") ";
  cout << endl;
#endif

  return(blkID);
}


int BlockHeap::pop_blk(int blkID)
{
#if verbose>=2
  cout << "BlockHeap: Remove block " << blkID << " from heap." << endl;
#endif

  // swap the block with last leaf
  int heapID = _hpIDs[blkID];
  swap_node(heapID, _nodes.size()-1);
  _hpIDs[blkID] = INDEX_NULL;

  // delete the block
  _nodes.pop_back();

  // heapify from blkID's position in heap
  if(_nodes.size() > (unsigned int)heapID)  hpfy_min(heapID);

#if verbose>=2
  cout << "BlockHeap: Heap blocks: ";
  for(unsigned int i=0; i<_nodes.size(); i++)
    cout << "(" << _nodes[i].id() << "," << _nodes[i].msg_load() << ") ";
  cout << endl;
#endif

  return(0);
}


bool BlockHeap::is_in(int blkID)
{
  if(blkID >= (int)_hpIDs.size())
    return(false);
  else
    return(_hpIDs[blkID] != INDEX_NULL);
}


int BlockHeap::clear()
{
  _nodes.clear();
  _hpIDs.clear();
  return(0);
}


int BlockHeap::view()
{
  for(unsigned int i=0; i<_nodes.size(); i++){
    cout << _nodes[i].id() << " " << _nodes[i].msg_load() << endl;
  }
  
  return(0);
}
