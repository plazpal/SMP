#include "BlockPart.h"

using namespace std;


/************************       BlockPart Class        ************************/


int BlockPart::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
/*--------------------------------------------------------------
  Mthd: * Build up the heap using block ids 'bids' in graph 'bg'.
        * Pop the top of heap, if it is less than average work,
          find company and put them in one partition; if it fits
          in average, set partition; if fits in multiple partitions, 
          bisect it to partitions (cut off part if it exceeds).

  Note: * 'bids' is appended with newly generated blocks at run
          time. At the end it is restored.
--------------------------------------------------------------*/
{
  // init heap
  grid = geo; // coordinates not copied
  hp.clear();
  hp.build(grid, bids);

  // set up attributes
  _nPart = nPart;
  //..
  double wkTot = 0.0;
  for(unsigned int i=0; i<bids.size(); i++)
    wkTot += (double)grid[bids[i]].work_load();
  _wkAvg = wkTot / _nPart;
  
  // partition id always starts from 0
  iPart = 0;

  // mark blocks as undone
  // note that isDones[i] matches bids[i]
  isDones.clear();
  for(unsigned int i=0; i<bids.size(); i++)
    isDones.push_back(false);

  while(hp.size() > 0){
    // id, workload of block with min message load
    int    id   = hp.id_min();
    double work = (double)grid[id].work_load();
    
    // if partitions are used up, pack all blocks to last partition
    if(iPart == _nPart){
      hp.pop_blk(id);
      grid[id].set_partition(_nPart - 1);
#if verbose>=1
      cout << "BlockPart: Impose block " << id << "on the last partition." << endl;
#endif
      continue;
    }

#if verbose>=1
    cout << endl;
    cout << "--------" << endl;
    cout << "BlockPart: heap top block " << id << " work load " << work << endl;
#endif

    // blk's wkload small, need company
    if(work < _wkAvg*(1.0-tolerRB)){
#if verbose>=1
      cout << "BlockPart: Need company load " << _wkAvg - work \
           << " to fit in one proc" << endl;
#endif
      grow_blks_to_part(grid, bids, id);
    }
    // blk's wkload fit in one proc
    else if(work <= _wkAvg*(1.0+tolerRB)){
      isDones[vector_index(bids, id)] = true;
      grid[id].set_partition(iPart);
      iPart++;
      hp.pop_min();
#if verbose>=1
      cout << "BlockPart: Fit in one proc, assgin to partition"
           << iPart-1 << endl;
#endif
    }
    // blk's wkload large, may need cut
    else{
      bisect_blk_to_parts(grid, bids, id);
    }

    if(iPart >  _nPart){
      cerr << "Fatal: use more partitions than assigned." << endl;
      exit(EXIT_FAILURE);
    }
  }// end while
 
  // update msg load
  for(uint i=0; i<bids.size(); i++)
    grid.cmpt_time(bids[i]);

  return(0);
}


int BlockPart::grow_blks_to_part(BlockGraph& grid, vector<int>& bids, int id)
{
  double      workSum = (double)grid[id].work_load();
  int         cmpny   = BLOCK_NULL;
  vector<int> localBids;

  // move min msg block from heap to local blocks (in same partition)
  hp.pop_blk(id);
  localBids.push_back(id);

  // find the companies
  while(workSum < _wkAvg*(1.0-tolerRB)){
    // find the company minimizing global message
    find_min_cmpny(grid, bids, localBids, cmpny);
    if(cmpny == BLOCK_NULL) break;
    // save the company
    workSum += (double)grid[cmpny].work_load();
    localBids.push_back(cmpny);
#if verbose>=1
    cout << "BlockPart: Find company "<< cmpny << ", " << grid[cmpny].work_load() << endl;
#endif
    // remove from heap
    hp.pop_blk(cmpny);
  }

  // set partition, and mark these blocks as done
  for(unsigned int i=0; i<localBids.size(); i++){
    grid[localBids[i]].set_partition(iPart);
    int pos = vector_index(bids, localBids[i]);
    isDones[pos] = true;
  }
  iPart++;
  // set the map through shared mem
  grid.set_shmem_map(localBids.size(), &localBids[0]);

#if verbose>=1
  if(cmpny == BLOCK_NULL)
    cout << "BlockPart: fail to find enough company, assign anyway." << endl;
  cout << "BlockPart: assign blocks ";
  for(unsigned int i=0; i<localBids.size(); i++)
    cout << localBids[i] << " ";
  cout << "to partition " << iPart-1 << endl;
#endif

  return(0);
}


int BlockPart::bisect_blk_to_parts(BlockGraph& grid, vector<int>& bids, int id)
{
  double  work     = (double)grid[id].work_load();
  int     nProcblk = (int)floor(work / _wkAvg); // #_parts for this block
  bool    isUsedUp = (iPart + nProcblk >= _nPart);  // is all _parts used up
    
  nProcblk = min(nProcblk, _nPart - iPart);

  // if exceeds nProcblk's load, then cut off a part
  if((work/_wkAvg - nProcblk) > tolerRB && !isUsedUp){ // if: cut
#if verbose>=1
    cout << "BlockPart: Exceed " << nProcblk
         << " blocks, need to cut off " << work-_wkAvg*nProcblk << endl;
#endif
    BlockCut cut;
    grid.find_min_blkcut(id, work - _wkAvg*nProcblk, tolerRB, cut);
    // if the cut is found
    if(cut.blkID != BLOCK_NULL){
      grid.cut_block(cut);
      // if the cutoff is appended to grid.blks
      if(cut.isKept){
        // mark cutoff as undone and add to bids and heap
        bids.push_back(grid.size()-1);
        isDones.push_back(false);
        hp.append_blk(grid[grid.size()-1]);
        // bisect remaining block
        _bisect_blk(grid, bids, id, nProcblk);
        hp.pop_blk(id);
      }
      // if the cutoff is saved at id
      else{
        // cutoff is in place, change it load
        hp.chng_load(id, grid[id].msg_load());
        // mark appended block as undone and add to bids
        isDones.push_back(false);
        bids.push_back(grid.size()-1);
        // blk will be bisect so only need to do add null index to heap
        hp.append_blk(BLOCK_NULL);
        _bisect_blk(grid, bids, grid.blks.size()-1, nProcblk);
      }
    }
        // cut not found
    else{
#if verbose>=1
      cout << "BlockPart: Fail to find a cut within tolerance, bisect anyway" << endl;
#endif
      _bisect_blk(grid, bids, id, nProcblk);
      hp.pop_blk(id);
    }
  }// end if: cut
  // fit in nProcblk's load or use up partitions
  else{
#if verbose>=1
    if(isUsedUp)
      cout << "BlockPart: Block used up remaining " << nProcblk << " partitions." << endl;
    else
      cout << "BlockPart: fit in " << nProcblk << " partitions, bisect." << endl;
#endif
    _bisect_blk(grid, bids, id, nProcblk);
    hp.pop_blk(id);
  }

  return(0);
}


int BlockPart::find_min_cmpny(BlockGraph& bg, vector<int>& bids, vector<int>& localBids, \
                              int& cmpny)
{
  int      work = 0;
  int      blkMin;
  double   msgIncrMin = DLARGE, msgIncr;
  BlockCut cutMin(DLARGE), cut;

  // default value, company not found, will be overwritten if found
  cmpny = BLOCK_NULL;

  // find the load needed
  for(unsigned int i=0; i<localBids.size(); i++){
    work += bg[localBids[i]].work_load();
  }
  double load = _wkAvg - (double)work;

  for(unsigned int i=0; i<bids.size(); i++){
    if(!isDones[i]){
      // skip blocks already in current partition
      if(find(localBids.begin(), localBids.end(), bids[i]) != localBids.end()) continue;
      // if whole block fit in the remaining space
      if(bg[bids[i]].work_load() < load*(1.0+tolerRB)){
        // decrease in global message
        msgIncr = 0.0;
        for(unsigned int j=0; j<localBids.size(); j++)
          msgIncr -= bg.msg_load(bids[i], localBids[j]);
        // mark the min message block
        if(msgIncr < msgIncrMin){
          msgIncrMin = msgIncr;
          blkMin     = bids[i];
        }
      }
      // find a cutoff part to fit in the remaining space
      else{
        bg.find_min_blkcut(bids[i], load, tolerRB, cut, localBids);
        if(cut < cutMin)  cutMin = cut;
      }
    }
  }

  // if cutoff minimize the global msg
  if(cutMin.msgIncr < msgIncrMin && cutMin.blkID != BLOCK_NULL){
    // cut block
    bg.cut_block(cutMin);
    // mark newly appended block as undone and add to bids, heap
    isDones.push_back(false);
    bids.push_back(bg.size()-1);
    hp.append_blk(bg[bg.size()-1]);
    // change the message load of cut block
    hp.chng_load(cutMin.blkID, bg[cutMin.blkID].msg_load());
    // update nbr's msg load and pos in heap
    update_heap_nbr(bg, cutMin.blkID);
    update_heap_nbr(bg, bg.size()-1);
    // set company id
    if(cutMin.isKept)
      cmpny = bg.size()-1;
    else
      cmpny = cutMin.blkID;
  }

  // if a whole block minimize the the global msg
  if(msgIncrMin <= cutMin.msgIncr && msgIncrMin < DLARGE){
    cmpny = blkMin;
  }
    
  return(0);
}


int BlockPart::_bisect_blk(BlockGraph& bg, vector<int>& bids, int id, int nProc)
{
#if verbose>=1
  cout << "BlockPart: Bisect block " << id << " begin." << endl;
#endif
  unsigned int size0   = bg.size();
  unsigned int nNewBlk = 0;
  vector<int>  npNewBlks;

  // bisect id until it fits in one proc
  _bisect_to_one_part(bg, id, nProc, npNewBlks);
  isDones[vector_index(bids, id)] = true;

  // bisect blocks appended to bg
  // This will traverse from size0 to the last block of current graph.
  // if '_bisect_to_one_part' cut a block to multiple blocks (partitions), the 
  // current graph size increases and remains greater than size0+nNewBlk. 
  // They equal only when the last block fits in one partition.
  while(bg.size() > (int)(size0 + nNewBlk)){
    _bisect_to_one_part(bg, size0+nNewBlk, npNewBlks[nNewBlk], npNewBlks);
    // append block to bids, mark as done
    bids.push_back(size0+nNewBlk);
    isDones.push_back(true);
    // the block is done, append null to heap to maintain the index mapping
    hp.append_blk(BLOCK_NULL);
    nNewBlk++;
  }
#if verbose>=1
  cout << "BlockPart: Bisect block " << id << " end." << endl;
#endif

  return(0);
}


int BlockPart::_bisect_to_one_part(BlockGraph& bg, int id, int nProc, vector<int>& npNewBlks)
{
  // workload and #proc
  double workBlk  = (double)bg[id].work_load();
  int    nProcBlk = nProc;

  // bisect the block recursively
  while(nProcBlk > 1){
    int    halfFlr = nProcBlk/2, halfCl = nProcBlk - nProcBlk/2;
    BlockCut cut;
    bg.find_min_blkcut(id, halfFlr*workBlk/nProcBlk, tolerRB, cut);
    bg.cut_block(cut);
    //bg.find_time_bisect(id, r, e, cut);
    //bg.cut_block(cut);
    // update cut blocks' nbr msg load and pos in heap
    update_heap_nbr(bg, id);
    update_heap_nbr(bg, bg.size()-1);
    // setup next iteration
    if(cut.isKept){
      npNewBlks.push_back(halfFlr);
      nProcBlk = halfCl;
    }
    else{
      npNewBlks.push_back(halfCl);
      nProcBlk = halfFlr;
    }
    workBlk = (double)bg.blks[id].work_load();
  }
#if verbose>=1
  cout << "BlockPart: Cut Block " << id 
       << " down to Partition "   << iPart << endl;
#endif

  bg.blks[id].set_partition(iPart);
  iPart++;

  return(0);
}


int BlockPart::update_heap_nbr(BlockGraph& bg, int bid)
{
  for(int iFc=0; iFc<6; iFc++){
    BlockFace *ptFc = bg[bid].face_pt(iFc);
    MapIter    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){
        bg.cmpt_time(iter->toBlk);
        if(hp.is_in(iter->toBlk))
          hp.chng_load(iter->toBlk, bg[iter->toBlk].msg_load());
      }
      iter++;
    }
  }
  return(0);
}
