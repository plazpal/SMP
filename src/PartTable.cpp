#include "PartTable.h"


/************************        TableRow Class        ************************/


int TableRow::add(int toPart, BlockCut& cut)
{
  // find the insert position
  unsigned int pos = 0; // insert position
  while(pos < _cuts.size()){
    if(_cuts[pos] < cut)
      pos++;
    else
      break;
  }

  // shift elements after pos, if any.
  // insert cut at pos 
  _targets.push_back(toPart);
  _cuts.push_back(cut);
  for(unsigned int i=_cuts.size()-1; i>pos; i--){
    _targets[i] = _targets[i-1];
    _cuts[i]    = _cuts[i-1];
  }
  _cuts[pos]    = cut;
  _targets[pos] = toPart;

  return(0);
}


int TableRow::del_top()
{
  for(unsigned int i=0; i<_cuts.size()-1; i++){
       _cuts[i] =    _cuts[i+1];
    _targets[i] = _targets[i+1];
  }

  if(_cuts.size() > 0){
       _cuts.pop_back();
    _targets.pop_back();
  }

  return(0);
}


int TableRow::del_target(int toPart)
{
  for(unsigned int i=0; i<_targets.size(); i++){
    if(_targets[i] == toPart){
      for(unsigned int j=i; j<_targets.size()-1; j++){
        _targets[j] = _targets[j+1];
        _cuts[j]    = _cuts[j+1];
      }
      _targets.pop_back();
      _cuts.pop_back();
      break;
    }
  }

  return(0);
}


int TableRow::pos(int toPart, const BlockCut& cut)
{
  for(unsigned int i=0; i<_targets.size(); i++)
    if(toPart == _targets[i] && _cuts[i] == cut)  return((int)i);

  return(INDEX_NULL);
}


int TableRow::pos(int toPart, int bid)
{
  for(unsigned int i=0; i<_targets.size(); i++)
    if(toPart == _targets[i] && _cuts[i].blkID == bid)  return((int)i);

  return(INDEX_NULL);
}


int TableRow::del_element(int idx)
{
  for(unsigned int i=(unsigned int)idx; i<_targets.size()-1; i++){
    _targets[i] = _targets[i+1];
    _cuts[i]    = _cuts[i+1];
  }
  _targets.pop_back();
  _cuts.pop_back();

  return(0);
}


int TableRow::del_element(const BlockCut& cut)
{
  for(int i=(int)_targets.size()-1; i>=0; i--){
    if(_cuts[i] == cut){
      for(int j=i+1; j<(int)_targets.size(); j++){
        _targets[j-1] = _targets[j];
        _cuts[j-1]    = _cuts[j];
      }
      _targets.pop_back();
      _cuts.pop_back();
    }
  }

  return(0);
}


int TableRow::chng_element(int idx, int toPart, const BlockCut& cut)
{
  _targets[idx] = toPart;
  _cuts[idx]    = cut;

  int i = idx;

  // exit loop if i moves to one end
  while(i > 0 && i < (int)_cuts.size()-1){
    // swap forward if smaller
    if(_cuts[i] < _cuts[i-1]){
      swap(_cuts[i],    _cuts[i-1]);
      swap(_targets[i], _targets[i-1]);
      i--;
    }
    // swap forward if smaller
    else if(_cuts[i] > _cuts[i+1]){
      swap(_cuts[i],    _cuts[i+1]);
      swap(_targets[i], _targets[i+1]);
      i++;
    }
    // fit, terminate loop
    else
      break;
  }

  return(0);
}


int TableRow::clear()
{
  _targets.clear();
  _cuts.clear();

  return(0);
}


int TableRow::view()
{
  cout << _pid << " ";
  for(unsigned int i=0; i<_targets.size(); i++)
    cout << "(" << _targets[i] << ", " << _cuts[i].blkID << ", " 
         << _cuts[i].axis << ", " << _cuts[i].pos << ", " 
         << _cuts[i].msgIncr << ")  ";
  cout << endl;

  return(0);
}


/************************       PartTable Class        ************************/


PartTable::PartTable(int numProc)
{
  _rowIDs.reserve(numProc);
  for(int i=0; i<numProc; i++)
    _rowIDs.push_back(INDEX_NULL);
}


bool PartTable::is_empty()
{
  bool isEmpty = true;

  if(_rows.size() != 0){
    for(unsigned int i=0; i<_rows.size(); i++){
      if(!_rows[i].is_empty()){
        isEmpty = false;
        break;
      }
    }
  }

  return(isEmpty);
}


TableRow& PartTable::operator[](int i)
{
  assert((unsigned int)i < _rowIDs.size() && i >= 0);
  assert(_rowIDs[i] != INDEX_NULL);

  return(_rows[_rowIDs[i]]);
}


int PartTable::del_row(int pid)
{
  int idLast = _rows.back().part_id();

  // swap with last row then delete it
  swap(_rows[_rowIDs[pid]], _rows.back());
  _rows.pop_back();

  _rowIDs[idLast] = _rowIDs[pid];
  _rowIDs[pid]    = INDEX_NULL;

  return(0);
}


int PartTable::clear_row(int pid)
{
  _rows[_rowIDs[pid]].clear();
  return(0);
}


int PartTable::add(int pid, int toPart, BlockCut& cut)
{
  if(_rowIDs[pid] == INDEX_NULL){
    cerr << "Error: input part " << pid << " mapped to null row" << endl;
    exit(-1);
  }

  _rows[_rowIDs[pid]].add(toPart, cut);
  return(0);
}


int PartTable::pop(int& pid, int& toPart, BlockCut& cut)
{
  pid         = INDEX_NULL;
  toPart      = INDEX_NULL;
  cut.msgIncr = DLARGE;

  // find the cut with min msg incr
  BlockCut cutTmp;
  int      target;
  for(unsigned int i=0; i<_rows.size(); i++){
    if(!_rows[i].is_empty()){
      _rows[i].top(target, cutTmp);
      if(cutTmp < cut){
        pid    = _rows[i].part_id();
        cut    = cutTmp;
        toPart = target;
      }
    }  
  }

  // remove the cut from row
  _rows[_rowIDs[pid]].del_top();

  return(0);
}


int PartTable::del_target(int toPart)
{
  for(unsigned int i=0; i<_rows.size(); i++)
    if(!_rows[i].is_empty())  _rows[i].del_target(toPart);

  return(0);
}


int PartTable::view()
{
  for(unsigned int i=0; i<_rows.size(); i++)
    _rows[i].view();

  return(0);
}
