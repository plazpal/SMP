#include "Partition.h"


/************************       Partition Class        ************************/


Partition::Partition(const Partition& rhs)
{
  id       = rhs.id;
  blkIDs   = rhs.blkIDs;
  nbrs     = rhs.nbrs;
  _work    = rhs._work;
  _wkIdeal = rhs._wkIdeal;
  _t       = rhs._t;
  _tComp   = rhs._tComp;
  _tComm   = rhs._tComm;
  _cmpr    = rhs._cmpr;
  _nShr    = rhs._nShr;
  _nComm   = rhs._nComm;
  _volComm = rhs._volComm;
  _volShr  = rhs._volShr;
  _volDirectCpy = rhs._volDirectCpy;
  _volIndirectCpy = rhs._volIndirectCpy;
}


const Partition& Partition::operator=(const Partition& rhs)
{
  if(this != &rhs){
    id       = rhs.id;
    blkIDs   = rhs.blkIDs;
    nbrs     = rhs.nbrs;
    _work    = rhs._work;
    _wkIdeal = rhs._wkIdeal;
    _t       = rhs._t;
    _tComp   = rhs._tComp;
    _tComm   = rhs._tComm;
    _cmpr    = rhs._cmpr;
    _nShr    = rhs._nShr;
    _nComm   = rhs._nComm;
    _volComm = rhs._volComm;
    _volShr  = rhs._volShr;
    _volDirectCpy = rhs._volDirectCpy;
    _volIndirectCpy = rhs._volIndirectCpy;
  }

  return(*this);
}


bool Partition::operator<(const Partition& rhs) const
{
  if(_cmpr == NUM_CELL)
    return(_work < rhs._work);
  else if(_cmpr == TIME)
    return(_t < rhs._t);
  else
    exit(-1);
}


bool Partition::operator>(const Partition& rhs) const
{
  if(_cmpr == NUM_CELL)
    return(_work > rhs._work);
  else if(_cmpr == TIME)
    return(_t > rhs._t);
  else
    exit(-1);
}


int Partition::setup_nbr(BlockGraph& bg)
{
  BlockFace *ptFc;

  // empty current nbr array
  nbrs.clear();

  // add nbrs
  for(unsigned int i=0; i<blkIDs.size(); i++){
    for(int iFc=0; iFc<6; iFc++){//face
      bg.blks[blkIDs[i]].get_face_pt(iFc, &ptFc);
      list<BcMap>::iterator iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0){// b2b
          int toPart = bg.blks[iter->toBlk].partition_id();
          if(toPart != id){
            if(nbrs.empty() || find(nbrs.begin(), nbrs.end(), toPart) == nbrs.end()){
              nbrs.push_back(toPart);
              //if(id == 5) cout << blkIDs[i] << " " << iter->toBlk << " " << toPart << endl;
            }
          }
        }// b2b
        iter++;
      }
    }// face loop
  }

  return(0);
}


int Partition::del_blk(int bid)
{
  for(unsigned int i=0; i<blkIDs.size(); i++){
    if(blkIDs[i] == bid){
      swap(blkIDs[i], blkIDs.back());
      blkIDs.pop_back();
      break;
    }
  }
  return(0);
}


int Partition::chng_blk(int bid, int bidNew)
{
  for(unsigned int i=0; i<blkIDs.size(); i++){
    if(blkIDs[i] == bid){
      blkIDs[i] = bidNew;
      break;
    }
  }
  return(0);
}


int Partition::view()
{
  cout << "partition" << right << setw(6) << id << " blocks: ";
  for(unsigned int i=0; i<blkIDs.size(); i++)
    cout << right << setw(6) << blkIDs[i];
  cout << endl;

  cout << right << setw(24) << "work: " 
       << right << setw(6)  << _work << endl;;

  cout << right << setw(24) << "time: " 
       << right << setw(6)  << _t << endl;;

  cout << right << setw(24) << "nbrs: ";
  for(unsigned int i=0; i<nbrs.size(); i++)
    cout << right << setw(6) << nbrs[i];
  cout << endl;

  return(0);
}


int Partition::del_shmem_map(BlockGraph& bg, int bid)
{
  BlockFace *ptFc;
  list<BcMap>::iterator iter, iterInv;

  for(int i=0; i<6; i++){
    bg[bid].get_face_pt(i, &ptFc);
    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){// b2b
        if(iter->toBlk != bid && 
           find(blkIDs.begin(), blkIDs.end(), iter->toBlk) != blkIDs.end())
        {
          iter->isShMem = false;
          // set inverse map
          iterInv = bg.inv_map_iter(*iter);
          iterInv->isShMem = false;
        }
      }
      iter++;
    }
  }

  return(0);
}


int Partition::add_shmem_map(BlockGraph& bg, int bid)
{
  for(int i=0; i<6; i++){
    BlockFace *ptFc = bg[bid].face_pt(i);
    MapIter    iter = ptFc->mapList.begin();
    while(iter != ptFc->mapList.end()){
      if(iter->bcType.compare("Block2Block") == 0){// b2b
        if(iter->toBlk != bid && 
           find(blkIDs.begin(), blkIDs.end(), iter->toBlk) != blkIDs.end())
        {
          iter->isShMem = true;
          // set inverse map
          MapIter iterInv = bg.inv_map_iter(*iter);
          iterInv->isShMem = true;
        }
      }// b2b if
      iter++;
    }
  }

  return(0);
}


int Partition::cmpt_msg_load(BlockGraph& bg)
{
  for(unsigned int i=0; i<blkIDs.size(); i++){
    bg.cmpt_msg_load( blkIDs[i] );
  }
  
  return(0);
}


int Partition::cmpt_time(BlockGraph& bg)
{
  _t       = 0.0;
  _tComp   = 0.0;
  _tComm   = 0.0;
  _work    = 0.0;
  _nShr    = 0;
  _nComm   = 0;
  _volShr  = 0.0;
  _volComm = 0.0;
  _volDirectCpy = 0.0;
  _volIndirectCpy = 0.0;
  for(unsigned int i=0; i<blkIDs.size(); i++){
    _work  += bg[blkIDs[i]].work();
    _t     += bg[blkIDs[i]].time();
    _tComp += bg[blkIDs[i]].time_comp();
    _tComm += bg[blkIDs[i]].msg_load();
    for(int j=0; j<6; j++){
      BlockFace *ptFc = bg[blkIDs[i]].face_pt(j);
      CMapIter   iter = ptFc->mapList.begin();
      while(iter != ptFc->mapList.end()){
        if(iter->bcType.compare("Block2Block") == 0){
          double vol = max(iter->rngs[5] - iter->rngs[2], 1) \
                     * max(iter->rngs[4] - iter->rngs[1], 1) \
                     * max(iter->rngs[3] - iter->rngs[0], 1) \
                     * (double)bg.num_halo() * bg.cell_data_size();
          if(iter->isShMem){
            _nShr++;
            _volShr += vol;
          }
          else{
            _nComm++;
            _volComm += vol;
            if (iter->is_direct(j))
              _volDirectCpy   += vol;
            else
              _volIndirectCpy += vol;
          }
        }
        iter++;
      }
    }
  }
  
  return(0);
}


/************************       PartArray Class        ************************/


PartArray::PartArray(const int& nPart)
{
  _nPart = nPart;
  _vals  = new Partition [_nPart];
  _keys  = new int [_nPart];

  for(int i=0; i<_nPart; i++){
    _vals[i].id = i;
    _keys[i]    = i;
  }

  _order = NUM_CELL;
}


PartArray::PartArray(const PartArray& rhs)
{
  _nPart = rhs._nPart;
  _vals  = new Partition [_nPart];
  _keys  = new int [_nPart];
  _order = rhs._order;

  for(int i=0; i<_nPart; i++){
    _vals[i] = rhs._vals[i];
    _keys[i] = rhs._keys[i];
  }
}


const PartArray& PartArray::operator=(const PartArray& rhs)
{
  if(this != &rhs){
    // # elements is different
    if(_nPart != rhs._nPart){
      _nPart = rhs._nPart;
      delete [] _vals;
      delete [] _keys;
      _vals  = new Partition [_nPart];
      _keys  = new int [_nPart];
    }
    // copy elements
    for(int i=0; i<_nPart; i++){
      _vals[i] = rhs._vals[i];
      _keys[i] = rhs._keys[i];
    }

    _order = rhs._order;
  }

  return(*this);
}


PartArray::~PartArray()
{
  delete [] _vals;
  delete [] _keys;
}


void PartArray::set_order(const int order)
{
  _order = order;
  for(int i=0; i<_nPart; i++)
    _vals[i].set_comparison(order);
}


int PartArray::build()
{
  sort(_vals, _vals+_nPart);

  // set keys
  for(int i=0; i<_nPart; i++)
    _keys[_vals[i].id] = i;

  return(0);
}


int PartArray::add_load(int i, double load)
/*----------------------------------------------------------
  Mthd: Change the work load of ith parition and exchange pos
        with neighbors until it moves to the right pos.
        This takes linear time in worst case.
----------------------------------------------------------*/
{
  _vals[_keys[i]].add_load(load);
  int j = _keys[i];

  // shift to right position if sorted by work
  if(_order == NUM_CELL){
    // move towards end
    if(load > 0){
      while(_vals[j] > _vals[j+1] && j < _nPart-1){
        _keys[_vals[j  ].id] = j+1;
        _keys[_vals[j+1].id] = j;
        swap(_vals[j], _vals[j+1]);
        j++;
      }
    }
    // move towards beginning
    else{
      while(_vals[j] < _vals[j-1] && j > 0){
        _keys[_vals[j  ].id] = j-1;
        _keys[_vals[j-1].id] = j;
        swap(_vals[j], _vals[j-1]);
        j--;
      }
    }
  }

  return(0);
}


int PartArray::find_part_shift(BlockGraph& bg, int pid0, int pid1, double toler,\
                               BlockCut& cut, bool isBlnc)
/*----------------------------------------------------------
Mthd: * traverse each face of each block to check if can cut at 
        the boundary of a b2b map, which may save latency.
      * pick the cut with min commucation increase
      * compute the above cut with the cut of ideal work load
        pick the min
Note: A cut may include the whole block.
----------------------------------------------------------*/
{
  int      bid0, plFcs[4], rngs[6];
  double   space;
  BlockCut cutTmp(DLARGE);
  
  // set space 
  int key0 = _keys[pid0], key1 = _keys[pid1];
  if(isBlnc)
    space = min(_vals[key0].work() - _vals[key0].ideal_work()*(1.0-toler),\
                _vals[key1].ideal_work()*(1.0+toler) - _vals[key1].work());
  else
    space = _vals[key0].work() - _vals[key0].ideal_work()*(1.0-toler);

  // init cut
  cut = cutTmp;

  for(unsigned int i=0; i<_vals[_keys[pid0]].blkIDs.size(); i++){
    bid0         = _vals[_keys[pid0]].blkIDs[i];
    cutTmp.blkID = bid0;
    bg[bid0].get_range(rngs);
    // if whole block fits in space
    if(space >= (double)bg[bid0].work_load()){
      cutTmp.blkID  = bid0;      
      cutTmp.axis   = 0;
      cutTmp.pos    = rngs[3];
      cutTmp.isKept = false;
      bg.cmpt_msg_incr(cutTmp, pid1);
      if(cutTmp < cut)  cut = cutTmp;
    }
    // if block exceeds space and need cut
    else{
      // traverse faces
      for(int iFc=0; iFc<6; iFc++){
        // skip if face is too large, else set cut
        if(bg[bid0].area(iFc) > space) continue;
        // index range on the cut axis that fits in the space
        // cannot cut at beginning, can include whole block
        int posFlr, posCl;
        if(iFc < 3){
          posFlr = rngs[iFc]+bg.num_halo();
          posCl  = min(rngs[iFc] + (int)floor(space/bg[bid0].area(iFc)), \
                       rngs[iFc+3] - bg.num_halo());
        }
        else{
          posCl  = rngs[iFc]-bg.num_halo();
          posFlr = max(rngs[iFc] - (int)floor(space/bg[bid0].area(iFc)), \
                       rngs[iFc-3] + bg.num_halo());
        }
        // skip the rest if cut cannot be found
        if(posCl < posFlr)  continue;
        // set cut basics
        cutTmp.axis   = iFc %  3;
        cutTmp.isKept = iFc >= 3;
        // parallel faces
        plFcs[0] = EdgeMap3d[iFc%3][0];  plFcs[2] = plFcs[0] + 3;
        plFcs[1] = EdgeMap3d[iFc%3][1];  plFcs[3] = plFcs[1] + 3;
        // traverse maps on parallel faces
        for(int j=0; j<4; j++){// prll
          BlockFace *ptPlFc = bg[bid0].face_pt(plFcs[j]);
          MapIter    iterPl = ptPlFc->mapList.begin();
          while(iterPl != ptPlFc->mapList.end()){
            if(iterPl->bcType.compare("Block2Block") == 0){
              // if map range intersect with cut range then check cut
              if(iterPl->rngs[iFc%3] >= posFlr && iterPl->rngs[iFc%3] <= posCl){
                cutTmp.pos = iterPl->rngs[iFc%3];
                bg.cmpt_msg_incr(cutTmp, pid1);
                if(cutTmp < cut)  cut = cutTmp;
              }
              //..
              if(iterPl->rngs[iFc%3+3] >= posFlr && iterPl->rngs[iFc%3+3] <= posCl){
                cutTmp.pos = iterPl->rngs[iFc%3+3];
                bg.cmpt_msg_incr(cutTmp, pid1);
                if(cutTmp < cut)  cut = cutTmp;
              }
            }
            iterPl++;
          }
        }//end prll loop

        // in case no b2b bc on parallel face intersects cut range
        int len = (int)floor( min(space,  _vals[_keys[pid0]].work() \
                                        - _vals[_keys[pid0]].ideal_work()) \
                            / bg[bid0].area(iFc));
        if(len > 0){
          if(iFc < 3) {
            if (rngs[iFc]+len < posFlr) continue;
            cutTmp.pos = min(rngs[iFc] + len, posCl);
          }
          else {
            if (rngs[iFc]-len > posCl) continue;
            cutTmp.pos = max(rngs[iFc]-len, posFlr);
          }
          //..
          bg.cmpt_msg_incr(cutTmp, pid1);
          if(cutTmp < cut)  cut = cutTmp;
        }
      }
    }
  }

  return(0);
}


int PartArray::shift_part(BlockGraph& bg, int toPart, BlockCut& cut)
{
  int rngs[6];
  bg[cut.blkID].get_range(rngs);

  int pid = bg[cut.blkID].partition_id();

  // check move the cut-off or block to target
  int bid;
  // cut from end
  if(cut.isKept){
    // if move whole block
    if(rngs[cut.axis] == cut.pos){
      bid = cut.blkID;
      bg[bid].set_partition(toPart);
      _vals[_keys[pid]].del_blk(bid);
    }
    // else move cut-off
    else{
      bg.cut_block(cut);
      bid = bg.size()-1;
      bg[bid].set_partition(toPart);
    }
  }
  // cut from beginning
  else{
    // if move whole block
    if(rngs[cut.axis+3] == cut.pos){
      bid = cut.blkID;
      bg[bid].set_partition(toPart);
      _vals[_keys[pid]].del_blk(bid);
    }
    // else move cut-off
    else{
      bg.cut_block(cut);
      bid = cut.blkID;
      bg[bg.size()-1].set_partition(pid);
      _vals[_keys[pid]].chng_blk(bid, bg.size()-1);
      bg[bid].set_partition(toPart);
    }
  }
  _vals[_keys[toPart]].blkIDs.push_back(bid);
  _vals[_keys[pid]].del_shmem_map(bg, bid);
  _vals[_keys[toPart]].add_shmem_map(bg, bid);

  // update work, msg load
  add_load(pid,   -(double)bg[bid].work_load());
  add_load(toPart, (double)bg[bid].work_load());
  _vals[_keys[pid]]. cmpt_msg_load(bg);
  _vals[_keys[toPart]]. cmpt_msg_load(bg);

#if verbose>=1
  cout << "Partitioner: Shift " << bg[bid].work_load() 
       << " from " << pid << " to " << toPart << endl;
  cout << "Partitioner: part " << pid << " " 
       << (_vals[_keys[pid]].work() - _vals[_keys[pid]].ideal_work())/_vals[_keys[pid]].ideal_work()
       << " " << toPart << " "
       << (_vals[_keys[toPart]].work() - _vals[_keys[toPart]].ideal_work())/_vals[_keys[toPart]].ideal_work()
       << endl;
#endif

  // update nbr
  _vals[_keys[pid]].setup_nbr(bg);
  _vals[_keys[toPart]].setup_nbr(bg);

  return(0);
}


int PartArray::merge_part_blk(BlockGraph& bg, int pid)
{
  int isMrged = true;
  int nCmpny, *cmpnys;

  // merge blocks as many as possible in each partition
  while(isMrged){
    isMrged = false;
    for(unsigned int j=0; j<_vals[_keys[pid]].blkIDs.size(); j++){
      if(bg[_vals[_keys[pid]].blkIDs[j]].id() != BLOCK_NULL){
        nCmpny  = (int)_vals[_keys[pid]].blkIDs.size();
        cmpnys  = &(_vals[_keys[pid]].blkIDs[0]);
        isMrged = isMrged || bg.merge(_vals[_keys[pid]].blkIDs[j], nCmpny, cmpnys);
        if(isMrged)  break;
      }
    }
  }

  _rm_null_blk(bg);

  // set up each partition's neighbor info
  _vals[_keys[pid]].nbrs.clear();
  _vals[_keys[pid]].setup_nbr(bg);

  // set message load
  _vals[_keys[pid]].cmpt_msg_load(bg);

#if verbose>=1
  cout << "Partition: merge blocks in part " << pid << endl;
#endif

  return(0);
}


int PartArray::merge_part_blk(BlockGraph& bg)
{
  int isMrged;
  int nCmpny, *cmpnys;

  // merge blocks as many as possible in each partition
  for(int i=0; i<_nPart; i++){
    isMrged = true;
    while(isMrged){
      isMrged = false;
      for(unsigned int j=0; j<_vals[_keys[i]].blkIDs.size(); j++){
        if(bg[_vals[_keys[i]].blkIDs[j]].id() != BLOCK_NULL){
          nCmpny  = (int)_vals[_keys[i]].blkIDs.size();
          cmpnys  = &(_vals[_keys[i]].blkIDs[0]);
          isMrged = isMrged || bg.merge(_vals[_keys[i]].blkIDs[j], nCmpny, cmpnys);
          if(isMrged)  break;
        }
      }
    }
  }

  _rm_null_blk(bg);

  // set up each partition's neighbor info
  for(int i=0; i<_nPart; i++){
    _vals[i].nbrs.clear();
    _vals[i].setup_nbr(bg);
  }

  // set message load
  for(int i=0; i<_nPart; i++)
    _vals[_keys[i]].cmpt_msg_load(bg);

#if verbose>=1
  cout << "Partition: Merge blocks in the same partition." << endl;
#endif

  return(0);
}


int PartArray::_rm_null_blk(BlockGraph& bg)
{
  // remove all null blocks from partition
  for(int i=0; i<bg.size(); i++){
    if(bg[i].id() == BLOCK_NULL){
      int pid = bg[i].partition_id();
      for(unsigned int k=0; k<_vals[_keys[pid]].blkIDs.size(); k++){
        if(_vals[_keys[pid]].blkIDs[k] == (int)i){
          _vals[_keys[pid]].del_blk(i);
          break;
        }
      }     
    }
  }

  // clean null blocks in middle, size() is changing
  int nSft = 0;
  int i    = 0;
  while(i < bg.size()){
    if(bg[i].id() == BLOCK_NULL){
      nSft++;
      i++;
    }
    else{
      // if previous several blocks are null
      if(nSft > 0){// pre null
        // update block id and related partition, map info
        for(int j=i; j<bg.size(); j++){// post blk
          if(bg[j].id() != BLOCK_NULL){
            // change toBlk id of b2b map targeting here
            for(int iFc=0; iFc<6; iFc++){
              BlockFace *ptFc = bg[j].face_pt(iFc);
              MapIter    iter = ptFc->mapList.begin();
              while(iter != ptFc->mapList.end()){// map
                if(iter->bcType.compare("Block2Block") == 0){
                  MapIter iterInv = bg.inv_map_iter(*iter);
                  iterInv->toBlk -= nSft;
                }
                iter++;
              }// map loop
            }
            // change block id in partition
            int pid = bg[j].partition_id();
            for(unsigned int k=0; k<_vals[_keys[pid]].blkIDs.size(); k++){
              if(_vals[_keys[pid]].blkIDs[k] == j){
                _vals[_keys[pid]].blkIDs[k] = j - nSft;
                break;
              }
            }
            // change block id
            bg[j].set_blockID(j-nSft);
          } 
          // shift block to correct position
          bg[j-nSft] = bg[j]; 
        }// post blk loop
        // remove null blocks from bg, actual shift takes place
        for(int j=0; j<nSft; j++)
          bg.pop_back();
        // reset null number and loop index
        i   -= nSft;
        nSft = 0;
      }// pre null if
      else{
        i++;
      }
    }
  }// while

  // clean possible null blocks at the end, nSft=0 if last block is not null
  for(int i=0; i<nSft; i++)
    bg.pop_back();

  return(0);
}


int PartArray::clear()
{
  for(int i=0; i<_nPart; i++){
    _vals[i].blkIDs.clear();
    _vals[i].nbrs.clear();
    _vals[i].set_work(0.0);
    _vals[i].set_ideal_work(0.0);
    _keys[i] = i;
  }
  return(0);
}
