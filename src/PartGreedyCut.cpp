#include "PartGreedyCut.h"


int PartGreedyCut::decompose(BlockGraph& geo, vector<int>& bids, int nPart, BlockGraph& grid)
{
  grid   = geo;
  _iPart = 0;
  _nPart = nPart;

  // mark blocks undone, isDones[i] maps to bids[i]
  _isDones.clear();
  for(unsigned int i=0; i<bids.size(); i++)
    _isDones.push_back(false);

  // mark each block's main zone and residual zone if any
  divide_zone(grid, bids);

  // decompose residual zones
  assign_r_zone(grid, bids);

  // cut all the main zones
  uint size0 = bids.size();
  for(uint i=0; i<size0; i++)
    if(!_isDones[i] && _nLoads[i] > 0) cut_mzone_reb(grid, bids, bids[i]);

  // update blocks message load
  for(uint i=0; i<bids.size(); i++)
    grid.cmpt_time(bids[i]);

  return(0);
}


int PartGreedyCut::divide_zone(BlockGraph& bg, vector<int>& bids)
{
  // average work load
  double wkTot = 0.0;
  for(unsigned int i=0; i<bids.size(); i++)
    wkTot += (double)bg[bids[i]].work_load();
  _wkAvg = wkTot / _nPart;

  // reset vectors
  // element i maps to bids[i]
  _rCls.clear();
  _rFlrs.clear();
  _nLoads.clear();
  for(unsigned int i=0; i<bids.size(); i++){
    _rCls.push_back(0.0);
    _rFlrs.push_back(0.0);
    _nLoads.push_back(1);
  }

  // set main zone and residual zone load
  for(unsigned int i=0; i<bids.size(); i++){
    _nLoads[i] = (int)floor(bg[bids[i]].work_load() / _wkAvg);
    // residual range floor
    _rFlrs[i] = (double)bg[bids[i]].work_load() - _nLoads[i]*_wkAvg*(1.0 + _toler);
    // residual range ceiling
    _rCls[i]  = (double)bg[bids[i]].work_load() - _nLoads[i]*_wkAvg*(1.0 - _toler);
    // if residual is smaller than all faces then set no residual
    // all belongs to main zone
    if(   bg[bids[i]].area(0) > _rCls[i] && bg[bids[i]].area(1) > _rCls[i] 
       && bg[bids[i]].area(2) > _rCls[i]){
      _rCls[i]  = 0.0;
      _rFlrs[i] = 0.0;
    }
    // if floor of residual is <= 0, set no residual zone
    if(_rFlrs[i] < DSMALL){
      _rCls[i]  = 0.0;
      _rFlrs[i] = 0.0;
    }
  }

  // available parts
  _nAvailPart = _nPart;
  for(unsigned int i=0; i<bids.size(); i++)
    _nAvailPart -= _nLoads[i];

  return(0);
}


int PartGreedyCut::assign_r_zone(BlockGraph& bg, vector<int>& bids)
{
  int        bid, rngs[6];
  BlockCut   cut;
  double     rToler;       // tolerance for residual zone range
  double     rLoad;        // work load for residual zone
  int        pid = _nPart-1;

  // Note during the loop, the size of bg is increasing. This is fine since 
  // blocks before 'i' has no residual zone, neither do the new added blocks.
  for(uint i=0; i<bids.size(); i++){ // blks
    // block range
    bg[bids[i]].get_range(rngs);
    // if blk i has residual zone
    if(_rCls[i] > DSMALL && !_isDones[i]){
      // if main zone exists, not whole block in main zone
      // cut into main zone and residual zone
      if(_nLoads[i] > 0){
        // cut block
        rLoad  = (_rCls[i] + _rFlrs[i]) / 2.0;
        rToler = (_rCls[i] -  rLoad)    / rLoad;
        bg.find_min_blkcut(bids[i], rLoad, rToler, cut);
        if(cut.blkID == BLOCK_NULL){
#if verbose>=1
          cout << "PartGreedyCut: Fail to find cut between r and m zone" << endl;
#endif
          _rFlrs[i] = 0.0; _rCls[i]  = 0.0;
          continue;
        }
        bg.cut_block(cut);
        // both blocks are marked without residual zones (even new block, since
        // it is done later).
        _rCls[i]  = 0.0;  _rCls.push_back(0.0);
        _rFlrs[i] = 0.0;  _rFlrs.push_back(0.0);
        // append new block
        bids.push_back((int)bg.size()-1);
        // if residual zone is in place
        if(!cut.isKept){
          bid   = bids[i];
          rLoad = (double)(bg[bid].area(cut.axis) * (cut.pos - rngs[cut.axis]));
          // append main block load and mark undone
          _nLoads.push_back(_nLoads[i]);
          _isDones.push_back(false);
          // set residual block load and marked as done
          // it will be one in this function.
          _nLoads[i]  = 0;
          _isDones[i] = true;
        }
        // if residual zone is appended
        else{
          bid   = (int)bg.size()-1;
          rLoad = (double)(bg[bid].area(cut.axis) * (rngs[cut.axis+3] - cut.pos));
          // current main block remains undone
          // append residual block, mark as done, will be done in this function
          _nLoads.push_back(0);
          _isDones.push_back(true);
        }
#if verbose>=1
        cout << "PartGreedyCut: cut block " << i << "'s rzone to rblock " << bid << endl;
#endif
      }
      // whole block is residual
      else{
        bid   = bids[i];
        rLoad = (double)bg[bid].work_load();
        _isDones[i] = true;
#if verbose>=1
        cout << "PartGreedyCut: block " << i << "is rblock." << endl;
#endif
      }

      vector<int> localBids;
      int         cmpny = BLOCK_NULL;
      localBids.push_back(bid);
      // find company for residual block bid to fulfill a partition
      while(rLoad < _wkAvg*(1.0-_toler)){
        _find_rzone_cmpny(bg, bids, localBids, cmpny);
        if(cmpny == BLOCK_NULL){
#if verbose>=1
          cout << "PartGreedyCut: Warning: Fail to find company" << endl;
          cout << "PartGreedyCut: Need load " << _wkAvg - rLoad << endl;
#endif
          break;
        }
        localBids.push_back(cmpny);
        rLoad += (double)bg[cmpny].work_load();
#if verbose>=1
        cout << "PartGreedyCut: rblock " << bid << " find cmpny " << cmpny 
             << " load " << bg[cmpny].work_load() << endl;
#endif
      }
      // set residual blocks to one partition
      //int pid  = _nPart - _nAvailPart;
      for(unsigned int j=0; j<localBids.size(); j++){
        bg[localBids[j]].set_partition(pid);
        _isDones[vector_index(bids, localBids[j])] = true;
      }
      bg.set_shmem_map(localBids.size(), &localBids[0]);
      // used one part and break out 'blks' loop if no part are available
      _nAvailPart--;
#if verbose>=1
        cout << "PartGreedyCut: Assign ";
        for(unsigned int j=0; j<localBids.size(); j++)
          cout << " " << localBids[j];
        cout << " to part " << pid << ", left " << _nAvailPart << " avail parts" << endl;
#endif
      // if pid is the last available partition, add all rblock here
      if(_nAvailPart == 0){
        for(unsigned int j=0; j<bids.size(); j++){
          if(_nLoads[j] == 0 && _rCls[j] > 0 && !_isDones[j]){
            if(find(localBids.begin(), localBids.end(), bids[j]) == localBids.end()){
              localBids.push_back(bids[j]);
              bg[bids[j]].set_partition(pid);
              _isDones[j] = true;
#if verbose>=1
              cout << "PartGreedyCut: Add rblock " << bids[j] << " to last available part." << endl;
#endif
            }
          }
        }
        bg.set_shmem_map(localBids.size(), &localBids[0]);
        break;
      }
      // next part, reverse order
      pid--;
      // clear saved block id
      localBids.clear();
    }
  }// end blks

  // if some residual zones are not added to partition, then remove them
  // their block is regarded as main zone only (mblock).
  for(unsigned int i=0; i<_rCls.size(); i++){
    _rCls[i]  = 0.0;
    _rFlrs[i] = 0.0;
  }

  // if there is still available parts, assign it to overload block
  if(_nAvailPart > 0){
    while(_nAvailPart > 0){
      double ovLdMax = -0.1; 
      int    blkMax  = INDEX_NULL;
      for(unsigned int i=0; i<bids.size(); i++){
        if(_nLoads[i] > 0){
          double ovLdTmp = (double)bg[bids[i]].work_load() - _nLoads[i]*_wkAvg;
          if(ovLdTmp > ovLdMax){
            ovLdMax = ovLdTmp;
            blkMax  = bids[i];
          }
        }
      }
      _nLoads[vector_index(bids, blkMax)]++;
      _nAvailPart--;
#if verbose>=1
      cout << "PartGreedyCut: assign 1 available part to block " << blkMax << endl;
#endif
    }
  }

  return(0);
}


int PartGreedyCut::cut_mzone_reb(BlockGraph& bg, vector<int>& bids, int bid)
{
  int iBlk = vector_index(bids, bid);
  vector< vector<int> > cuts(3); // cut positions

  // bid must be in bids
  assert(find(bids.begin(), bids.end(), bid) != bids.end());

  // if itself is single main zone, set partition
  if(_nLoads[iBlk] == 1){
    bg[bid].set_partition(_iPart);
    _iPart++;
#if verbose>=1
    cout << "PartGreedyCut: mblock " << bid << " fit in one part, assigned to " 
         << _iPart << endl;
#endif
  }
  // if has multiple main zones
  else{
    int size0 = bg.size();
    bg.reb(bid, _nLoads[iBlk], _toler);
    // set partition of the main block
    bg[bid].set_partition(_iPart);
    ++_iPart;
    _rCls[iBlk]    = 0.0;  _rFlrs[iBlk] = 0.0;
    _isDones[iBlk] = true;
    _nLoads[iBlk]  = 1;
    // set partition for newly generated blocks
    for (int j=size0; j<bg.size(); ++j) {
      bg[j].set_partition(_iPart);
      ++_iPart;
      bids.push_back(j);
      _rCls.push_back(0.0);  _rFlrs.push_back(0.0);
      _isDones.push_back(true);
    _nLoads.push_back(1);
    }
#if verbose>=1
      cout << "PartGreedyCut: reb mblock " << bid << " into " << _nLoads[iBlk] << endl;
#endif
  }

  return(0);
}


int PartGreedyCut::_find_rzone_cmpny(BlockGraph& bg, vector<int>& bids, \
                                     vector<int>& localBids, int& cmpny)
/*--------------------------------------------------------------
Method: first find the load needed to fit in one partition. Then
        traverse blocks with residual zone find part or whole block
        that fit in the load. Finally choose the one with min 
        increase in communication time.
--------------------------------------------------------------*/
{
  double   rLoad   = 0.0;  // input residual blocks' load
  double   loadCut = 0.0, tolerCut = 0.0;
  BlockCut cut, cutMin(DLARGE);
  double   msgIncrMin = DLARGE, msgIncr;
  int      blkMin = BLOCK_NULL;

  // default value: company not found
  cmpny = BLOCK_NULL;

  // get current residual load
  for(unsigned int i=0; i<localBids.size(); i++)
    rLoad += (double)bg[localBids[i]].work_load();

  loadCut  = _wkAvg - rLoad;
  tolerCut = _wkAvg * _toler / loadCut;

  // find the cut-off and whole block fit in 1 part and minimize the communicaiton
  for(unsigned int i=0; i<bids.size(); i++){
    if(!_isDones[i]){
      // skip blocks already considered
      if(find(localBids.begin(), localBids.end(), bids[i]) != localBids.end()) continue;
      // if load needed fit in the residual zone, cut for the load
      if(loadCut < _rCls[i]){
        bg.find_min_blkcut(bids[i], loadCut, tolerCut, cut, localBids);
        if(cut < cutMin && cut.blkID != BLOCK_NULL)  cutMin = cut;
      }
      // if has residual zone and it is less than the load needs
      else if(_rCls[i] > DSMALL){
        // if main zone exists, cut the residual zone
        if(_nLoads[i] > 0){
          //double loadTmp  = (_rCls[i] + _rFlrs[i]) / 2.0;
          //double tolerTmp = (_rCls[i] - loadTmp)   / loadTmp;
          double loadTmp  = _rCls[i];
          double tolerTmp = _toler;
          bg.find_min_blkcut(bids[i], loadTmp, tolerTmp, cut, localBids);
          if(cut < cutMin && cut.blkID != BLOCK_NULL)  cutMin = cut;
        }
        // it is a redisual block
        else{
          msgIncr = 0.0;
          for(unsigned int j=0; j<localBids.size(); j++)
            msgIncr -= bg.msg_load(bids[i], localBids[j]);
          // mark the min message block
          if(msgIncr < msgIncrMin){
            msgIncrMin = msgIncr;
            blkMin     = bids[i];
          }
        }
      }
    }
  }

  // if cutoff minimize the global msg
  if(cutMin.msgIncr < msgIncrMin && cutMin.blkID != BLOCK_NULL){
    // id of remaining block, can be mblock or rblock
    int rmn, iRmn, iCmpny, iMin = vector_index(bids, cutMin.blkID); 
    // cut block and add new block to 'bids'.
    bg.cut_block(cutMin);
    bids.push_back(bg.size()-1);
    // append default value to vectors, changes later
    _isDones.push_back(false);
    _rCls.push_back(0.0);
    _rFlrs.push_back(0.0);
    _nLoads.push_back(0);
    // set cmpny id, and main zone id
    if(cutMin.isKept){
      cmpny = bg.size()-1;   iCmpny = vector_index(bids, cmpny);
      rmn   = cutMin.blkID;  iRmn   = vector_index(bids, rmn);
    }
    else{
      cmpny = cutMin.blkID;  iCmpny = vector_index(bids, cmpny);
      rmn  = bg.size()-1;    iRmn   = vector_index(bids, rmn);
    }
    // reset nload
    _nLoads[iRmn]   = _nLoads[iMin];
    _nLoads[iCmpny] = 0;
    // reset residual
    _rCls[iRmn]    = _rCls[iMin]  - (double)bg[cmpny].work_load();
    _rFlrs[iRmn]   = _rFlrs[iMin] - (double)bg[cmpny].work_load();
    _rCls[iCmpny]  = 0.0;
    _rFlrs[iCmpny] = 0.0;
    // check if residual is smaller than block's face
    if(_rCls[iRmn] < bg[rmn].area(0) && _rCls[iRmn] < bg[rmn].area(1) && 
       _rCls[iRmn] < bg[rmn].area(2)  ){
      _rFlrs[iRmn] = 0.0;
      _rCls[iRmn]  = 0.0;
    }
    // check if residual floor is already zero
    if(_rFlrs[iRmn] < DSMALL){
      _rFlrs[iRmn] = 0.0;
      _rCls[iRmn]  = 0.0;
    }
  }

  // if a whole block is residual and minimize the the global msg
  if(msgIncrMin <= cutMin.msgIncr && msgIncrMin < DLARGE){
    cmpny = blkMin;
  }

  // mark company as done
  if(cmpny != BLOCK_NULL)  _isDones[vector_index(bids, cmpny)] = true;

  return(0);
}


int PartGreedyCut::_cut_blk_to_parts(BlockGraph& bg, vector<int>& bids, int bid,\
                                     vector< vector<int> >& cutLocs)
{
  BlockCut cut;
  int      nAppend = 0;
  int      size0   = bg.size();
  int      iBlk    = vector_index(bids, bid);

  // cut the block, append new blocks to end of graph
  for(int iDir=0; iDir<3; iDir++){
    if(cutLocs[iDir].size() > 0){
      sort(cutLocs[iDir].begin(), cutLocs[iDir].end(), greater<int>());
      for(unsigned int iCut=0; iCut<cutLocs[iDir].size(); iCut++){
        cut.blkID  = bid;
        cut.axis   = iDir;
        cut.pos    = cutLocs[iDir][iCut];
        cut.isKept = true;
        bg.cut_block(cut);
        for(int iApnd=0; iApnd<nAppend; iApnd++){
          cut.blkID = size0 + iApnd;
          bg.cut_block(cut);
        }
      }
    }
    // save # appended blocks
    nAppend = bg.size() - size0;
  }

  // set partitions and update residual, isdone, nLoads for bid
  bg[bid].set_partition(_iPart);
  _iPart++;
  _rCls[iBlk]    = 0.0;  _rFlrs[iBlk] = 0.0;
  _isDones[iBlk] = false;
  _nLoads[iBlk]  = 1;

  // set partitions and update residual, isdone, nLoads for appended blocks
  for(int i=size0; i<bg.size(); i++){
    bg[i].set_partition(_iPart);
    _iPart++;
    bids.push_back(i);
    _rCls.push_back(0.0);  _rFlrs.push_back(0.0);
    _isDones.push_back(true);
    _nLoads.push_back(1);
  }

  return(0);
}
